﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Net;
using System.Net.Http;
using System.Net.Mail;
using System.Net.Mime;
using System.Net.NetworkInformation;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using Android.App;
using Android.Content;
using Android.OS;
using Android.Util;
using Android.Views.InputMethods;
using HHT.Resources.DataHelper;
using HHT.Resources.Model;

namespace HHT
{
    public class CommonUtils
    {

        private static readonly HttpClient client = new HttpClient();

        // サーバからデータ取得（同期）
        public static string GetJsonData(string url)
        {
            // Create an HTTP web request using the URL:
            HttpWebRequest request = (HttpWebRequest)HttpWebRequest.Create(new System.Uri(url));
            request.ContentType = "application/json";
            request.Method = "GET";

            // Send the request to the server and wait for the response:
            using (WebResponse response = request.GetResponse())
            {
                // Get a stream representation of the HTTP web response:
                using (StreamReader reader = new StreamReader(response.GetResponseStream()))
                {
                    var content = reader.ReadToEnd();
                    if (string.IsNullOrWhiteSpace(content))
                    {
                        Console.Out.WriteLine("Response contained empty body...");
                    }
                    else
                    {
                        return content;
                    }
                }
            }

            return null;
        }

        // サーバからデータ取得（非同期）
        public static async Task<string> GetJsonDataAsync(string url)
        {
            // Create an HTTP web request using the URL:
            HttpWebRequest request = (HttpWebRequest)HttpWebRequest.Create(new System.Uri(url));
            request.ContentType = "application/json";
            request.Method = "GET";

            // Send the request to the server and wait for the response:
            using (WebResponse response = await request.GetResponseAsync())
            {
                // Get a stream representation of the HTTP web response:
                using (StreamReader reader = new StreamReader(response.GetResponseStream()))
                {
                    var content = reader.ReadToEnd();
                    if (string.IsNullOrWhiteSpace(content))
                    {
                        Console.Out.WriteLine("Response contained empty body...");
                    }
                    else
                    {
                        return content;
                    }
                }
            }

            return null;
        }

        // サーバからデータ取得（非同期）
        public static string Post(string url, Dictionary<string, string> values)
        {
            var content = new FormUrlEncodedContent(values);
            
            var response = client.PostAsync(url, content).Result;
            var responseString = response.Content.ReadAsStringAsync().Result;
            
            return responseString;
        }

        // サーバからデータ取得（非同期）
        public static async Task<string> PostAsync(string url, Dictionary<string, string> values)
        {
            var content = new FormUrlEncodedContent(values);
            
            var response = await client.PostAsync(url, content);
            var responseString = await response.Content.ReadAsStringAsync();
            
            return responseString;
        }
        
        // ソフトキーボードを隠す
        public static void HideKeyboard(Activity activity)
        {
            InputMethodManager inputManager = (InputMethodManager)activity.GetSystemService(Context.InputMethodService);

            var currentFocus = activity.CurrentFocus;
            if (currentFocus != null)
            {
                inputManager.HideSoftInputFromWindow(currentFocus.WindowToken, HideSoftInputFlags.None);
            }
        }

        public static bool IsHostReachable()
        {
            string host = WebService.GetHostIpAddress();
            
            if (string.IsNullOrEmpty(host))
                return false;

            bool isReachable = false;
            

            Ping x = new Ping();
            PingReply reply = x.Send(IPAddress.Parse(host)); //enter ip of the machine
            if (reply.Status == IPStatus.Success) // here we check for the reply status if it is success it means the host is reachable
            {
                isReachable = true;
            }

            return isReachable;
        }

        public async static Task<bool> IsHostReachableAsync()
        {
            string host = WebService.GetHostIpAddress();

            if (string.IsNullOrEmpty(host))
                return false;

            bool isReachable = false;

            Ping x = new Ping();
            PingReply reply = await x.SendPingAsync(IPAddress.Parse(host), 3000); //enter ip of the machine
            if (reply.Status == IPStatus.Success) // here we check for the reply status if it is success it means the host is reachable
            {
                isReachable = true;
            }

            return isReachable;
        }


        public static string GetDateYYYYMMDDwithSlash(string dateString)
        {
            string ymd = Regex.Replace(Convert.ToString(dateString), @"[^\u0000-\u007F]|/", string.Empty);
            DateTime dt = DateTime.ParseExact(ymd.ToString(), "yyyyMMdd", CultureInfo.InvariantCulture);
            return dt.ToString("yyyy/MM/dd", CultureInfo.InvariantCulture);
        }

        public static void SendFile(string fileName)
        {
            FtpFileHelper ftpFileHelper = new FtpFileHelper();
            FtpFile ftpInfo = ftpFileHelper.GetFtpFileInfo();
            string ftpIp = ftpInfo.ftp_ip;
            string ftpUser = ftpInfo.ftp_user;
            string ftpPass = ftpInfo.ftp_pass;

            string PureFileName = new FileInfo(fileName).Name;
            System.Uri serverUri = new System.Uri(System.String.Format("ftp://" + ftpIp + "/" + PureFileName));

            FtpWebRequest request = (FtpWebRequest)WebRequest.Create(serverUri);
            request.Method = WebRequestMethods.Ftp.UploadFile;
            request.UseBinary = true;
            request.UsePassive = true;
            request.KeepAlive = false;
            request.Timeout = System.Threading.Timeout.Infinite;
            request.AuthenticationLevel = System.Net.Security.AuthenticationLevel.None;

            if (ftpUser != "" && ftpPass != "")
            {
                request.Credentials = new NetworkCredential(ftpUser, ftpPass);
            }
            
            byte[] data = File.ReadAllBytes(fileName);
            Stream stream = request.GetRequestStream();
            stream.Write(data, 0, data.Length);
            stream.Close();
              
            FtpWebResponse response = (FtpWebResponse)request.GetResponse();

            response.Close();
            response.Dispose();
            
        }


        public static async Task SendFileAsync(string fileName)
        {

            FtpWebRequest request = null;
            FtpWebResponse response = null;

            try
            {
                FtpFileHelper ftpFileHelper = new FtpFileHelper();
                FtpFile ftpInfo = ftpFileHelper.GetFtpFileInfo();
                string ftpIp = ftpInfo.ftp_ip;
                string ftpUser = ftpInfo.ftp_user;
                string ftpPass = ftpInfo.ftp_pass;

                string PureFileName = new FileInfo(fileName).Name;
                System.Uri serverUri = new System.Uri(System.String.Format("ftp://" + ftpIp + "/" + PureFileName));

                request = (FtpWebRequest)WebRequest.Create(serverUri);
                request.Method = WebRequestMethods.Ftp.UploadFile;
                request.UseBinary = true;
                request.UsePassive = true;//true;
                request.KeepAlive = false;
                request.Timeout = System.Threading.Timeout.Infinite;
                request.AuthenticationLevel = System.Net.Security.AuthenticationLevel.None;

                if (ftpUser != "" && ftpPass != "")
                {
                    request.Credentials = new NetworkCredential(ftpUser, ftpPass);
                }

                byte[] data = File.ReadAllBytes(fileName);
                Stream stream = request.GetRequestStream();
                stream.Write(data, 0, data.Length);
                stream.Close();

                response = (FtpWebResponse)await request.GetResponseAsync();

                response.Close();
                response.Dispose();

                return;
            }
            catch
            {
                throw new System.Exception();
            }
            finally
            {
                
                if (response != null)
                {
                    response.Close();
                    response.Dispose();
                }
                if (request != null)
                    
                    request = null;
                GC.Collect();
            }
        }


        public static bool CheckFTPConnection()
        {

            try
            {
                FtpFileHelper ftpFileHelper = new FtpFileHelper();
                FtpFile ftpInfo = ftpFileHelper.GetFtpFileInfo();
                string ftpIp = ftpInfo.ftp_ip;
                string ftpUser = ftpInfo.ftp_user;
                string ftpPass = ftpInfo.ftp_pass;

                System.Uri serverUri = new System.Uri(System.String.Format("ftp://" + ftpIp + "/"));

                FtpWebRequest request = (FtpWebRequest)WebRequest.Create(serverUri);
                request.Method = WebRequestMethods.Ftp.ListDirectory;
                request.UseBinary = true;
                request.UsePassive = true;
                request.KeepAlive = false;
                request.Timeout = System.Threading.Timeout.Infinite;
                request.AuthenticationLevel = System.Net.Security.AuthenticationLevel.None;

                if (ftpUser != "" && ftpPass != "")
                {
                    request.Credentials = new NetworkCredential(ftpUser, ftpPass);
                }


                FtpWebResponse response = (FtpWebResponse)request.GetResponse();

                response.Close();
                response.Dispose();

            }
            catch
            {
                return false;
            }


            return true;
        }

        public static bool IsEmulator()
        {
            return Build.Fingerprint.StartsWith("generic")
                    || Build.Fingerprint.StartsWith("unknown")
                    || Build.Model.Contains("google_sdk")
                    || Build.Model.Contains("Emulator")
                    || Build.Model.Contains("Android SDK built for x86")
                    || Build.Manufacturer.Contains("Genymotion")
                    || (Build.Brand.StartsWith("generic") && Build.Device.StartsWith("generic"))
                    || "google_sdk".Equals(Build.Product);
        }

    }
}