﻿using System.Collections.Generic;
using Android.Content;
using Android.Views;
using Android.Widget;
using HHT.Resources.Model;

namespace HHT
{
    class TsumikomiMikenAdapter : BaseAdapter
    {
        private List<string> mikenList;

        public TsumikomiMikenAdapter(List<string> mikenList)
        {
            this.mikenList = mikenList;
        }

        public override Java.Lang.Object GetItem(int position)
        {
            return mikenList[position];
        }

        public override long GetItemId(int position)
        {
            return position;
        }

        public override View GetView(int position, View convertView, ViewGroup parent)
        {
            var view = convertView;
            VendorAdapterViewHolder holder = null;

            if (view != null)
                holder = view.Tag as VendorAdapterViewHolder;

            if (holder == null)
            {
                holder = new VendorAdapterViewHolder();
                var item = mikenList[position];

                view = LayoutInflater.From(parent.Context).Inflate(Resource.Layout.adapter_list_tsumikomi_miken, parent, false);
                view.FindViewById<TextView>(Resource.Id.kamotsu_no).Text = item;

            }

            return view;
        }

        public override int Count
        {
            get
            {
                return mikenList.Count;
            }
        }

    }
}