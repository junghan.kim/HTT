﻿using Android.App;
using Android.Content;
using Android.OS;
using Android.Support.V7.App;
using Android.Util;
using HHT.Resources.DataHelper;
using System.Threading.Tasks;

namespace HHT
{
    [Activity(Theme = "@style/MyTheme.Splash", MainLauncher = true, NoHistory = true)]
    public class SplashActivity : AppCompatActivity
    {
        static readonly string TAG = "X:" + typeof(SplashActivity).Name;

        public override void OnCreate(Bundle savedInstanceState, PersistableBundle persistentState)
        {
            base.OnCreate(savedInstanceState, persistentState);
            Log.Debug(TAG, "SplashActivity.OnCreate");
        }

        protected override void OnResume()
        {
            base.OnResume();
            
            Task.Run(() =>
            {
                DataBase db = new DataBase();
                db.CreateDataBase();
                Resources.Model.Config config = db.GetHostIpAddress();
                
                if (config != null)
                {
                    string hostIp = config.hostIp;
                    string port = config.hostPort;
                    WebService.SetHostIpAddress(hostIp, port);
                }

                Log.Debug(TAG, "Startup work is finished - starting MainActivity.");
                StartActivity(new Intent(Application.Context, typeof(MainActivity)));
                
            });
        }

        // Prevent the back button from canceling the startup process
        public override void OnBackPressed() { }

    }
}