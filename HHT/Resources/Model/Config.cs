﻿
namespace HHT.Resources.Model
{
    public class Config
    {
        public string hostIp { get; set; }
        public string hostPort { get; set; }
    }
}