﻿
using SQLite;

namespace HHT.Resources.Model
{
    public class Login
    {
        [PrimaryKey]
        public string souko_cd { get; set; }
        public string souko_nm { get; set; }
        
        public string tantousha_cd { get; set; }
        public string tantohsya_nm { get; set; }

        public string kitaku_cd { get; set; }
        public string menu_flg { get; set; }

        public string def_tokuisaki_cd { get; set; }

        public string souko_kbn { get; set; }

        public string tsuhshin_kbn { get; set; }

        // 作業中データ
        public string tokuisaki_cd { get; set; }
        public string todokesaki_cd { get; set; }
        public string syuka_date { get; set; }
        public string haiso_date { get; set; }
        public string hht_no { get; set; }

        public string driver_cd { get; set; }
        public string driver_nm { get; set; }
        public string sagyousya_cd { get; set; }
        public string menu_kbn { get; set; }
        
    }
}