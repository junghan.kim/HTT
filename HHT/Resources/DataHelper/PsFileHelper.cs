﻿using System.Collections.Generic;
using System.Linq;
using Android.App;
using Android.Util;
using HHT.Resources.Model;
using SQLite;

namespace HHT.Resources.DataHelper
{
    public class PsFileHelper : DataBase
    {
        readonly static string folder = System.Environment.GetFolderPath(System.Environment.SpecialFolder.Personal);
        readonly static string dbFileName = Application.Context.Resources.GetString(Resource.String.LocalDatabaseFileName);

        public static List<string> GetPassword()
        {
            try
            {
                using (var connection = new SQLiteConnection(System.IO.Path.Combine(folder, dbFileName)))
                {
                    List<PsFile> psfiles = connection.Table<PsFile>().ToList();
                    List<string> result = new List<string>();

                    foreach (PsFile ps in psfiles)
                    {
                        result.Add(ps.pass);
                    }

                    return result;
                }

            }
            catch (SQLiteException ex)
            {
                Log.Info("SQLiteEx", ex.Message);
                return null;
            }
        }

        public bool Insert(PsFile psFile)
        {
            try
            {
                using (var connection = new SQLiteConnection(System.IO.Path.Combine(folder, dbFileName)))
                {
                    connection.Insert(psFile);
                    return true;
                }

            }
            catch (SQLiteException ex)
            {
                Log.Info("SQLiteEx", ex.Message);
                return false;
            }
        }

        public bool DeleteAll()
        {
            try
            {
                using (var connection = new SQLiteConnection(System.IO.Path.Combine(folder, dbFileName)))
                {
                    connection.DeleteAll<PsFile>();
                    return true;
                }

            }
            catch (SQLiteException ex)
            {
                Log.Info("SQLiteEx", ex.Message);
                return false;
            }
        }

    }
}