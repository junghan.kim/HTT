﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Android.App;
using Android.Util;
using HHT.Resources.Model;
using SQLite;

namespace HHT.Resources.DataHelper
{
    public class MbFileHelper : DataBase
    {
        readonly static string folder = System.Environment.GetFolderPath(System.Environment.SpecialFolder.Personal);
        readonly static string dbFileName = Application.Context.Resources.GetString(Resource.String.LocalDatabaseFileName);
        
        public bool Insert(MbFile mbFile)
        {
            try
            {
                using (var connection = new SQLiteConnection(System.IO.Path.Combine(folder, dbFileName)))
                {
                    connection.Insert(mbFile);
                    return true;
                }

            }
            catch (SQLiteException ex)
            {
                Log.Info("SQLiteEx", ex.Message);
                return false;
            }
        }

        public bool HasExistMailBagData()
        {
            try
            {
                using (var connection = new SQLiteConnection(System.IO.Path.Combine(folder, dbFileName)))
                {
                    if (connection.Table<MbFile>().ToList().Count > 0) return true;
                }

            }
            catch (SQLiteException ex)
            {
                Log.Info("SQLiteEx", ex.Message);
                return false;
            }

            return false;
        }
        
        public static bool HasExistMailBagData(string tokuisakiCd, string todokesakiCd , string keycode)
        {
            try
            {
                using (var connection = new SQLiteConnection(System.IO.Path.Combine(folder, dbFileName)))
                {
                    string sql = "select distinct * from MbFile where tokuisaki_cd = ? and todokesaki_cd= ? and kanri_no =?";
                    List<MbFile> result = connection.Query<MbFile>(sql, tokuisakiCd, todokesakiCd, keycode);
                    return result != null && result.Count != 0;
                }
            }
            catch (SQLiteException ex)
            {
                Log.Info("SQLiteEx", ex.Message);
                return false;
            }
        }

        /*
        public bool HasExistMailBagData(string keycode)
        {
            try
            {
                using (var connection = new SQLiteConnection(System.IO.Path.Combine(folder, dbFileName)))
                {
                    string sql = "select * from MbFile where kanri_no =?";
                    List<MbFile> result = connection.Query<MbFile>(sql, keycode);
                    return result != null && result.Count !=0;
                }
            }
            catch (SQLiteException ex)
            {
                Log.Info("SQLiteEx", ex.Message);
                return false;
            }
        }
        */

        public static int GetMailbackCount(string tokuisaki_cd, string todokesaki_cd)
        {
            try
            {
                using (var connection = new SQLiteConnection(System.IO.Path.Combine(folder, dbFileName)))
                {
                    string sql = @"select distinct * from MbFile where tokuisaki_cd = ? and todokesaki_cd= ?";
                    return connection.Query<MbFile>(sql, tokuisaki_cd, todokesaki_cd).Count;
                }

            }
            catch (SQLiteException ex)
            {
                Log.Info("SQLiteEx", ex.Message);
            }

            return 0;
        }

        public bool InsertAll(List<MbFile> mbFiles)
        {
            try
            {
                using (var connection = new SQLiteConnection(System.IO.Path.Combine(folder, dbFileName)))
                {
                    connection.InsertAll(mbFiles);
                    return true;
                }

            }
            catch (SQLiteException ex)
            {
                Log.Info("SQLiteEx", ex.Message);
                return false;
            }

        }

        public bool DeleteAll()
        {
            try
            {
                using (var connection = new SQLiteConnection(System.IO.Path.Combine(folder, dbFileName)))
                {
                    connection.DeleteAll<MbFile>();
                    return true;
                }

            }
            catch (SQLiteException ex)
            {
                Log.Info("SQLiteEx", ex.Message);
                return false;
            }
        }

        public bool DeleteByTokuiAndTodokeCode(string tokuisaki_cd, string todokesaki_cd)
        {
            try
            {
                using (var connection = new SQLiteConnection(System.IO.Path.Combine(folder, dbFileName)))
                {
                    string sql = @"delete from MbFile where tokuisaki_cd = ? and todokesaki_cd= ?";
                    List<int> result = connection.Query<int>(sql, tokuisaki_cd, todokesaki_cd);


                    return true;
                }

            }
            catch (SQLiteException ex)
            {
                Log.Info("SQLiteEx", ex.Message);
                return false;
            }
        }

    }
}