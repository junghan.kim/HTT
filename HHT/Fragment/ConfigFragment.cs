﻿using Android.OS;
using Android.Views;
using Android.Widget;
using HHT.Resources.DataHelper;
using Android.App;
using Android.Util;

namespace HHT
{
    public class ConfigFragment : BaseFragment
    {
        static readonly string TAG = "X:" + typeof(ConfigFragment).Name;
        private View view;
        private EditText _ipEditText, _portEditText;
        private Button _button;
        
        public override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            Log.Debug(TAG, "ConfigFragment is loadad.");
        }

        public override View OnCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
        {
            view = inflater.Inflate(Resource.Layout.fragment_config, container, false);
            
            SetTitle("ホスト設定");

            // ホスト接続先設定
            _ipEditText = view.FindViewById<EditText>(Resource.Id.hostIp);
            _ipEditText.Text = WebService.GetHostIpAddress();

            _portEditText = view.FindViewById<EditText>(Resource.Id.hostPort);
            _portEditText.Text = WebService.GetPort();

            _button = view.FindViewById<Button>(Resource.Id.confirm);
            _button.Click += delegate {
                if (_ipEditText.Text == ""){
                    ShowDialog("エラー", "ホストIPを入力してください。", () => { });
                    return;
                }

                if (_portEditText.Text == "")
                {
                    ShowDialog("エラー", "Port番号を入力してください。", () => { });
                    return;
                }
                

                ShowDialog("警告", "この設定でよろしいでしょうか？", (isOkPushed) => {
                    if (isOkPushed)
                    {
                        new DataBase().SetHostIpAddress(
                            _ipEditText.Text, _portEditText.Text);
                        WebService.SetHostIpAddress(_ipEditText.Text, _portEditText.Text);

                        Log.Debug(TAG, "Host Setting Completed : " + _ipEditText.Text + ":" + _portEditText.Text);
                        Log.Debug(TAG, "Back to LoginFragment");
                        FragmentManager.PopBackStack();
                    }
                });
            };

            return view;
        }
        
        public override bool OnKeyDown(Keycode keycode, KeyEvent paramKeyEvent)
        {
            return true;
        }
    }
}
 