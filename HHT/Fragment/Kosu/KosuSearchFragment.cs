﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Android.App;
using Android.Content;
using Android.OS;
using Android.Preferences;
using Android.Views;
using Android.Widget;
using HHT.Resources.Model;

namespace HHT
{
    public class KosuSearchFragment : BaseFragment
    {
        private TodokesakiAdapter todokesakiAdapter;
        
        ISharedPreferences prefs;
        ISharedPreferencesEditor editor;

        ListView listView;
        private int kosuMenuflag;

        public override View OnCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
        {
            View view = inflater.Inflate(Resource.Layout.fragment_kosu_search, container, false);
            prefs = PreferenceManager.GetDefaultSharedPreferences(Context);
            editor = prefs.Edit();

            kosuMenuflag = prefs.GetInt(Const.KOSU_MENU_FLAG, (int)Const.KOSU_MENU.TODOKE); // 画面区分
            listView = view.FindViewById<ListView>(Resource.Id.listView1);

            SetTitle("届先指定検品");
            SetFooterText("F1：未検");

            SetTodokesakiAsync();

            return view;
        }

        private void SetTodokesakiAsync()
        {
            ((MainActivity)this.Activity).ShowProgress("届先検索中。。。");

            Task.Run(async() => {

                await Task.Delay(500);

                List<KOSU060> resultList = new List<KOSU060>();
                string soukoCd = prefs.GetString("souko_cd", "");
                string kitakuCd = prefs.GetString("kitaku_cd", "");
                string syuka_date = prefs.GetString("syuka_date", "");
                string bin_no = prefs.GetString("bin_no", "");

                try
                {
                    if (kosuMenuflag == (int)Const.KOSU_MENU.TODOKE)
                    {
                        resultList = await WebService.RequestKosu060Async(soukoCd, kitakuCd, syuka_date, bin_no);
                    }
                    else if (kosuMenuflag == (int)Const.KOSU_MENU.VENDOR)
                    {
                        resultList = await WebService.RequestKosu065Async(soukoCd, kitakuCd, syuka_date, bin_no);
                    }
                }
                catch
                {
                    ShowDialog("エラー", "例外エラーが発生しました。", () => { });
                    return;
                }

                if (resultList.Count > 0)
                {
                    Activity.RunOnUiThread(() =>
                    {
                        todokesakiAdapter = new TodokesakiAdapter(resultList);
                        listView.ItemClick += ListView_ItemClick;
                        listView.Adapter = todokesakiAdapter;
                        listView.RequestFocus();
                        listView.SetSelection(0);
                    });
                }
                else
                {
                    ShowDialog("エラー", "表示データがありません。", () =>{
                        FragmentManager.PopBackStack();
                    });
                }

                Activity.RunOnUiThread(() => ((MainActivity)this.Activity).DismissDialog()); 
            });
        }

        void ListView_ItemClick(object sender, AdapterView.ItemClickEventArgs e)
        {
            var item = todokesakiAdapter[e.Position];
            
            editor.PutString("todokesaki_cd", item.todokesaki_cd);
            editor.PutString("tokuisaki_cd", item.tokuisaki_cd);
            editor.PutString("tokuisaki_nm", item.tokuisaki_rk);
            editor.PutString("tsumi_vendor_cd", item.default_vendor);
            editor.PutString("tsumi_vendor_nm", "");
            editor.PutString("vendor_cd", item.default_vendor);
            editor.PutString("vendor_nm", "");
            editor.PutString("start_vendor_cd", "");
            
            editor.Apply();

            StartFragment(FragmentManager, typeof(KosuWorkFragment));
            
        }

        public override bool OnKeyDown(Keycode keycode, KeyEvent paramKeyEvent)
        {
            if (keycode == Keycode.F1)
            {
                var item = todokesakiAdapter[listView.SelectedItemPosition];
                editor.PutString("tokuisaki_cd", item.tokuisaki_cd);
                editor.PutString("todokesaki_cd", item.todokesaki_cd);
                editor.Apply();

                StartFragment(FragmentManager, typeof(KosuMikenFragment));
            }

            return true;
        }

    }
}