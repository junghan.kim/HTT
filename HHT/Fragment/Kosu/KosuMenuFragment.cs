﻿using Android.OS;
using Android.Views;
using Android.Widget;

namespace HHT
{
    public class KosuMenuFragment : BaseFragment
    {
        public override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
        }

        public override View OnCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
        {
            var view = inflater.Inflate(Resource.Layout.fragment_menu_nyuka, container, false);
            SetTitle("入荷検品");

            view.FindViewById<Button>(Resource.Id.singleInspButton).Click += 
                delegate { StartFragment(FragmentManager, typeof(KosuTanpinMenuFragment)); };
            
            return view;
        }

        public override bool OnKeyDown(Keycode keycode, KeyEvent paramKeyEvent)
        {
            if (keycode == Keycode.Num1)
            {
                StartFragment(FragmentManager, typeof(KosuTanpinMenuFragment));
            }
            
            return true;
        }

    }
}