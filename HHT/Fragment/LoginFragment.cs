﻿using Android.OS;
using Android.Views;
using Android.Widget;
using HHT.Resources.DataHelper;
using HHT.Resources.Model;
using Android.App;
using System;
using System.Collections.Generic;
using System.Threading;
using Android.Content;
using Android.Preferences;
using Android.Util;
using Com.Beardedhen.Androidbootstrap;
using System.Threading.Tasks;
using Com.Densowave.Bhtsdk.Barcode;

namespace HHT
{
    public class LoginFragment : BaseFragment
    {
        private readonly string TAG = "LoginFragment";

        private View view;

        private LoginHelper loginHelper;
        private TantoHelper tantoHelper;

        private BootstrapEditText _SoukoCodeEditText, _TantoCodeEditText;
        private TextView _SoukoNameTextView;
        private BootstrapButton _LoginBtn;

        ISharedPreferences prefs;
        ISharedPreferencesEditor editor;

        LOGIN010 login010;
        string soukoCode, tantoCode, menu_kbn, tantohsya_nm , def_tokuisaki_cd, kitaku_cd, tsuhshin_kbn, souko_kbn, serialNum;
        
        private readonly string ERROR = "エラー";
        private readonly string ERR_NOT_FOUND_SOUKO = "センター情報が見つかりませんでした。\n再確認してください。";

        public override View OnCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
        {
            view = inflater.Inflate(Resource.Layout.fragment_login, container, false);
            prefs = PreferenceManager.GetDefaultSharedPreferences(Context);
            editor = prefs.Edit();
            HideFooter();
            loginHelper = new LoginHelper();
            tantoHelper = new TantoHelper();

            ((MainActivity)this.Activity).SupportActionBar.Title = "ログイン";
            
            _SoukoCodeEditText = view.FindViewById<BootstrapEditText>(Resource.Id.soukoCode);
            _SoukoNameTextView = view.FindViewById<TextView>(Resource.Id.tv_login_soukoName);
            _TantoCodeEditText = view.FindViewById<BootstrapEditText>(Resource.Id.tantoCode);
            _LoginBtn = view.FindViewById<BootstrapButton>(Resource.Id.loginButton);
            TextView _VersionName = view.FindViewById<TextView>(Resource.Id.versionName);
            
            // バージョン情報設定
            _VersionName.Text = "Version " + Application.Context.ApplicationContext.PackageManager.GetPackageInfo(
            Application.Context.ApplicationContext.PackageName, 0).VersionName;
            
            // TODO SerialNumber
            serialNum = "5" + Build.Serial.Substring(Build.Serial.Length - 4, 4);

            if (CommonUtils.IsEmulator())
            {
                view.FindViewById<TextView>(Resource.Id.textView1).Text += "(Emulator)";
                serialNum = "59999";
            }

            // 倉庫コード
            _SoukoCodeEditText.FocusChange += async delegate {
                if (!_SoukoCodeEditText.IsFocused)
                {
                    ((MainActivity)this.Activity).ShowProgress("読み込み中");
                    
                    bool isConneted = await CommonUtils.IsHostReachableAsync();
                    if (isConneted)
                    {
                        _SoukoNameTextView.Text = await SetSoukoNameAsync(_SoukoCodeEditText.Text);
                    }

                    // UIスレッドでの処理
                    ((MainActivity)this.Activity).DismissDialog();
                }
            };

            // 担当者コード
            _TantoCodeEditText.KeyPress += (sender, e) => {
                if (e.Event.Action == KeyEventActions.Down && e.KeyCode == Keycode.Enter)
                {
                    e.Handled = true;
                    CommonUtils.HideKeyboard(Activity);
                    LoginBtn_Clicked();
                }
                else { 
                    e.Handled = false;
                }
            };

            // ログインボタン
            _LoginBtn.Click += delegate { LoginBtn_Clicked();};
            
            return view;
        }
        
        public override void OnActivityCreated(Bundle savedInstanceState)
        {
            base.OnActivityCreated(savedInstanceState);

            // 以前ログイン情報を設定する。
            LoadLastLoginFromDB();
        }
        
        public void LoginBtn_Clicked()
        {
            // Validation Check
            if (!LoginCheck()) return;
            
            var syncContext = SynchronizationContext.Current;
            bool hasError = false;

            ((MainActivity)this.Activity).ShowProgress("ログインしています。");

            Task.Run(() =>
            {
                try
                {
                    bool isConnected = CommonUtils.IsHostReachable();
                    if (isConnected)
                    {
                        Log.Debug(TAG, "Host is Connected");
                        
                        // 担当者マスタ情報をロカールDBに保存する。
                        SaveTantoMaster();

                        // 無線管理テーブルへ情報を登録する。
                        WebService.RequestLogin040(tantoCode, soukoCode, serialNum);
                        
                        // 担当者情報取得
                        LOGIN030 login030 = WebService.RequestLogin030(tantoCode);
                        menu_kbn = login030.menu_kbn;
                        tantohsya_nm = login030.tantohsya_nm;

                    }
                    else
                    {
                        Log.Debug(TAG, "Host is not connected");

                        // 担当者情報取得
                        Tanto tanto = tantoHelper.SelectTantoInfo(tantoCode);
                        menu_kbn = tanto.menu_kbn;
                        tantohsya_nm = tanto.tantohsya_nm;

                        // OffLine作業アラート
                        this.Activity.RunOnUiThread(() 
                            => Toast.MakeText(this.Activity, "ホスト接続なしでログインしました。", ToastLength.Long).Show());
                    }
                }
                catch
                {
                    ShowDialog("エラー", "認証できませんでした。\n入力内容をご確認下さい。", () => {
                        _TantoCodeEditText.Text = "";
                        _TantoCodeEditText.RequestFocus();
                    });

                    ((MainActivity)this.Activity).DismissDialog();
                    hasError = true;

                    return;
                }

                SetCommonParam();

                // 正常の場合、前回ログイン情報を保存する
                loginHelper.Insert(new Login
                {
                    souko_cd = soukoCode,
                    souko_nm = _SoukoNameTextView.Text,
                    tantousha_cd = tantoCode,
                    menu_flg = menu_kbn,
                    tantohsya_nm = tantohsya_nm,
                    def_tokuisaki_cd = this.def_tokuisaki_cd,
                    kitaku_cd = this.kitaku_cd,
                    hht_no = serialNum,
                    driver_cd = tantoCode,
                    driver_nm = tantohsya_nm,
                    menu_kbn = menu_kbn,
                    sagyousya_cd = tantoCode,
                    souko_kbn = souko_kbn,
                    tokuisaki_cd = "",
                    todokesaki_cd = "",
                    syuka_date = "",
                    haiso_date = "",
                    tsuhshin_kbn = ""
                });
                
                Log.Debug(TAG, "Login Succeeded");
                
            }).ContinueWith(t => syncContext.Post(state =>
            {
                // UIスレッドでの処理

                ((MainActivity)this.Activity).DismissDialog();
                //((MainActivity)this.Activity).EnableScanning();

                if (!hasError)
                {
                    PlayBeepOk();
                    StartFragment(FragmentManager, typeof(MainMenuFragment));
                }
            }, null));
        }

        public override void OnResume()
        {
            base.OnResume();

            if(_SoukoCodeEditText.Text != "" && _SoukoNameTextView.Text == "")
            {
                SetSoukoName(_SoukoNameTextView.Text);
            }
        }

        // センター名取得（倉庫名）
        public void SetSoukoName(string soukoCd)
        {
            if (soukoCd == "")
            {
                _SoukoNameTextView.Text = "";
                return;
            }

            try
            {
                login010 = WebService.RequestLogin010(soukoCd);
                def_tokuisaki_cd = login010.def_tokuisaki_cd;
                kitaku_cd = login010.kitaku_cd;
                tsuhshin_kbn = login010.tsuhshin_kbn;
                souko_kbn = login010.souko_kbn;

                this.Activity.RunOnUiThread(()=> _SoukoNameTextView.Text = login010.souko_nm);
            }
            catch (Exception e)
            {
                Log.Debug(TAG, e.StackTrace.ToString());
                
                ShowDialog(ERROR, ERR_NOT_FOUND_SOUKO, () => {
                    _SoukoCodeEditText.Text = "";
                    _SoukoNameTextView.Text = "";
                    _SoukoCodeEditText.RequestFocus();
                });
            }
        }

        public async Task<string> SetSoukoNameAsync(string soukoCd)
        {
            if (string.IsNullOrEmpty(soukoCd)) return "";

            if (!CommonUtils.IsHostReachable()) return "";
            
            try
            {
                login010 = await WebService.RequestLogin010Async(soukoCd);

                def_tokuisaki_cd = login010.def_tokuisaki_cd;
                kitaku_cd = login010.kitaku_cd;
                tsuhshin_kbn = login010.tsuhshin_kbn;
                souko_kbn = login010.souko_kbn;
                
                return login010.souko_nm;

            }
            catch (Exception e)
            {
                Log.Debug(TAG, e.StackTrace.ToString());

                ShowDialog(ERROR, ERR_NOT_FOUND_SOUKO, () => {
                    _SoukoCodeEditText.Text = "";
                    _SoukoNameTextView.Text = "";
                    _SoukoCodeEditText.RequestFocus();
                });

                return "";
            }
        }

        // ログインチェック
        private bool LoginCheck()
        {
            soukoCode = _SoukoCodeEditText.Text;
            tantoCode = _TantoCodeEditText.Text;

            if (soukoCode == "")
            {
                string alertTitle = Resources.GetString(Resource.String.error);
                string alertBody = Resources.GetString(Resource.String.errorMsg002);

                ShowDialog(alertTitle, alertBody, () => {
                    _SoukoNameTextView.Text = "";
                    _SoukoCodeEditText.RequestFocus();
                });

                Log.Debug(TAG, "倉庫コードがありません。");
                return false;
            }

            if (tantoCode == "")
            {
                string alertTitle = Resources.GetString(Resource.String.error);
                ShowDialog(alertTitle, "担当者コードを\n入力して下さい。", () => { });

                Log.Warn(TAG, "担当者コードがありません。");
                return false;
            }

            return true;
        }

        // 以前ログイン情報を設定する。
        private void SetLastLoginInfo()
        {
            ((MainActivity)this.Activity).ShowProgress("最後のログイン情報を取得しています。");

            var syncContext = SynchronizationContext.Current;

            Task.Run(() =>
            {
                if (CommonUtils.IsHostReachable())
                {
                    try
                    {
                        LOGIN020 loginInfo = WebService.RequestLogin020(serialNum);

                        this.Activity.RunOnUiThread(() =>
                        {
                            _SoukoCodeEditText.Text = loginInfo.souko_cd;
                            _TantoCodeEditText.RequestFocus();
                        });
                        
                        SetSoukoName(loginInfo.souko_cd);
                        
                    }
                    catch
                    {
                        LoadLastLoginFromDB();
                    }
                }
                else
                {
                    LoadLastLoginFromDB();
                }

            }).ContinueWith(t => syncContext.Post(state =>
            {
                // UIスレッドでの処理
                ((MainActivity)this.Activity).DismissDialog();
            }, null));
            
        }

        // ホストと連携されていない場合、ローカルから最終ログイン情報を設定する。
        private void LoadLastLoginFromDB()
        {
            // ログイン情報の存在有無によって、処理が分岐される
            Login loginInfo = loginHelper.GetLoginInfo();

            if (loginInfo != null)
            {
                this.Activity.RunOnUiThread(() =>
                {
                    _SoukoCodeEditText.Text = loginInfo.souko_cd;
                    _SoukoNameTextView.Text = loginInfo.souko_nm;
                    def_tokuisaki_cd = loginInfo.def_tokuisaki_cd;
                    kitaku_cd = loginInfo.kitaku_cd;

                    _TantoCodeEditText.RequestFocus();
                });
            }
        }

        // 共通パラメータ設定
        private void SetCommonParam()
        {
            editor.PutString("terminal_id", serialNum);
            editor.PutString("hht_no", serialNum);
            editor.PutString("souko_cd", soukoCode);
            editor.PutString("souko_nm", _SoukoNameTextView.Text);
            editor.PutString("driver_cd", tantoCode);
            editor.PutString("sagyousya_cd", tantoCode);
            editor.PutString("kitaku_cd", kitaku_cd);
            editor.PutString("def_tokuisaki_cd", def_tokuisaki_cd);
            editor.PutString("tsuhshin_kbn", tsuhshin_kbn);
            editor.PutString("souko_kbn", souko_kbn);
            editor.PutString("menu_kbn", menu_kbn);
            editor.PutString("driver_nm", tantohsya_nm);
            editor.Apply();
        }

        // 担当者マスタ情報をロカールDBに保存する。
        private async void SaveTantoMaster()
        {
            List<Tanto> tantoshaList = await WebService.RequestLogin050Async();
            tantoHelper.InsertTantoList(tantoshaList);
        }

        public override bool OnKeyDown(Keycode keycode, KeyEvent paramKeyEvent)
        {
            if (keycode == Keycode.F3)
            {
                StartFragment(FragmentManager, typeof(ConfigFragment));
            }

            // TODO 削除予定
            if (CommonUtils.IsEmulator())
            {
                if (keycode == Keycode.Apostrophe)
                {
                    StartFragment(FragmentManager, typeof(ConfigFragment));
                }
            }

            return true;
        }

        // バックキーが聞かないようにfalseを返却
        public override bool OnBackPressed()
        {
            return false;
        }

        // ログイン担当者コードスキャンのみできる
        public override void OnBarcodeDataReceived(BarcodeDataReceivedEvent_ dataReceivedEvent)
        {
            IList<BarcodeDataReceivedEvent_.BarcodeData_> listBarcodeData = dataReceivedEvent.BarcodeData;
            
            foreach (BarcodeDataReceivedEvent_.BarcodeData_ barcodeData in listBarcodeData)
            {
                // 担当者コードのみバーコードスキャン有効
                if (_TantoCodeEditText.IsFocused)
                {
                    this.Activity.RunOnUiThread(() => _TantoCodeEditText.Text = barcodeData.Data);
                }
            }
        }
    }
}
 