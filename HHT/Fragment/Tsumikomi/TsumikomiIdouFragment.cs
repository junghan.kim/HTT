﻿using Android.App;
using Android.Content;
using Android.OS;
using Android.Preferences;
using Android.Util;
using Android.Views;
using Android.Widget;
using Com.Beardedhen.Androidbootstrap;
using Com.Densowave.Bhtsdk.Barcode;
using HHT.Resources.DataHelper;
using HHT.Resources.Model;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;

namespace HHT
{
    public class TsumikomiIdouFragment : BaseFragment
    {
        private readonly string TAG = "TsumikaeIdouSakiFragment";
        private View view;
        private ISharedPreferences prefs;
        private ISharedPreferencesEditor editor;

        private int menuFlag;
        private string souko_cd, kitaku_cd, syuka_date, bin_no, course, tokuisaki_cd, todokesaki_cd;
        private TextView txtCase, txtOricon, txtIdosu, txtMail, txtSonota , txtFuteikei, txtHansoku, txtTc, txtKosu;
        private BootstrapButton btnConfirm;

        public override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
        }

        public override View OnCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
        {
            view = inflater.Inflate(Resource.Layout.fragment_tsumikomi_Idou, container, false);
            prefs = PreferenceManager.GetDefaultSharedPreferences(Context);
            editor = prefs.Edit();

            SetTitle("積込移動");

            txtCase = view.FindViewById<TextView>(Resource.Id.txt_tsumikae_case);
            txtOricon = view.FindViewById<TextView>(Resource.Id.txt_tsumikae_oricon);
            txtIdosu = view.FindViewById<TextView>(Resource.Id.txt_tsumikae_idosu);
            txtMail = view.FindViewById<TextView>(Resource.Id.txt_tsumikae_mail);
            txtSonota = view.FindViewById<TextView>(Resource.Id.txt_tsumikae_sonota);
            txtFuteikei = view.FindViewById<TextView>(Resource.Id.txt_tsumikae_futeikei);
            txtHansoku = view.FindViewById<TextView>(Resource.Id.txt_tsumikae_hansoku);
            txtTc = view.FindViewById<TextView>(Resource.Id.txt_tsumikae_tc);
            txtKosu = view.FindViewById<TextView>(Resource.Id.txt_tsumikae_kosu);

            btnConfirm = view.FindViewById<BootstrapButton>(Resource.Id.confirmButton);

            txtCase.Text = prefs.GetString("sk_case_su", "0");
            txtOricon.Text = prefs.GetString("sk_oricon_su", "0");
            txtIdosu.Text = prefs.GetString("sk_ido_su", "0");
            txtMail.Text = prefs.GetString("sk_mail_su", "0");
            txtSonota.Text = prefs.GetString("sk_sonota_su", "0");
            txtFuteikei.Text = prefs.GetString("sk_futeikei_su", "0");
            txtHansoku.Text = prefs.GetString("sk_hansoku_su", "0");
            txtTc.Text = prefs.GetString("sk_sonota_su", "0");
            txtKosu.Text = prefs.GetString("sk_ko_su", "0");
            
            souko_cd = prefs.GetString("souko_cd", "");
            kitaku_cd = prefs.GetString("kitaku_cd", "");
            syuka_date = prefs.GetString("syuka_date", "");
            bin_no = prefs.GetString("bin_no", "");
            course = prefs.GetString("course", "");
            tokuisaki_cd = prefs.GetString("tokuisaki_cd", "");
            todokesaki_cd = prefs.GetString("todokesaki_cd", "");

            view.FindViewById<TextView>(Resource.Id.tokuisakiNm).Text = prefs.GetString("tokuisaki_nm", "");


            menuFlag = prefs.GetInt("menuFlag", 1);
            
            btnConfirm.Text = "詳細";
            btnConfirm.Click += delegate{

                //増便商品未検品一覧
                List<TUMIKOMI170> mikenList = WebService.RequestTumikomi170(new Dictionary<string, string>
                {
                    {"kenpin_souko", souko_cd},
                    {"kitaku_cd", kitaku_cd},
                    {"syuka_date", prefs.GetString("syuka_date", "")},
                    {"tokuisaki_cd", prefs.GetString("tokuisaki_cd", "")},
                    {"todokesaki_cd", prefs.GetString("todokesaki_cd", "")},
                    {"bin_no", prefs.GetString("bin_no", "")},
                });

                if(mikenList.Count == 0)
                {
                    ShowDialog("エラー", "表示データがありません。", () => { });
                    return;
                }
                else
                {
                    //増便商品未検品一覧画面へ遷移。(sagyou13)
                    List<string> mikenStringList = new List<string>();

                    foreach(TUMIKOMI170 miken in mikenList)
                    {
                        mikenStringList.Add(miken.kamotsu_no);
                    }

                    Bundle bundle = new Bundle();
                    bundle.PutString("mikenList", JsonConvert.SerializeObject(mikenStringList));
                    StartFragment(FragmentManager, typeof(TsumikomiMikenFragment), bundle);
                }
            };
            
            return view;
        }

        public override bool OnKeyDown(Keycode keycode, KeyEvent paramKeyEvent)
        {            
            return true;
        }

        public override void OnBarcodeDataReceived(BarcodeDataReceivedEvent_ dataReceivedEvent)
        {
            IList<BarcodeDataReceivedEvent_.BarcodeData_> listBarcodeData = dataReceivedEvent.BarcodeData;

            foreach (BarcodeDataReceivedEvent_.BarcodeData_ barcodeData in listBarcodeData)
            {
                this.Activity.RunOnUiThread(() =>
                {
                    string kamotsuNo = barcodeData.Data;

                    MTumikomiProc result = WebService.RequestTumikomi200(new Dictionary<string, string>
                    {
                       {"pTerminalID", prefs.GetString("terminal_id","")},
                       {"pProgramID", "TUM"},
                       {"pSagyosyaCD", prefs.GetString("driver_cd", "")},
                       {"pSoukoCD", souko_cd},
                       {"pSyukaDate", prefs.GetString("syuka_date", "")},
                       {"pBinNo", prefs.GetString("bin_no", "")},
                       {"pCourse", prefs.GetString("course", "")},
                       {"pTokuisakiCD", prefs.GetString("tokuisaki_cd", "")},
                       {"pTodokesakiCD", prefs.GetString("todokesaki_cd", "")},
                       {"pKamotsuNo", kamotsuNo},
                       {"pHHT_No", prefs.GetString("hht_no","")},
                    });

                    if (result.poRet == "1")
                    {
                        ShowDialog("エラー", "貨物Noが見つかりません。", () => { });
                        return;
                    }

                    if (result.poRet == "3")
                    {
                        ShowDialog("エラー", "他の作業者が作業中です。", () => { });
                        return;
                    }

                    if (result.poRet == "4")
                    {
                        ShowDialog("エラー", "既に積込済です。", () => { });
                        return;
                    }

                    if (result.poRet == "5")
                    {
                        ShowDialog("エラー", "増便チェック済です。", () => { });
                        return;
                    }


                    if (result.poRet == "0" || result.poRet == "2")
                    {
                        SetMatehan(result.poCategory, 1);
                    }

                    if (result.poRet == "0")
                    {
                            Dictionary<string, string> param = GetProcParam(barcodeData.Data);

                            Log.Debug(TAG, "CreateTsumiFiles Start");

                            CreateTsumiFiles();

                            Log.Debug(TAG, "CreateTsumiFiles End");

                            //配車テーブルの該当コースの各数量を実績数で更新する
                            var updateResult = WebService.CallTumiKomiProc("210", param);

                            if (updateResult.poRet == "0" || updateResult.poRet == "99")
                            {
                                Activity.RunOnUiThread(() =>
                                {
                                    //	正常登録
                                    PlayBeepOk();
                                    ShowDialog("報告", "積込検品が\n完了しました。", () => { FragmentManager.PopBackStack(FragmentManager.GetBackStackEntryAt(0).Id, 0); });
                                });

                            }
                            else
                            {
                                ShowDialog("エラー", "表示データがありません", () => { FragmentManager.PopBackStack(FragmentManager.GetBackStackEntryAt(0).Id, 0); });
                                return;
                            }
                        }
                    });
            }
        }
        
        
        private void SetMatehan(string bunrui, int addValue)
        {
            string addedValue = ""; //加算した値を保存

            switch (bunrui)
            {
                case "01":
                    addedValue = (int.Parse(txtCase.Text) + addValue).ToString();
                    txtCase.Text = addedValue.ToString();
                    break;
                case "02":
                    addedValue = (int.Parse(txtOricon.Text) + addValue).ToString();
                    txtOricon.Text = addedValue.ToString();
                    break; // case 03は存在しない
                case "04":
                    addedValue = (int.Parse(txtIdosu.Text) + addValue).ToString();
                    txtIdosu.Text = addedValue.ToString();
                    break;
                case "07":
                    addedValue = (int.Parse(txtFuteikei.Text) + addValue).ToString();
                    txtFuteikei.Text = addedValue.ToString();
                    break;
                case "T":
                    addedValue = (int.Parse(txtTc.Text) + addValue).ToString();
                    txtTc.Text = addedValue.ToString();
                    break;
                default:
                    addedValue = (int.Parse(txtSonota.Text) + addValue).ToString();
                    txtSonota.Text = addedValue.ToString();
                    break;
            }

            txtKosu.Text = (int.Parse(txtKosu.Text) + addValue).ToString();
            editor.PutString("ko_su", txtKosu.Text);
            editor.Apply();
        }


        // 積込完了時に生成されるファイル（納品で使います。）
        private void CreateTsumiFiles()
        {
            // CRATE TUMIKOMI FILE
            // MAIN FILE
            List<MFile> mFiles = WebService.RequestTumikomi100(souko_cd, kitaku_cd, syuka_date, bin_no, course, tokuisaki_cd, todokesaki_cd);
            MFileHelper mFileHelper = new MFileHelper();
            mFileHelper.InsertALL(mFiles);

            PsFile psFile = WebService.RequestTumikomi180(souko_cd, syuka_date);
            PsFileHelper psFileHelper = new PsFileHelper();
            psFileHelper.Insert(psFile);
            
            // MAILBACK FILE 
            List<MbFile> mbFiles = WebService.RequestTumikomi140(souko_cd, kitaku_cd, syuka_date, bin_no, course);
            MbFileHelper mbFileHelper = new MbFileHelper();
            mbFileHelper.InsertAll(mbFiles);

            // SOUKO FILE
            SoFile soFile = WebService.RequestTumikomi160(souko_cd);
            SoFileHelper soFileHelper = new SoFileHelper();
            soFileHelper.DeleteAll();
            soFileHelper.Insert(soFile);

            // VENDOR FILE
            string nohin_date = DateTime.Now.ToString("yyyyMMdd");
            List<MateFile> mateFile = WebService.RequestTumikomi260();
            MateFileHelper mateFileHelper = new MateFileHelper();
            mateFileHelper.DeleteAll();
            mateFileHelper.InsertAll(mateFile);

            // TOKUISAKI FILE
            List<TokuiFile> tokuiFile = WebService.RequestTumikomi270();
            TokuiFileHelper tokuiFileHelper = new TokuiFileHelper();
            tokuiFileHelper.DeleteAll();
            tokuiFileHelper.InsertAll(tokuiFile);

            Log.Debug(TAG, "CreateTsumiFiles end");
        }

        // PROC専用のパラメータ設定
        private Dictionary<string, string> GetProcParam(string barcodeData)
        {
            return new Dictionary<string, string>
                        {
                            { "pTerminalID",  prefs.GetString("terminal_id","")},
                            { "pProgramID", "TUM" },
                            { "pSagyosyaCD", prefs.GetString("sagyousya_cd","") },
                            { "pSoukoCD",  souko_cd},
                            { "pSyukaDate", prefs.GetString("syuka_date", "")},
                            { "pBinNo", prefs.GetString("bin_no", "")},
                            { "pCourse", prefs.GetString("course", "") },
                            { "pTokuisakiCD", prefs.GetString("tokuisaki_cd", "") },
                            { "pTodokesakiCD", prefs.GetString("todokesaki_cd", "") },
                            { "pHHT_No", prefs.GetString("hht_no","") }
                        };
        }
    }
}