﻿using Android.Content;
using Android.OS;
using Android.Preferences;
using Android.Views;
using Android.Widget;

namespace HHT
{
    public class TsumikomiIdouMenuFragment : BaseFragment
    {
        private ISharedPreferences prefs;
        private ISharedPreferencesEditor editor;

        public override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
        }

        public override View OnCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
        {
            prefs = PreferenceManager.GetDefaultSharedPreferences(Context);
            editor = prefs.Edit();

            SetTitle("積込移動");
            SetFooterText("");

            var view = inflater.Inflate(Resource.Layout.fragment_menu_tsumikomi_idou, container, false);
            Button button1 = view.FindViewById<Button>(Resource.Id.btn_tsumikomiMenu_course);
            Button button2 = view.FindViewById<Button>(Resource.Id.btn_tsumikomiMenu_tsumikae);

            string kansen_kbn = prefs.GetString("kansen_kbn", "1");
            if (kansen_kbn == "1")
            {
                button1.Text = "1. 積替移動";
                button1.Click += delegate { GotoTsumikae(); };

                button2.Text = "2. 強制出発";
                button2.Click += delegate { /* menuFlag = 3, msg2  -> sagyou10 */ };
            }
            else
            {
                button1.Text = "1. コース変更"; /* menuFlag = 1, msg2  -> sagyou10 パスワード入力画面へ */
                button1.Click += delegate { StartFragment(FragmentManager, typeof(TsumikomiManagerFragment)); };

                button2.Text = "2. 積替移動"; // ido_sagyou1
                button2.Click += delegate { GotoTsumikae(); };
            }

            return view;
        }

        public override bool OnKeyDown(Keycode keycode, KeyEvent paramKeyEvent)
        {
            string kansen_kbn = prefs.GetString("kansen_kbn", "1");
            if (kansen_kbn == "1")
            {
                if (keycode == Keycode.Num1)
                {
                    GotoTsumikae();
                }
                else if (keycode == Keycode.Num2)
                {
                    /* menuFlag = 3, msg3*/
                }
            }
            else
            {
                if (keycode == Keycode.Num1)
                {
                    StartFragment(FragmentManager, typeof(TsumikomiManagerFragment));
                }
                else if (keycode == Keycode.Num2)
                {
                    GotoTsumikae();
                }
            }
            
            return true;
        }

        private void GotoTsumikae()
        {
            editor.PutBoolean("fromTsumikomi", true);
            editor.Apply();
            StartFragment(FragmentManager, typeof(TsumikaeIdouMotoFragment));
        }
    }
}