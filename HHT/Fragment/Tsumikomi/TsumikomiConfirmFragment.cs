﻿using System;
using System.Collections.Generic;
using System.Threading;
using Android.App;
using Android.Content;
using Android.OS;
using Android.Preferences;
using Android.Util;
using Android.Views;
using Android.Widget;
using Com.Beardedhen.Androidbootstrap;
using HHT.Resources.DataHelper;
using HHT.Resources.Model;

namespace HHT
{
    public class TsumikomiConfirmFragment : BaseFragment
    {
        private readonly string TAG = "TsumikomiConfirmFragment";
        
        ISharedPreferences prefs;
        ISharedPreferencesEditor editor;

        private View view;
        private EditText etKosu, etCarry, etKargo, etCard, etBara, etSonata;
        private BootstrapButton _CompleteButton;

        private string souko_cd, kitaku_cd, syuka_date, tokuisaki_cd, todokesaki_cd, bin_no, course;
        private int btvOkFlg2;
        
        public override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
        }
        public override View OnCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
        {
            view = inflater.Inflate(Resource.Layout.fragment_tsumikomi_confirm, container, false);
            prefs = PreferenceManager.GetDefaultSharedPreferences(Context);
            editor = prefs.Edit();

            SetTitle("積込検品");
            
            view.FindViewById<TextView>(Resource.Id.txt_tsumikomiWork_tokuisakiNm).Text = prefs.GetString("tokuisaki_nm", "");
            etKosu = view.FindViewById<EditText>(Resource.Id.sekisaiKosu);
            etCarry = view.FindViewById<EditText>(Resource.Id.carry);
            etKargo = view.FindViewById<EditText>(Resource.Id.kargoCar);
            etCard = view.FindViewById<EditText>(Resource.Id.card);
            etBara = view.FindViewById<EditText>(Resource.Id.bara);
            etSonata = view.FindViewById<EditText>(Resource.Id.sonota);

            _CompleteButton = view.FindViewById<BootstrapButton>(Resource.Id.completeButton);
            _CompleteButton.Click += delegate { Complete(); };
            
            souko_cd = prefs.GetString("souko_cd", "");
            kitaku_cd = prefs.GetString("kitaku_cd", "");
            syuka_date = prefs.GetString("syuka_date", "");
            tokuisaki_cd = prefs.GetString("tokuisaki_cd", "");
            todokesaki_cd = prefs.GetString("todokesaki_cd", "");
            bin_no = prefs.GetString("bin_no", "");
            course = prefs.GetString("course", "");

            btvOkFlg2 = 0;

            try
            {
                ((MainActivity)this.Activity).ShowProgress("少々お待ちください。");

                new Thread(new ThreadStart(delegate {

                    Activity.RunOnUiThread(() =>
                    {
                        Thread.Sleep(2000);

                        Dictionary<string, string> param = new Dictionary<string, string>
                        {
                            { "kenpin_souko", souko_cd },
                            { "kitaku_cd", kitaku_cd },
                            { "syuka_date", syuka_date },
                            { "bin_no", bin_no },
                            { "course", course },
                            { "tokuisaki_cd", tokuisaki_cd },
                            { "todokesaki_cd", todokesaki_cd }
                        };

                        List<TUMIKOMI110> tumikomi110List = WebService.RequestTumikomi110(param);

                        int sekisaiKosu = WebService.RequestTumikomi120(param);
                        etKosu.Text = sekisaiKosu.ToString();

                        for (int i = 0; i < tumikomi110List.Count; i++)
                        {
                            if (i == 0)
                            {
                                view.FindViewById<TextView>(Resource.Id.categoryNm1).Text = tumikomi110List[0].category_nm;
                                etCarry.Text = tumikomi110List[0].cnt;
                            }
                            else if (i == 1)
                            {
                                view.FindViewById<TextView>(Resource.Id.categoryNm2).Text = tumikomi110List[1].category_nm;
                                etKargo.Text = tumikomi110List[1].cnt;
                            }
                            else if (i == 2)
                            {
                                view.FindViewById<TextView>(Resource.Id.categoryNm3).Text = tumikomi110List[2].category_nm;
                                etCard.Text = tumikomi110List[2].cnt;
                            }
                            else if (i == 3)
                            {
                                view.FindViewById<TextView>(Resource.Id.categoryNm4).Text = tumikomi110List[3].category_nm;
                                etBara.Text = tumikomi110List[3].cnt;
                            }
                            else if (i == 4)
                            {
                                view.FindViewById<TextView>(Resource.Id.categoryNm5).Text = tumikomi110List[4].category_nm;
                                etSonata.Text = tumikomi110List[4].cnt;
                            }
                        }
                    }
                    );
                    Activity.RunOnUiThread(() => ((MainActivity)this.Activity).DismissDialog());
                }
                )).Start();
            }
            catch
            {
                etKosu.Text = "0";
            }

            return view;
        }

        private void Complete()
        {
            Log.Debug(TAG, "CreateTsumiFiles Start");

            CreateTsumiFiles();

            Log.Debug(TAG, "CreateTsumiFiles End");

            /**　プロシージャ呼び出し **/

            Dictionary<string, string> param = GetProcParam();

            if (btvOkFlg2 == 0)
            {    
                var result = WebService.CallTumiKomiProc("220", param);

                if (result.poRet == "0")
                {
                    btvOkFlg2 = 1;
                }
                else if (result.poRet == "8")
                {
                    ShowDialog("エラー", "更新できませんでした。\n再度行して下さい。", () => { });
                    return;
                }  
            }

            //配車テーブルの該当コースの各数量を実績数で更新する
            var updateResult = WebService.CallTumiKomiProc("210", param);

            if (updateResult.poRet == "0")
            {
                //完全OK　トップ画面へ
                PlayBeepOk();
                ShowDialog("報告", "積込検品が\n完了しました。", () => { FragmentManager.PopBackStack(FragmentManager.GetBackStackEntryAt(0).Id, 0); });
            }
            else if(updateResult.poRet == "99")
            {
                //残りあり 積込指定画面へ
                PlayBeepOk();
                ShowDialog("報告", "積込検品が\n完了しました。", () => { FragmentManager.PopBackStack(FragmentManager.GetBackStackEntryAt(1).Id, 0); });
            }
            else
            {
                // NG beep
                ShowDialog("エラー", "更新できませんでした。\n再度行して下さい。", () => { });
                btvOkFlg2 = 1;
            }
        }

        public override bool OnKeyDown(Keycode keycode, KeyEvent paramKeyEvent)
        {
            if (keycode == Keycode.F1)
            {
                
            }
            else if (keycode == Keycode.F3)
            {
                
            }

            return true;
        }

        public override bool OnBackPressed()
        {
            return true;
        }
        
        // 積込完了時に生成されるファイル（納品で使います。）
        private void CreateTsumiFiles()
        {
            // CRATE TUMIKOMI FILE
            // MAIN FILE
            List<MFile> mFiles = WebService.RequestTumikomi100(souko_cd, kitaku_cd, syuka_date, bin_no, course, tokuisaki_cd, todokesaki_cd);
            MFileHelper mFileHelper = new MFileHelper();
            mFileHelper.InsertALL(mFiles);

            // PASSWORD Info
            PsFile psFile = WebService.RequestTumikomi180(souko_cd, syuka_date);
            PsFileHelper psFileHelper = new PsFileHelper();
            psFileHelper.Insert(psFile);

            // MAILBACK FILE 
            List<MbFile> mbFiles = WebService.RequestTumikomi140(souko_cd, kitaku_cd, syuka_date, bin_no, course);
            MbFileHelper mbFileHelper = new MbFileHelper();
            mbFileHelper.DeleteAll();
            mbFileHelper.InsertAll(mbFiles);

            // SOUKO FILE
            SoFile soFile = WebService.RequestTumikomi160(souko_cd);
            SoFileHelper soFileHelper = new SoFileHelper();
            soFileHelper.DeleteAll();
            soFileHelper.Insert(soFile);
            
            // VENDOR FILE
            string nohin_date = DateTime.Now.ToString("yyyyMMdd");
            List<MateFile> mateFile = WebService.RequestTumikomi260();
            MateFileHelper mateFileHelper = new MateFileHelper();
            mateFileHelper.DeleteAll();
            mateFileHelper.InsertAll(mateFile);

            // TOKUISAKI FILE
            List<TokuiFile> tokuiFile = WebService.RequestTumikomi270();
            TokuiFileHelper tokuiFileHelper = new TokuiFileHelper();
            tokuiFileHelper.DeleteAll();
            tokuiFileHelper.InsertAll(tokuiFile);

            Log.Debug(TAG, "CreateTsumiFiles end");
        }

        // PROC専用のパラメータ設定
        private Dictionary<string, string> GetProcParam()
        {
            return new Dictionary<string, string>
                        {
                            { "pTerminalID",  prefs.GetString("terminal_id","")},
                            { "pProgramID", "TUM" },
                            { "pSagyosyaCD", prefs.GetString("sagyousya_cd","") },
                            { "pSoukoCD",  souko_cd},
                            { "pSyukaDate", syuka_date},
                            { "pBinNo", bin_no},
                            { "pCourse", course },
                            { "pTokuisakiCD", tokuisaki_cd },
                            { "pTodokesakiCD", todokesaki_cd },
                            { "pHHT_No", prefs.GetString("hht_no","") }
                        };
        }

    }
}
 