﻿
using Android.App;
using Android.Content;
using Android.OS;
using Android.Preferences;
using Android.Views;
using Android.Widget;
using Com.Beardedhen.Androidbootstrap;
using HHT.Resources.DataHelper;
using System.Collections.Generic;

namespace HHT
{
    public class NohinMailBagPasswordFragment : BaseFragment
    {
        private View view;
        private EditText etPassword;
        private ISharedPreferences prefs;
        private ISharedPreferencesEditor editor;
        
        public override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
        }

        public override View OnCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
        {
            view = inflater.Inflate(Resource.Layout.fragment_nohin_mailBag_password, container, false);
            prefs = PreferenceManager.GetDefaultSharedPreferences(Context);
            editor = prefs.Edit();

            SetTitle("メールバック");

            etPassword = view.FindViewById<EditText>(Resource.Id.et_nohinMailbagPwd_password);

            string menuFlag = prefs.GetString("menu_flg", "1");
            
            BootstrapButton button = view.FindViewById<BootstrapButton>(Resource.Id.btn_nohinMailbagPwd_confirm);
            button.Click += delegate {

                List<string> passList = PsFileHelper.GetPassword();

                if (passList != null)
                {
                    //bool existFlag = false;

                    if(passList.Count == 0 || etPassword.Text == "")
                    {
                        if (menuFlag == "1")
                        {
                            editor.PutBoolean("mailBagFlag", true);
                            editor.Apply();

                            PlayBeepOk();
                            ShowDialog("報告", "メールバッグの\n納品が完了しました。", () => { FragmentManager.PopBackStack(FragmentManager.GetBackStackEntryAt(2).Id, 0); });
                        }
                        else
                        {
                            PlayBeepOk();
                            ShowDialog("報告", "納品検品が\n完了しました。\n\nお疲れ様でした！", () => { FragmentManager.PopBackStack(FragmentManager.GetBackStackEntryAt(2).Id, 0); });
                        }
                    }


                    foreach(string pass in passList)
                    {
                        if (etPassword.Text.Equals(pass) || (etPassword.Text == "" && pass == null))
                        {
                            //existFlag = true;

                            if (menuFlag == "1")
                            {
                                editor.PutBoolean("mailBagFlag", true);
                                editor.Apply();

                                PlayBeepOk();
                                ShowDialog("報告", "メールバッグの\n納品が完了しました。", () => { FragmentManager.PopBackStack(FragmentManager.GetBackStackEntryAt(2).Id, 0); });
                            }
                            else
                            {
                                PlayBeepOk();
                                ShowDialog("報告", "納品検品が\n完了しました。\n\nお疲れ様でした！", () => { FragmentManager.PopBackStack(FragmentManager.GetBackStackEntryAt(2).Id, 0); });
                            }

                            break;

                        }
                    }

                    // パスワードが存在しない場合
                    /*
                    if(existFlag == false)
                    {
                        ShowDialog("報告", "パスワードが違います。", () => { etPassword.Text = ""; etPassword.RequestFocus(); });
                    }
                    */
                }
                else
                {
                    ShowDialog("報告", "パスワード情報が存在しません。" +
                        "\n管理者に連絡してください。", () => { etPassword.Text = ""; etPassword.RequestFocus(); });
                }
            };
            
            return view;
        }

        public override bool OnKeyDown(Keycode keycode, KeyEvent paramKeyEvent)
        {
            if (keycode == Keycode.Num1)
            {
                StartFragment(FragmentManager, typeof(KosuMenuFragment));
            }
            
            return true;
        }

    }
}