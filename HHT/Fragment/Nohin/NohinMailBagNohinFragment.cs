﻿using Android.Content;
using Android.OS;
using Android.Preferences;
using Android.Util;
using Android.Views;
using Android.Widget;
using Com.Beardedhen.Androidbootstrap;
using Com.Densowave.Bhtsdk.Barcode;
using HHT.Resources.DataHelper;
using HHT.Resources.Model;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace HHT
{
    public class NohinMailBagNohinFragment : BaseFragment
    {
        private readonly string TAG = "NohinMailBagNohinFragment";
        private View view;
        private ISharedPreferences prefs;
        private ISharedPreferencesEditor editor;

        private BootstrapEditText etMailBagNumber;
        private string tokuisakiCd;
        private string todokesakiCd;
        private int mailbackCnt;

        private bool endFlag;
        
        public override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
        }

        public override View OnCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
        {
            view = inflater.Inflate(Resource.Layout.fragment_nohin_mailBag_nohin, container, false);
            prefs = PreferenceManager.GetDefaultSharedPreferences(Context);
            editor = prefs.Edit();
            
            SetTitle("メールバック納品");
            tokuisakiCd = prefs.GetString("tokuisaki_cd", "");
            todokesakiCd = prefs.GetString("todokesaki_cd", "");

            endFlag = false;

            // 得意先名設定
            view.FindViewById<TextView>(Resource.Id.text_tokuisaki_name).Text = prefs.GetString("tokuisaki_nm", "");

            // 納品したメールバック数取得
            etMailBagNumber = view.FindViewById<BootstrapEditText>(Resource.Id.et_nohinMailBagNohin_mailbagNumber);
            etMailBagNumber.Text = SndNohinMailHelper.SelectByPk(tokuisakiCd, todokesakiCd).Count.ToString();
            
            // メールバッグ件数取得
            mailbackCnt = MbFileHelper.GetMailbackCount(tokuisakiCd, todokesakiCd);
            
            // 解除ボタン
            BootstrapButton kaizoButton = view.FindViewById<BootstrapButton>(Resource.Id.btn_nohinMailBagNohin_kaizo);
            kaizoButton.Click += delegate {
                editor.PutString("menu_flg", "1");
                editor.Apply();
                StartFragment(FragmentManager, typeof(NohinMailBagPasswordFragment));
            };

            // 解除ボタン
            BootstrapButton muButton = view.FindViewById<BootstrapButton>(Resource.Id.btn_nohinMailBagNohin_mu);
            muButton.Click += delegate { CompleteMailNohin(); };
            if (mailbackCnt != 0) muButton.Enabled = false; // 納品予定メールバックが存在する場合、非活性

            return view;
        }

        private void CompleteMailNohin()
        {
            ShowDialog("警告", "メールバッグ納品業務を終了しますか？", (okFlag) => {
                if (okFlag)
                {
                    Log.Debug(TAG, "F3 return pushed : " + prefs.GetString("tokuisaki_cd", "") + ", " + prefs.GetString("tokuisaki_nm", ""));
                    editor.PutBoolean("mailBagFlag", true);
                    editor.Apply();
                    FragmentManager.PopBackStack();
                }
            });
        }

        public override bool OnKeyDown(Keycode keycode, KeyEvent paramKeyEvent)
        {
            if (keycode == Keycode.F1)
            {
                StartFragment(FragmentManager, typeof(NohinMailBagPasswordFragment));
            }
            if (keycode == Keycode.F3)
            {
                if (mailbackCnt == 0)
                {
                    CompleteMailNohin();
                }
            }

            return true;
        }

        public override bool OnBackPressed()
        {
            return false;
        }

        public override void OnBarcodeDataReceived(BarcodeDataReceivedEvent_ dataReceivedEvent)
        {
            if (endFlag) return;
 
            IList<BarcodeDataReceivedEvent_.BarcodeData_> listBarcodeData = dataReceivedEvent.BarcodeData;

            foreach (BarcodeDataReceivedEvent_.BarcodeData_ barcodeData in listBarcodeData)
            {
                Task.Run(() => {
                    try
                    {
                        string mailbackCd = barcodeData.Data;

                        if (MbFileHelper.HasExistMailBagData(tokuisakiCd, todokesakiCd, mailbackCd))
                        {
                            string btvTokuisaki = mailbackCd.Substring(1, 4);
                            string btvTodokesaki = mailbackCd.Substring(5, 4);

                            this.Activity.RunOnUiThread(() =>
                            {
                                // バリデーションチェック
                                // 得意先、届先が一致するか確認
                                if (tokuisakiCd != btvTokuisaki || todokesakiCd != btvTodokesaki)
                                {
                                    ShowDialog("エラー", "納入先店舗が違います。", () => { });
                                    return;
                                }

                                // すでにスキャン済みか確認
                                if (SndNohinMailHelper.HasSameMailBack(tokuisakiCd, todokesakiCd, mailbackCd))
                                {
                                    ShowDialog("報告", "既にスキャン済みです。", () => { });
                                    return;
                                }
                            });

                            // メールバック納品処理
                            ExecuteNohinMailBack(mailbackCd);

                        }
                        else
                        {
                            ShowDialog("エラー", "該当メールバッグはありません。", () => { });
                            return;
                        }
                    }
                    catch
                    {
                        ShowDialog("エラー", "該当メールバッグはありません。", () => { });
                        return;
                    }
                });
            }
        }
        
        /// <summary>
        /// メールバック納品処理
        /// </summary>
        /// <param name="mailbackBarcode"></param>
        private void ExecuteNohinMailBack(string mailbackBarcode)
        {
            // メールバック登録
            SndNohinMail sndNohinMail = new SndNohinMail
            {
                wPackage = "01",
                wTerminalID = prefs.GetString("terminal_id", ""), //Handy: serialId
                wProgramID = prefs.GetString("program_id", "NOH"), //JOB: program_id
                wSagyosyaCD = prefs.GetString("sagyousya_cd", "99999"),
                wSoukoCD = prefs.GetString("souko_cd", ""), //JOB: noh_soukoCd
                wHaisoDate = prefs.GetString("haiso_date", ""), // noh_syukaDate
                wBinNo = prefs.GetString("bin_no", ""), //JOB: noh_binNo
                wCourse = prefs.GetString("course", ""),
                wDriverCD = prefs.GetString("driver_cd", ""),
                wTokuisakiCD = prefs.GetString("tokuisaki_cd", ""),
                wTodokesakiCD = prefs.GetString("todokesaki_cd", ""),
                wKanriNo = mailbackBarcode,
                wVendorCd = prefs.GetString("vendor_cd", ""),
                wMateVendorCd = "",
                wSyukaDate = prefs.GetString("haiso_date", ""),
                wButsuryuNo = "",
                wKamotuNo = "",
                wMatehan = "",
                wMatehanSu = "0",
                wHHT_no = prefs.GetString("hht_no", ""),
                wNohinKbn = "",
                wKaisyuKbn = "",
                wTenkanState = "00",
                wSakiTokuisakiCD = "",
                wSakiTodokesakiCD = "",
                wNohinDate = prefs.GetString("nohin_date", ""),
                wNohinTime = prefs.GetString("nohin_time", ""),
                wNohinEndDate = DateTime.Now.ToString("yyyyMMdd"),
                wNohinEndTime = DateTime.Now.ToString("HHmm")
            };
            SndNohinMailHelper.Insert(sndNohinMail);

            // 正常の場合、プラス１
            int mail_bag_su = SndNohinMailHelper.SelectByPk(tokuisakiCd, todokesakiCd).Count;
            
            this.Activity.RunOnUiThread(() =>
            {
                etMailBagNumber.Text = mail_bag_su.ToString();
                // 最大メールバック数になった場合、完了画面へ
                if (mail_bag_su >= mailbackCnt)
                {
                    endFlag = true;

                    editor.PutBoolean("mailBagFlag", true);
                    editor.PutString("menu_flg", "1");
                    editor.Apply();

                    PlayBeepOk();
                    ShowDialog("報告", "メールバッグの\n納品が完了しました。", () => {
                        FragmentManager.PopBackStack(FragmentManager.GetBackStackEntryAt(2).Id, 0);
                    });
                }
            });
            
        }
    }
}