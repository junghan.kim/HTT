﻿using Android.Content;
using Android.OS;
using Android.Preferences;
using Android.Util;
using Android.Views;
using Android.Widget;
using Com.Beardedhen.Androidbootstrap;
using Com.Densowave.Bhtsdk.Barcode;
using HHT.Common;
using HHT.Resources.DataHelper;
using HHT.Resources.Model;
using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace HHT
{
    public class NohinMailBagKaisyuFragment : BaseFragment
    {
        private readonly string TAG = "NohinMailBagKaisyuFragment";
        private View view;
        private ISharedPreferences prefs;
        private ISharedPreferencesEditor editor;
        private List<string> arrMailBag;
        private int mail_bag;
        private EditText etKaisyuMail;

        private SndNohinMailKaisyuHelper sndNohinMailKaisyuHelper;

        private string tokuisaki_cd;
        private string todokesaki_cd;

        public override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
        }

        public override View OnCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
        {
            view = inflater.Inflate(Resource.Layout.fragment_nohin_mailBag_kaisyu, container, false);
            prefs = PreferenceManager.GetDefaultSharedPreferences(Context);
            editor = prefs.Edit();

            InitComponent();

            return view;
        }

        private void InitComponent()
        {
            SetTitle("メールバック回収");
            SetFooterText("");

            TextView tvTokuisaki = view.FindViewById<TextView>(Resource.Id.txt_nohinKaisyuMail_tokuisakiNm);
            tvTokuisaki.Text = prefs.GetString("tokuisaki_nm", "");

            TextView tvTodokesaki = view.FindViewById<TextView>(Resource.Id.txt_nohinKaisyuMail_todokisakiNm);
            tvTodokesaki.Text = prefs.GetString("todokesaki_nm", "");

            etKaisyuMail = view.FindViewById<EditText>(Resource.Id.et_nohinKaisyuMail_kaisyuMailbag);

            BootstrapButton btnConfirm = view.FindViewById<BootstrapButton>(Resource.Id.et_nohinKaisyuMail_Confirm);
            btnConfirm.Click += delegate { ConfirmMailBagKaisyu(); };

            tokuisaki_cd = prefs.GetString("tokuisaki_cd", "");
            todokesaki_cd = prefs.GetString("todokesaki_cd", "");
            sndNohinMailKaisyuHelper = new SndNohinMailKaisyuHelper();

            // メール回収情報取得
            List<SndNohinMailKaisyu> temp = sndNohinMailKaisyuHelper.SelectByPK(tokuisaki_cd, todokesaki_cd);

            etKaisyuMail.Text = temp.Count.ToString();
            mail_bag = temp.Count;
            arrMailBag = new List<string>();

            foreach (SndNohinMailKaisyu mailkaisyu in temp)
            {
                arrMailBag.Add(mailkaisyu.wKanriNo);
            }

        }

        public override bool OnKeyDown(Keycode keycode, KeyEvent paramKeyEvent)
        {
            return true;
        }

        public override bool OnBackPressed()
        {
            return true;
        }

        private void ConfirmMailBagKaisyu()
        {
            this.Activity.RunOnUiThread(() => {
                ShowDialog("確認", "よろしいですか？", (okFlag) => {
                    if (okFlag)
                    {
                        // 送信ファイル作成
                        foreach (string mailbag in arrMailBag)
                        {
                            appendFile(mailbag);
                        }

                        Log.Debug(TAG, "COMMIT");
                        FragmentManager.PopBackStack();
                    }
                });
            });
        }

        public override void OnBarcodeDataReceived(BarcodeDataReceivedEvent_ dataReceivedEvent)
        {
            foreach (BarcodeDataReceivedEvent_.BarcodeData_ barcodeData in dataReceivedEvent.BarcodeData)
            {
                Task.Run(() => {
                    try
                    {
                        string data = barcodeData.Data;

                        if (data[0].ToString() != "M" || data.Length < 9)
                        {
                            this.Activity.RunOnUiThread(() => { ShowDialog("エラー", "メールバッグではありません。", () => { }); });
                            Log.Debug(TAG, "MAIN BAG KAISYU INPUT_ERR1:" + data);
                            return;
                        }

                        string btvTokuisaki = data.Substring(1, 4);
                        string btvTodokesaki = data.Substring(5, 4);

                        // スキャン済みチェック
                        foreach (string mailkaisyu in arrMailBag)
                        {
                            if (mailkaisyu == data)
                            {
                                this.Activity.RunOnUiThread(() => { ShowDialog("エラー", "既にスキャン済みです。", () => { }); });
                                return;
                            }
                        }

                        // 納品メールバッグ重複チェック
                        bool hasData = MbFileHelper.HasExistMailBagData(tokuisaki_cd, todokesaki_cd, data);
                        if (hasData)
                        {
                            this.Activity.RunOnUiThread(() => {
                                ShowDialog("警告", "回収メールバッグと納入メールバッグが同じです\nよろしいですか？", (flag) => {
                                    if (flag)
                                    {
                                        CheckMailBack(data);
                                    }
                                });
                            });
                        }
                        else
                        {
                            CheckMailBack(data);
                        }
                    }
                    catch(Exception e)
                    {
                        this.Activity.RunOnUiThread(() => {
                            ShowDialog("エラー", "エラーが発生しました。\n" + e.Message , () => {});
                        });
                    }
                });
            }
        }

        private void CheckMailBack(string barcode)
        {
            string btvTokuisaki = barcode.Substring(1, 4);
            string btvTodokesaki = barcode.Substring(5, 4);

            string btvKey1 = btvTokuisaki + btvTodokesaki;
            string btvKey2 = prefs.GetString("tokuisaki_cd", "") + prefs.GetString("todokesaki_cd", "");
            
            if (btvKey1 != btvKey2)
            {
                this.Activity.RunOnUiThread(() => {
                    ShowDialog("エラー", "納入先店舗が違います。", () => { });
                });
                
                Log.Debug(TAG, "納入先店舗が違います  btvKey1 :" + btvKey1 + "  btvKey2 :" + btvKey2);
                return;
            }

            bool result = PlusMailBack(barcode);
            if (!result)
            {
                this.Activity.RunOnUiThread(() => {
                    ShowDialog("エラー", "既にスキャン済みです。", () => { });
                });
                
                Log.Debug(TAG, "既にスキャン済みです。 barcode :" + barcode);
                return;
            }
        }

        private bool PlusMailBack(string data)
        {
            int idx = arrMailBag.FindIndex(x => x == data);
            if (idx == -1)
            {
                arrMailBag.Add(data);
                mail_bag++;

                Activity.RunOnUiThread(() =>
                {
                    etKaisyuMail.Text = mail_bag.ToString();
                });

                return true;
            }
            else
            {
                this.Activity.RunOnUiThread(() => {
                    ShowDialog("エラー", "既にスキャン済みです。", () => { });
                });
                
                Log.Debug(TAG, "既にスキャン済みです。 data :" + data);
                return false;
            }
        }

        private void appendFile(string mailbag)
        {
            // レコード作成用　値取得
            SndNohinMailKaisyu sndNohinMailKaisyu = new SndNohinMailKaisyu
            {
                wPackage = "00",
                wTerminalID = prefs.GetString("terminal_id", ""),
                wProgramID = "NOH",
                wSagyosyaCD = prefs.GetString("sagyousya_cd", ""),
                wSoukoCD = prefs.GetString("souko_cd", ""),
                wHaisoDate = prefs.GetString("haiso_date", ""),
                wBinNo = prefs.GetString("bin_no", ""),
                wCourse = prefs.GetString("course", ""),
                wDriverCD = prefs.GetString("driver_cd", ""),
                wTokuisakiCD = prefs.GetString("tokuisaki_cd", ""),
                wTodokesakiCD = prefs.GetString("todokesaki_cd", ""),
                wKanriNo = mailbag,
                wVendorCd = prefs.GetString("vendor_cd", ""),
                wMateVendorCd = "",
                wSyukaDate = "0",
                wButsuryuNo = "",
                wKamotuNo = "",
                wMatehan = "",
                wMatehanSu = "0",
                wHHT_no = prefs.GetString("hht_no", ""),
                wNohinKbn = "1", // 商品回収
                wKaisyuKbn = "0",
                wTenkanState = "00",
                wSakiTokuisakiCD = "",
                wSakiTodokesakiCD = "",
                wNohinDate = prefs.GetString("nohin_date", ""),
                wNohinTime = prefs.GetString("nohin_time", ""),
                wNohinEndDate = DateTime.Now.ToString("yyyyMMdd"),
                wNohinEndTime = DateTime.Now.ToString("HHmm")
            };
            
            //DEF_MAIL_KAI
            sndNohinMailKaisyuHelper.Insert(sndNohinMailKaisyu);

        }
    }
}