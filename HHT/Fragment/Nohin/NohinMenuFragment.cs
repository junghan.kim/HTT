﻿using Android.Content;
using Android.OS;
using Android.Preferences;
using Android.Util;
using Android.Views;
using Android.Widget;
using HHT.Resources.DataHelper;
using HHT.Resources.Model;
using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Threading;
using System.Threading.Tasks;

namespace HHT
{
    public class NohinMenuFragment : BaseFragment
    {
        private string TAG = "NohinMenuFragment";
        private View view;
        private ISharedPreferences prefs;
        private ISharedPreferencesEditor editor;

        private bool hasMailBagData;
        private string tokuisaki_cd, todokesaki_cd;

        List<SndNohinMail> mailList;
        List<SndNohinMailKaisyu> mailKaisyuList;
        List<SndNohinMate> mateList;
        List<SndNohinWork> workList;
        List<SndNohinSyohinKaisyu> syohinKaisyuList;

        public override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
        }

        public override View OnCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
        {
            view = inflater.Inflate(Resource.Layout.fragment_menu_nohin, container, false);
            prefs = PreferenceManager.GetDefaultSharedPreferences(Context);
            editor = prefs.Edit();

            SetTitle("納品検品");
            SetFooterText("");

            bool isDone = prefs.GetBoolean("mailBagFlag", false);
            tokuisaki_cd = prefs.GetString("tokuisaki_cd", "");
            todokesaki_cd = prefs.GetString("todokesaki_cd", "");

            MbFileHelper mbFileHelper = new MbFileHelper();
            hasMailBagData = mbFileHelper.HasExistMailBagData();

            SndNohinWorkHelper sndNohinWorkHelper = new SndNohinWorkHelper();
            int nohinCount = sndNohinWorkHelper.SelectAll().Count;

            Button button1 = view.FindViewById<Button>(Resource.Id.btn_nohinMenu_mailNohin);
            button1.Click += delegate {
                
                if (prefs.GetBoolean("mailBagFlag", false))
                {
                    ShowDialog("報告", "メールバッグ納品処理は終了しています。", () => { });
                }
                else
                {
                    StartFragment(FragmentManager, typeof(NohinMailBagNohinFragment));
                }
            };

            Button button2 = view.FindViewById<Button>(Resource.Id.btn_nohinMenu_nohin);
            button2.Click += delegate {
                bool errorFlag = false;
                
                if (!prefs.GetBoolean("mailBagFlag", false))
                {
                    Log.Debug(TAG, "メールバッグ納品処理が終了していません。");
                    errorFlag = true;
                    ShowDialog("報告", "メールバッグ納品処理が終了していません。", () => { });
                }
                else if (new SndNohinWorkHelper().SelectNohinWorByPk(tokuisaki_cd, todokesaki_cd).Count != 0)
                {
                    Log.Debug(TAG, "納品処理は終了しています。");
                    errorFlag = true;
                    ShowDialog("報告", "納品処理は終了しています。", () => { });
                }
                
                if (errorFlag == false)
                    StartFragment(FragmentManager, typeof(NohinWorkFragment));

            };

            Button button3 = view.FindViewById<Button>(Resource.Id.btn_nohinMenu_kaisyu); // 回収業務
            button3.Click += delegate {
                bool errorFlag = false;

                /*
                if (!prefs.GetBoolean("mailBagFlag", false))
                {
                    Log.Debug(TAG, "メールバッグ納品処理が終了していません。");
                    errorFlag = true;
                    CommonUtils.AlertDialog(view, "確認", "メールバッグ納品処理が終了していません。", () => { return; });
                }
                else 
                if (!prefs.GetBoolean("nohinWorkEndFlag", false))
                {
                    Log.Debug(TAG, "納品処理が終了していません。");
                    errorFlag = true;
                    CommonUtils.AlertDialog(view, "確認", "納品処理が終了していません。", () => { return; });
                }
                else if (prefs.GetBoolean("kaisyuEndFlag", false))
                {
                    Log.Debug(TAG, "納品処理が終了していません。");
                    errorFlag = true;
                    CommonUtils.AlertDialog(view, "確認", "納品処理が終了していません。", () => { return; });
                }
                */
                if (errorFlag == false)
                {
                    editor.PutBoolean("searchFlag", false);
                    editor.Apply();
                    StartFragment(FragmentManager, typeof(NohinKaisyuMenuFragment));
                }
            };

            Button button4 = view.FindViewById<Button>(Resource.Id.btn_nohinMenu_mailKaisyu);
            button4.Click += delegate {
                bool errorFlag = false;

                /*
                if (!prefs.GetBoolean("mailBagFlag", false))
                {
                    Log.Debug(TAG, "メールバッグ納品処理が終了していません。");
                    errorFlag = true;
                    CommonUtils.AlertDialog(view, "確認", "メールバッグ納品処理が終了していません。", () => { return; });
                } else 
                if (!prefs.GetBoolean("kaisyuEndFlag", false))
                {
                    Log.Debug(TAG, "回収処理が終了していません。");
                    errorFlag = true;
                    CommonUtils.AlertDialog(view, "確認", "回収処理が終了していません。", () => { return; });
                }
                else if (prefs.GetBoolean("mailKaisyuEndFlag", false))
                {
                    Log.Debug(TAG, "メールバッグ回収処理は終了しています。");
                    errorFlag = true;
                    CommonUtils.AlertDialog(view, "確認", "メールバッグ回収処理は終了しています。", () => { return; });
                }
                */
                if (errorFlag == false)
                    StartFragment(FragmentManager, typeof(NohinMailBagKaisyuFragment)); }; // sagyou2

            return view;
        }

        public override bool OnKeyDown(Keycode keycode, KeyEvent paramKeyEvent)
        {
            if (keycode == Keycode.Num1)
            {
                if (prefs.GetBoolean("mailBagFlag", false))
                {
                    ShowDialog("報告", "メールバッグ納品処理は終了しています。", () => { });
                }
                else
                {
                    StartFragment(FragmentManager, typeof(NohinMailBagNohinFragment));
                }
            }
            else if (keycode == Keycode.Num2)
            {
                bool errorFlag = false;

                if (!prefs.GetBoolean("mailBagFlag", false))
                {
                    Log.Debug(TAG, "メールバッグ納品処理が終了していません。");
                    errorFlag = true;
                    ShowDialog("報告", "メールバッグ納品処理が終了していません。", () => { });
                }
                else if (new SndNohinWorkHelper().SelectNohinWorByPk(tokuisaki_cd, todokesaki_cd).Count != 0)
                {
                    Log.Debug(TAG, "納品処理は終了しています。");
                    errorFlag = true;
                    ShowDialog("報告", "納品処理は終了しています。", () => { });
                }

                if (errorFlag == false)
                    StartFragment(FragmentManager, typeof(NohinWorkFragment));
            }
            else if (keycode == Keycode.Num3)
            {
                StartFragment(FragmentManager, typeof(MatehanSelectFragment));
            }
            else if (keycode == Keycode.Num4)
            {
                StartFragment(FragmentManager, typeof(MatehanSelectFragment));
            }

            return true;
        }

        private bool HasSendData()
        {
            SndNohinMailHelper mailHelper = new SndNohinMailHelper();
            SndNohinMailKaisyuHelper mailKaisyuHelper = new SndNohinMailKaisyuHelper();
            SndNohinMateHelper mateHelper = new SndNohinMateHelper();
            SndNohinWorkHelper workHelper = new SndNohinWorkHelper();
            SndNohinSyohinKaisyuHelper syohinKaisyuHelper = new SndNohinSyohinKaisyuHelper();

            mailList = mailHelper.SelectAll();
            mailKaisyuList = mailKaisyuHelper.SelectAll();
            mateList = mateHelper.SelectAll();
            workList = workHelper.SelectAll();
            syohinKaisyuList = syohinKaisyuHelper.SelectAll();

            int count = mailList.Count + mailKaisyuList.Count + mateList.Count + workList.Count + syohinKaisyuList.Count;

            return count != 0;
        }

        public override bool OnBackPressed()
        {
            ((MainActivity)this.Activity).ShowProgress("FTPサーバに接続しています。");

            var syncContext = SynchronizationContext.Current;

            Task.Run(() =>
            {
                if (HasSendData() && CommonUtils.CheckFTPConnection())
                {
                    ShowDialog("警告", "納品情報を送信して業務メニューに戻ってよろしいですか。", (okFlag) =>
                    {
                        if (okFlag)
                        {

                            var filePath = System.Environment.GetFolderPath(System.Environment.SpecialFolder.Personal);

                            if (mailList.Count > 0)
                            {
                                var fileName = filePath + Java.IO.File.Separator + "SndMBN_";

                                foreach (SndNohinMail temp in mailList)
                                {
                                    WriteFTPInfo(fileName, temp);
                                }
                            }

                            Log.Debug(TAG, "メールバックデータ送信完了");

                            if (mailKaisyuList.Count > 0)
                            {
                                var fileName = filePath + Java.IO.File.Separator + "SndMBK_";

                                foreach (SndNohinMailKaisyu temp in mailKaisyuList)
                                {
                                    WriteFTPInfo(fileName, temp);
                                }
                            }

                            Log.Debug(TAG, "メールバック回収データ送信完了");

                            if (mateList.Count > 0)
                            {
                                var fileName = filePath + Java.IO.File.Separator + "SndMAT_";

                                foreach (SndNohinMate temp in mateList)
                                {
                                    WriteFTPInfo(fileName, temp);
                                }
                            }

                            Log.Debug(TAG, "マテハンデータ送信完了");

                            if (workList.Count > 0)
                            {
                                var fileName = filePath + Java.IO.File.Separator + "SndNOH_";

                                foreach (SndNohinWork temp in workList)
                                {
                                    WriteFTPInfo(fileName, temp);
                                }
                            }

                            Log.Debug(TAG, "納品作業データ送信完了");

                            if (syohinKaisyuList.Count > 0)
                            {
                                var fileName = filePath + Java.IO.File.Separator + "SndKAI_";

                                foreach (SndNohinSyohinKaisyu temp in syohinKaisyuList)
                                {
                                    WriteFTPInfo(fileName, temp);
                                }
                            }

                            ((MainActivity)this.Activity).ShowProgress("データ送信中");

                            new Thread(new ThreadStart(delegate
                            {
                                Activity.RunOnUiThread(() =>
                                {
                                    try
                                    {

                                        var filesList = Directory.GetFiles(filePath);
                                        foreach (var file in filesList)
                                        {
                                            var filename = Path.GetFileName(file);
                                            if (filename.Contains("Snd"))
                                            {
                                                CommonUtils.SendFile(filePath + Java.IO.File.Separator + filename);

                                                FileInfo fileInfo = new FileInfo(filePath + Java.IO.File.Separator + filename);
                                                if (fileInfo.Exists) fileInfo.Delete();

                                            }
                                        }

                                    }
                                    catch
                                    {
                                        Log.Debug(TAG, "FTP接続失敗");
                                    }

                                    Log.Debug(TAG, "商品回収データ送信完了");


                                    string tokuisaki_cd = prefs.GetString("tokuisaki_cd", "");
                                    string todokesaki_cd = prefs.GetString("todokesaki_cd", "");

                                    // 削除処理
                                    new SndNohinMailHelper().DeleteAll();
                                    new SndNohinMailKaisyuHelper().DeleteAll();
                                    new SndNohinMateHelper().DeleteAll();
                                    new SndNohinWorkHelper().DeleteAll();
                                    new SndNohinSyohinKaisyuHelper().DeleteAll();

                                    new MFileHelper().DeleteByTokuiAndTodokeCode(tokuisaki_cd, todokesaki_cd);
                                    new MbFileHelper().DeleteByTokuiAndTodokeCode(tokuisaki_cd, todokesaki_cd);
                                });

                                Activity.RunOnUiThread(() =>
                                {
                                    ((MainActivity)this.Activity).DismissDialog();

                                    PlayBeepOk();
                                    ShowDialog("報告", "データ送信完了しました。", () =>
                                    {
                                        FragmentManager.PopBackStack();
                                        FragmentManager.PopBackStack();
                                    });
                                });
                            })).Start();
                        }
                    });
                }
                else
                {
                    ShowDialog("警告", "業務メニューに戻ってよろしいですか。", (okFlag) => {
                        if (okFlag)
                        {
                            Log.Debug(TAG, "NOHIN_END");
                            FragmentManager.PopBackStack();
                            FragmentManager.PopBackStack();
                        }
                    });
                }
            }).ContinueWith(t => syncContext.Post(state =>
            {
                // UIスレッドでの処理
                ((MainActivity)this.Activity).DismissDialog();
            }, null));
            
            return false;
        }

        private void WriteFTPInfo(string fileName , object param)
        {
            SndNohinWork temp = SetFileParam(param);
            fileName = fileName + temp.wHHT_no + temp.wTokuisakiCD + temp.wTodokesakiCD + ".txt";

            using (var fs = new StreamWriter(fileName, true))
            {
                fs.WriteLine(
                    temp.wPackage
                        + ',' + temp.wTerminalID
                        + ',' + temp.wProgramID
                        + ',' + temp.wSagyosyaCD
                        + ',' + temp.wSoukoCD
                        + ',' + temp.wHaisoDate
                        + ',' + temp.wBinNo
                        + ',' + temp.wCourse
                        + ',' + temp.wDriverCD
                        + ',' + temp.wTokuisakiCD
                        + ',' + temp.wTodokesakiCD
                        + ',' + temp.wKanriNo
                        + ',' + temp.wVendorCd
                        + ',' + temp.wMateVendorCd
                        + ',' + temp.wSyukaDate
                        + ',' + temp.wButsuryuNo
                        + ',' + temp.wKamotuNo
                        + ',' + temp.wMatehan
                        + ',' + temp.wMatehanSu
                        + ',' + temp.wHHT_no
                        + ',' + temp.wNohinKbn
                        + ',' + temp.wKaisyuKbn
                        + ',' + temp.wTenkanState
                        + ',' + temp.wSakiTokuisakiCD
                        + ',' + temp.wSakiTodokesakiCD
                        + ',' + temp.wNohinDate
                        + ',' + temp.wNohinTime
                        + ',' + temp.wNohinEndDate
                        + ',' + temp.wNohinEndTime
                        );
                    
                fs.Close();
            }
        }

        private SndNohinWork SetFileParam(object param)
        {
            SndNohinWork result = new SndNohinWork();

            if (param.GetType() == typeof(SndNohinMail))
            {
                SndNohinMail temp = ((SndNohinMail)param);
                result = new SndNohinWork
                {
                    wPackage = temp.wPackage,
                    wTerminalID = temp.wTerminalID,
                    wProgramID = temp.wProgramID,
                    wSagyosyaCD = temp.wSagyosyaCD,
                    wSoukoCD = temp.wSoukoCD,
                    wHaisoDate = temp.wHaisoDate,
                    wBinNo = temp.wBinNo,
                    wCourse = temp.wCourse,
                    wDriverCD = temp.wDriverCD,
                    wTokuisakiCD = temp.wTokuisakiCD,
                    wTodokesakiCD = temp.wTodokesakiCD,
                    wKanriNo = temp.wKanriNo,
                    wVendorCd = temp.wVendorCd,
                    wMateVendorCd = temp.wMateVendorCd,
                    wSyukaDate = temp.wSyukaDate,
                    wButsuryuNo = temp.wButsuryuNo,
                    wKamotuNo = temp.wKamotuNo,
                    wMatehan = temp.wMatehan,
                    wMatehanSu = temp.wMatehanSu,
                    wHHT_no = temp.wHHT_no,
                    wNohinKbn = temp.wNohinKbn,
                    wKaisyuKbn = temp.wKaisyuKbn,
                    wTenkanState = temp.wTenkanState,
                    wSakiTokuisakiCD = temp.wSakiTokuisakiCD,
                    wSakiTodokesakiCD = temp.wSakiTodokesakiCD,
                    wNohinDate = temp.wNohinDate,
                    wNohinTime = temp.wNohinTime,
                    wNohinEndDate = temp.wNohinEndDate,
                    wNohinEndTime = temp.wNohinEndTime

                };

            }
            else if (param.GetType() == typeof(SndNohinMailKaisyu))
            {
                SndNohinMailKaisyu temp = ((SndNohinMailKaisyu)param);
                result = new SndNohinWork
                {
                    wPackage = temp.wPackage,
                    wTerminalID = temp.wTerminalID,
                    wProgramID = temp.wProgramID,
                    wSagyosyaCD = temp.wSagyosyaCD,
                    wSoukoCD = temp.wSoukoCD,
                    wHaisoDate = temp.wHaisoDate,
                    wBinNo = temp.wBinNo,
                    wCourse = temp.wCourse,
                    wDriverCD = temp.wDriverCD,
                    wTokuisakiCD = temp.wTokuisakiCD,
                    wTodokesakiCD = temp.wTodokesakiCD,
                    wKanriNo = temp.wKanriNo,
                    wVendorCd = temp.wVendorCd,
                    wMateVendorCd = temp.wMateVendorCd,
                    wSyukaDate = temp.wSyukaDate,
                    wButsuryuNo = temp.wButsuryuNo,
                    wKamotuNo = temp.wKamotuNo,
                    wMatehan = temp.wMatehan,
                    wMatehanSu = temp.wMatehanSu,
                    wHHT_no = temp.wHHT_no,
                    wNohinKbn = temp.wNohinKbn,
                    wKaisyuKbn = temp.wKaisyuKbn,
                    wTenkanState = temp.wTenkanState,
                    wSakiTokuisakiCD = temp.wSakiTokuisakiCD,
                    wSakiTodokesakiCD = temp.wSakiTodokesakiCD,
                    wNohinDate = temp.wNohinDate,
                    wNohinTime = temp.wNohinTime,
                    wNohinEndDate = temp.wNohinEndDate,
                    wNohinEndTime = temp.wNohinEndTime
                };
            }
            else if (param.GetType() == typeof(SndNohinMate))
            {
                SndNohinMate temp = ((SndNohinMate)param);
                result = new SndNohinWork
                {
                    wPackage = temp.wPackage,
                    wTerminalID = temp.wTerminalID,
                    wProgramID = temp.wProgramID,
                    wSagyosyaCD = temp.wSagyosyaCD,
                    wSoukoCD = temp.wSoukoCD,
                    wHaisoDate = temp.wHaisoDate,
                    wBinNo = temp.wBinNo,
                    wCourse = temp.wCourse,
                    wDriverCD = temp.wDriverCD,
                    wTokuisakiCD = temp.wTokuisakiCD,
                    wTodokesakiCD = temp.wTodokesakiCD,
                    wKanriNo = temp.wKanriNo,
                    wVendorCd = temp.wVendorCd,
                    wMateVendorCd = temp.wMateVendorCd,
                    wSyukaDate = temp.wSyukaDate,
                    wButsuryuNo = temp.wButsuryuNo,
                    wKamotuNo = temp.wKamotuNo,
                    wMatehan = temp.wMatehan,
                    wMatehanSu = temp.wMatehanSu,
                    wHHT_no = temp.wHHT_no,
                    wNohinKbn = temp.wNohinKbn,
                    wKaisyuKbn = temp.wKaisyuKbn,
                    wTenkanState = temp.wTenkanState,
                    wSakiTokuisakiCD = temp.wSakiTokuisakiCD,
                    wSakiTodokesakiCD = temp.wSakiTodokesakiCD,
                    wNohinDate = temp.wNohinDate,
                    wNohinTime = temp.wNohinTime,
                    wNohinEndDate = temp.wNohinEndDate,
                    wNohinEndTime = temp.wNohinEndTime
                };
            }
            else if (param.GetType() == typeof(SndNohinWork))
            {
                result = ((SndNohinWork)param);
            }
            else if (param.GetType() == typeof(SndNohinSyohinKaisyu))
            {
                SndNohinSyohinKaisyu temp = ((SndNohinSyohinKaisyu)param);
                result = new SndNohinWork
                {
                    wPackage = temp.wPackage,
                    wTerminalID = temp.wTerminalID,
                    wProgramID = temp.wProgramID,
                    wSagyosyaCD = temp.wSagyosyaCD,
                    wSoukoCD = temp.wSoukoCD,
                    wHaisoDate = temp.wHaisoDate,
                    wBinNo = temp.wBinNo,
                    wCourse = temp.wCourse,
                    wDriverCD = temp.wDriverCD,
                    wTokuisakiCD = temp.wTokuisakiCD,
                    wTodokesakiCD = temp.wTodokesakiCD,
                    wKanriNo = temp.wKanriNo,
                    wVendorCd = temp.wVendorCd,
                    wMateVendorCd = temp.wMateVendorCd,
                    wSyukaDate = temp.wSyukaDate,
                    wButsuryuNo = temp.wButsuryuNo,
                    wKamotuNo = temp.wKamotuNo,
                    wMatehan = temp.wMatehan,
                    wMatehanSu = temp.wMatehanSu,
                    wHHT_no = temp.wHHT_no,
                    wNohinKbn = temp.wNohinKbn,
                    wKaisyuKbn = temp.wKaisyuKbn,
                    wTenkanState = temp.wTenkanState,
                    wSakiTokuisakiCD = temp.wSakiTokuisakiCD,
                    wSakiTodokesakiCD = temp.wSakiTodokesakiCD,
                    wNohinDate = temp.wNohinDate,
                    wNohinTime = temp.wNohinTime,
                    wNohinEndDate = temp.wNohinEndDate,
                    wNohinEndTime = temp.wNohinEndTime
                };
            }

            return result;
        }

    }
}
 