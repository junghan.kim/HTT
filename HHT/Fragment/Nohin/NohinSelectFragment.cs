﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Android.App;
using Android.Content;
using Android.OS;
using Android.Preferences;
using Android.Views;
using Com.Beardedhen.Androidbootstrap;
using Com.Densowave.Bhtsdk.Barcode;
using HHT.Resources.DataHelper;
using HHT.Resources.Model;

namespace HHT
{
    public class NohinSelectFragment : BaseFragment
    {
        private View view;
        ISharedPreferences prefs;
        ISharedPreferencesEditor editor;
        private BootstrapEditText etTokuisaki, etTodokesaki, etReceipt;

        public override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
        }

        public override View OnCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
        {
            view = inflater.Inflate(Resource.Layout.fragment_nohin_select, container, false);
            prefs = PreferenceManager.GetDefaultSharedPreferences(Context);
            editor = prefs.Edit();

            SetTitle("納品検品");
            SetFooterText("");

            etTokuisaki = view.FindViewById<BootstrapEditText>(Resource.Id.et_nohinSelect_tokuisaki);
            etTokuisaki.Text = prefs.GetString("def_tokuisaki_cd", "");
            etTodokesaki = view.FindViewById<BootstrapEditText>(Resource.Id.et_nohinSelect_todokesaki);
            etReceipt = view.FindViewById<BootstrapEditText>(Resource.Id.et_nohinSelect_receipt);

            BootstrapButton confirm = view.FindViewById<BootstrapButton>(Resource.Id.btn_nohinSelect_confirm);
            confirm.FocusChange += delegate { if (confirm.IsFocused) CommonUtils.HideKeyboard(this.Activity); };
            confirm.Click += delegate {

                // 得意先チェック
                string tokui = etTokuisaki.Text;
                if (tokui == "")
                {
                    ShowDialog("エラー", "得意先コードを入力してください。", () => { etTokuisaki.RequestFocus(); });
                    return;
                }

                // 届先チェック
                string todoke = etTodokesaki.Text;
                if (todoke == "")
                {
                    ShowDialog("エラー", "届先コードを入力してください。", () => { etTodokesaki.RequestFocus(); });
                    return;
                }

                // 受領書チェック
                string jyuryo = etReceipt.Text;
                if (jyuryo == "")
                {
                    ShowDialog("エラー", "受領書をスキャンしてください。", () => { etReceipt.RequestFocus(); });
                    return;
                }

                if (jyuryo[0] != 'J' || jyuryo.Length != 9)
                {
                    ShowDialog("エラー", "受領書ではありません。", () => { etReceipt.RequestFocus(); });
                    return;
                }

                if (jyuryo.Substring(1, 4) != etTokuisaki.Text || jyuryo.Substring(5, 4) != etTodokesaki.Text)
                {
                    ShowDialog("エラー", "納入先店舗が違います。", () => { etReceipt.RequestFocus(); });
                    return;
                }

                Confirm();
                
            };
            

            etReceipt.KeyPress += (sender, e) => {
                if (e.Event.Action == KeyEventActions.Down && e.KeyCode == Keycode.Enter)
                {
                    e.Handled = true;
                    CommonUtils.HideKeyboard(Activity);
                    // 得意先チェック
                    string tokui = etTokuisaki.Text;
                    if (tokui == "")
                    {
                        ShowDialog("エラー", "得意先コードを入力してください。", () => { etTokuisaki.RequestFocus(); });
                        return;
                    }

                    // 届先チェック
                    string todoke = etTodokesaki.Text;
                    if (todoke == "")
                    {
                        ShowDialog("エラー", "届先コードを入力してください。", () => { etTodokesaki.RequestFocus(); });
                        return;
                    }

                    // 受領書チェック
                    string jyuryo = etReceipt.Text;
                    if (jyuryo == "")
                    {
                        ShowDialog("エラー", "受領書をスキャンしてください。", () => { etReceipt.RequestFocus(); });
                        return;
                    }

                    if (jyuryo[0] != 'J' || jyuryo.Length != 9)
                    {
                        ShowDialog("エラー", "受領書ではありません。", () => { etReceipt.RequestFocus(); });
                        return;
                    }
                    
                    if (jyuryo.Substring(1,4) != etTokuisaki.Text || jyuryo.Substring(5, 4) != etTodokesaki.Text)
                    {
                        ShowDialog("エラー", "納入先店舗が違います。", () => { etReceipt.RequestFocus(); });
                        return;
                    }

                    Confirm();

                }
                else
                {
                    e.Handled = false;
                }
            };

            etTodokesaki.RequestFocus();

            return view;
        }
        
        public override void OnBarcodeDataReceived(BarcodeDataReceivedEvent_ dataReceivedEvent)
        {
            IList<BarcodeDataReceivedEvent_.BarcodeData_> listBarcodeData = dataReceivedEvent.BarcodeData;

            foreach (BarcodeDataReceivedEvent_.BarcodeData_ barcodeData in listBarcodeData)
            {
                this.Activity.RunOnUiThread(() =>
                {
                    string data = barcodeData.Data;

                    if (etTodokesaki.HasFocus)
                    {
                        if(data.Length >= 8)
                        {
                            string tokuisaki = data.Substring(0, 4);
                            string todokesaki = data.Substring(4, 4);
                            etTokuisaki.Text = tokuisaki;
                            etTodokesaki.Text = todokesaki;
                            
                            TokuiFileHelper helper = new TokuiFileHelper();
                            TokuiFile tokui = helper.SelectByPk(tokuisaki, todokesaki);
                            if(tokui == null)
                            {
                                ShowDialog("エラー", "得意先コードが見つかりません。", () => { });
                                return;   
                            }
                            else
                            {
                                etReceipt.RequestFocus();
                            }
                        }
                        
                    }
                    else if (etReceipt.HasFocus)
                    {
                        try
                        {
                            string barcode_toku_todo = data.Substring(1, 8);
                            if (data[0] != 'J')
                            {
                                ShowDialog("エラー", "受領書ではありません。", () => { });
                                return;
                            }

                            if (barcode_toku_todo != etTokuisaki.Text + etTodokesaki.Text)
                            {
                                ShowDialog("エラー", "納入先店舗が違います。", () => { });
                                return;
                            }

                            etReceipt.Text = data;
                            //Confirm();
                        }
                        catch
                        {
                            ShowDialog("エラー", "受領書ではありません。", () => { });
                            return;
                        }
                    }
                });
            }
        }

        public override bool OnKeyDown(Keycode keycode, KeyEvent paramKeyEvent)
        {
            return true;
        }


        private void Confirm()
        {
            // 得意先チェック
            string tokui = etTokuisaki.Text;
            if (tokui == "")
            {
                ShowDialog("エラー", "得意先コードを入力してください。", () => { etTokuisaki.RequestFocus(); });
                return;
            }

            // 届先チェック
            string todoke = etTodokesaki.Text;
            if (todoke == "")
            {
                ShowDialog("エラー", "届先コードを入力してください。", () => { etTodokesaki.RequestFocus(); });
                return;
            }

            // 受領書チェック
            string jyuryo = etReceipt.Text;
            if (jyuryo == "")
            {
                ShowDialog("エラー", "受領書をスキャンしてください。", () => { etReceipt.RequestFocus(); });
                return;
            }

            if (jyuryo[0] != 'J' || jyuryo.Length != 9)
            {
                ShowDialog("エラー", "受領書ではありません。", () => { etReceipt.RequestFocus(); });
                return;
            }

            if (jyuryo.Substring(1, 4) != etTokuisaki.Text || jyuryo.Substring(5, 4) != etTodokesaki.Text)
            {
                ShowDialog("エラー", "納入先店舗が違います。", () => { etReceipt.RequestFocus(); });
                return;
            }


            bool hasError = false;
            

            ((MainActivity)this.Activity).ShowProgress("納品情報を確認しています。");

           
            var syncContext = SynchronizationContext.Current;

            Task.Run(() =>
            {
                // Fat な処理を別スレッドで
                Thread.Sleep(1000);

                // Main file
                MFileHelper mFlieHelper = new MFileHelper();
                List<MFile> tsumikomiList = mFlieHelper.SelectTsumikomiList(etTokuisaki.Text, etTodokesaki.Text);
                if (tsumikomiList.Count == 0)
                {
                    hasError = true;
                    ShowDialog("エラー", "得意先コードが見つかりません。", () => { etTodokesaki.RequestFocus(); });
                    return;
                }

                // 店舗到着情報を登録する。
                var fileName = System.Environment.GetFolderPath(System.Environment.SpecialFolder.Personal)
                + Java.IO.File.Separator + "TenpoArrive" + prefs.GetString("hht_no", "")
                + tsumikomiList[0].tokuisaki_cd + tsumikomiList[0].todokesaki_cd + ".csv";

                using (var fs = new StreamWriter(fileName, true))
                {

                    fs.WriteLine(
                        DateTime.Now.ToString("HHmm")
                         + ',' + tsumikomiList[0].kenpin_souko
                         + ',' + tsumikomiList[0].kitaku_cd
                         + ',' + tsumikomiList[0].syuka_date
                         + ',' + tsumikomiList[0].syuka_date
                         + ',' + tsumikomiList[0].bin_no
                         + ',' + tsumikomiList[0].course
                         + ',' + tsumikomiList[0].tokuisaki_cd
                         + ',' + tsumikomiList[0].todokesaki_cd
                         + ',' + prefs.GetString("terminal_id", "")
                         + ',' + "htt");

                    fs.Close();

                    try
                    {
                        CommonUtils.SendFile(fileName);

                        new FileInfo(fileName).Delete();

                        editor.PutString("souko_cd", tsumikomiList[0].kenpin_souko);
                        editor.PutString("kitaku_cd", tsumikomiList[0].kitaku_cd);
                        editor.PutString("haiso_date", tsumikomiList[0].syuka_date);
                        editor.PutString("bin_no", tsumikomiList[0].bin_no);
                        editor.PutString("course", tsumikomiList[0].course);
                        editor.PutString("driver_cd", tsumikomiList[0].driver_cd);

                        editor.PutString("tokuisaki_cd", tsumikomiList[0].tokuisaki_cd);
                        editor.PutString("tokuisaki_nm", tsumikomiList[0].tokuisaki_rk);
                        editor.PutString("todokesaki_cd", tsumikomiList[0].todokesaki_cd);
                        editor.PutString("vendor_cd", tsumikomiList[0].vendor_cd);
                        editor.PutString("vendor_nm", tsumikomiList[0].vendor_nm);
                        editor.PutString("mate_vendor_cd", tsumikomiList[0].default_vendor);
                        editor.PutString("mate_vendor_nm", tsumikomiList[0].default_vendor_nm);
                        editor.PutString("mailbag_flg", "1");
                        editor.PutString("nohin_date", DateTime.Now.ToString("yyyyMMdd"));
                        editor.PutString("nohin_time", DateTime.Now.ToString("HHmm"));

                        editor.PutString("bunrui", tsumikomiList[0].bunrui);
                        editor.PutString("matehan_cd", tsumikomiList[0].matehan);

                        editor.PutString("jyuryo", etReceipt.Text);
                        editor.PutBoolean("mailBagFlag", false);

                        editor.Apply();

                    }
                    catch (Exception e)
                    {
                        hasError = true;

                        ShowDialog("警告", "店着情報の送信が失敗しました。\n\n送信せず納品作業を行いますか？", (flag) => {
                            if (flag)
                            {
                                editor.PutString("souko_cd", tsumikomiList[0].kenpin_souko);
                                editor.PutString("kitaku_cd", tsumikomiList[0].kitaku_cd);
                                editor.PutString("haiso_date", tsumikomiList[0].syuka_date);
                                editor.PutString("bin_no", tsumikomiList[0].bin_no);
                                editor.PutString("course", tsumikomiList[0].course);
                                editor.PutString("driver_cd", tsumikomiList[0].driver_cd);

                                editor.PutString("tokuisaki_cd", tsumikomiList[0].tokuisaki_cd);
                                editor.PutString("tokuisaki_nm", tsumikomiList[0].tokuisaki_rk);
                                editor.PutString("todokesaki_cd", tsumikomiList[0].todokesaki_cd);
                                editor.PutString("vendor_cd", tsumikomiList[0].vendor_cd);
                                editor.PutString("vendor_nm", tsumikomiList[0].vendor_nm);
                                editor.PutString("mate_vendor_cd", tsumikomiList[0].default_vendor);
                                editor.PutString("mate_vendor_nm", tsumikomiList[0].default_vendor_nm);
                                editor.PutString("mailbag_flg", "1");
                                editor.PutString("nohin_date", DateTime.Now.ToString("yyyyMMdd"));
                                editor.PutString("nohin_time", DateTime.Now.ToString("HHmm"));

                                editor.PutString("bunrui", tsumikomiList[0].bunrui);
                                editor.PutString("matehan_cd", tsumikomiList[0].matehan);

                                editor.PutString("jyuryo", etReceipt.Text);
                                editor.PutBoolean("mailBagFlag", false);

                                editor.Apply();


                                ((MainActivity)this.Activity).DismissDialog();

                                StartFragment(FragmentManager, typeof(NohinMenuFragment));

                            }
                            else
                            {
                                ShowDialog("エラー", "FTP接続に失敗しました。", () => { });
                                hasError = true;
                                return;
                            }
                        });
                    }
                }
            }).ContinueWith(t => syncContext.Post(state =>
            {
                // UIスレッドでの処理
                ((MainActivity)this.Activity).DismissDialog();

                if (!hasError)
                {
                    StartFragment(FragmentManager, typeof(NohinMenuFragment));
                }

            }, null));
        }
    }
}