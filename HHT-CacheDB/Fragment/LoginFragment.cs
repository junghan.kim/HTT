﻿using Android.OS;
using Android.Views;
using Android.Widget;
using Android.App;
using System.Collections.Generic;
using Android.Content;
using Android.Util;
using Com.Beardedhen.Androidbootstrap;
using System.Threading.Tasks;
using Com.Densowave.Bhtsdk.Barcode;
using Android.Content.PM;

namespace HHT
{
    public class LoginFragment : BaseFragment
    {
        private readonly string TAG = "LoginFragment";
        private readonly string ERROR = "エラー";
        private readonly string ERR_NOT_FOUND_SOUKO = "センター情報が見つかりませんでした。\n再確認してください。";

        BootstrapEditText mSoukoCodeEditText;
        BootstrapEditText mTantoCodeEditText;
        TextView mSoukoNameTextView;
        TextView mVersionTextView;
        BootstrapButton mLoginButton;

        public override View OnCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
        {
            View view = inflater.Inflate(Resource.Layout.fragment_login, container, false);

            SetActionBarTitle("ログイン-あらた専用");

            mSoukoCodeEditText = view.FindViewById<BootstrapEditText>(Resource.Id.soukoCode);
            mSoukoNameTextView = view.FindViewById<TextView>(Resource.Id.tv_login_soukoName);
            mTantoCodeEditText = view.FindViewById<BootstrapEditText>(Resource.Id.tantoCode);
            mLoginButton = view.FindViewById<BootstrapButton>(Resource.Id.loginButton);
            mVersionTextView = view.FindViewById<TextView>(Resource.Id.versionName);

            // バージョン情報設定
            PackageInfo packageInfo = Context.PackageManager.GetPackageInfo(Context.ApplicationContext.PackageName, 0);
            mVersionTextView.Text = "Version " + packageInfo.VersionName;

            // サーバ 設定
            string restUrl = prefs.GetString("REST_URL", "");
            string nameSpace = prefs.GetString("CacheNamespace", "ara");

            if (restUrl != "")
            {
                WebService.SetRESTURLForArata(restUrl, nameSpace);
            }

            // Emulator 専用
            if (CommonUtils.IsEmulator())
                view.FindViewById<TextView>(Resource.Id.textView1).Text += "(Emulator)";
            

            #region keyEvent
            // 倉庫コード
            mSoukoCodeEditText.FocusChange += delegate {
                if (!mSoukoCodeEditText.IsFocused)    
                    SetSoukoName(mSoukoCodeEditText.Text);
            };

            // 担当者コード
            mTantoCodeEditText.KeyPress += (sender, e) => {
                if (e.Event.Action == KeyEventActions.Down && e.KeyCode == Keycode.Enter)
                {
                    e.Handled = true;
                    CommonUtils.HideKeyboard(Activity);
                    LoginBtn_Clicked();
                }
                else {
                    e.Handled = false;
                }
            };

            // ログインボタン
            mLoginButton.Click += delegate { LoginBtn_Clicked(); };

            #endregion

            return view;
        }

        public void LoginBtn_Clicked()
        {
            // Validation Check
            if (LoginCheck() == false)
                return;

            // 担当者情報取得かつ無線管理テーブルへ情報を登録する
            var param = new Dictionary<string, string>
            {
                { "soukoCd", mSoukoCodeEditText.Text },
                { "tanto",   mTantoCodeEditText.Text },
                { "serial",  Build.Serial }
            };

            Task.Run(async () =>
            {
                var result = await PostRestAPI("ログインしています。", "login", param);
                string menu_kbn = result["menu_kbn"];

                if (menu_kbn == "" || !int.TryParse(menu_kbn, out int temp))
                {
                    ShowDialog("エラー", "認証できませんでした。\n入力内容をご確認下さい。", () =>
                    {
                        mTantoCodeEditText.Text = "";
                        mTantoCodeEditText.RequestFocus();
                    });
                }
                else
                {
                    Bundle bundle = new Bundle();
                    bundle.PutInt("menu_kbn", int.Parse(menu_kbn));

                    editor.PutString("terminal_id", Build.Serial);
                    editor.PutString("hht_no", Build.Serial);
                    editor.PutString("souko_cd", mSoukoCodeEditText.Text);
                    editor.PutString("soukoName", mSoukoNameTextView.Text);
                    editor.PutString("driver_cd", mTantoCodeEditText.Text);
                    editor.PutString("sagyousya_cd", mTantoCodeEditText.Text);
                    editor.Apply();

                    PlayBeepOk();
                    StartFragment(FragmentManager, typeof(MainMenuFragment), bundle);
                }
            });
        }

        public override void OnResume()
        {
            base.OnResume();

            string soukoCd = prefs.GetString("souko_cd", "");
            mSoukoCodeEditText.Text = soukoCd;

            string soukoNm = prefs.GetString("soukoName", "");
            mSoukoNameTextView.Text = soukoNm;

            if(mSoukoCodeEditText.Text != "")
            {
                mTantoCodeEditText.RequestFocus();
            }
        }

        // センター名取得（倉庫名）
        public void SetSoukoName(string soukoCd)
        {
            if (string.IsNullOrEmpty(soukoCd)) return;

            Task.Run(async() => {
                try
                {
                    var result = await GetRestAPI("倉庫を確認しています。", "souko/" + soukoCd);
                    string soukoName = result["SoukoName"];
                    string cacheNamespace = result["CacheNamespace"];

                    if (string.IsNullOrEmpty(soukoName))
                    {
                        ShowDialog(ERROR, ERR_NOT_FOUND_SOUKO, () => {
                            Activity.RunOnUiThread(() =>
                            {
                                mSoukoCodeEditText.Text = "";
                                mSoukoNameTextView.Text = "";
                                mSoukoCodeEditText.RequestFocus();
                            });
                        });
                    }
                    else
                    {

                        Activity.RunOnUiThread(() => {
                            mSoukoNameTextView.Text = soukoName;

                            // MASTネームスペースに倉庫コードを確認する処理がまだできてないため、コメントアウトします。
                            string restUrl = prefs.GetString("REST_URL", "");
                            editor.PutString("CacheNamespace", cacheNamespace);
                            editor.Apply();

                            WebService.SetRESTURLForArata(restUrl, cacheNamespace);
                        });
                    }
                }
                catch
                {
                    ShowDialog(ERROR, ERR_NOT_FOUND_SOUKO, () => {

                        Activity.RunOnUiThread(() => { 
                            DismissProgress();

                            mSoukoCodeEditText.Text = "";
                            mSoukoNameTextView.Text = "";
                            mSoukoCodeEditText.RequestFocus();
                        });
                    });
                }
                
            });
        }

        // ログインチェック
        private bool LoginCheck()
        {
            if (mSoukoCodeEditText.Text == "")
            {
                string alertTitle = Resources.GetString(Resource.String.error);
                string alertBody = Resources.GetString(Resource.String.errorMsg002);

                ShowDialog(alertTitle, alertBody, () => {
                    mSoukoNameTextView.Text = "";
                    mSoukoCodeEditText.RequestFocus();
                });

                Log.Debug(TAG, "倉庫コードがありません。");
                return false;
            }

            if (mTantoCodeEditText.Text == "")
            {
                string alertTitle = Resources.GetString(Resource.String.error);
                ShowDialog(alertTitle, "担当者コードを\n入力して下さい。", () => { });

                Log.Warn(TAG, "担当者コードがありません。");
                return false;
            }

            return true;
        }

        public override bool OnKeyDown(Keycode keycode, KeyEvent paramKeyEvent)
        {
            if (keycode == Keycode.F3)
            {
                StartFragment(FragmentManager, typeof(ServerConfigFragment));
            }

            return true;
        }

        // バックキーが聞かないようにする（終了されないように）
        public override bool OnBackPressed()
        {
            return false;
        }

        // ログイン担当者コードスキャンのみできる
        public override void OnBarcodeDataReceived(BarcodeDataReceivedEvent_ dataReceivedEvent)
        {
            IList<BarcodeDataReceivedEvent_.BarcodeData_> listBarcodeData = dataReceivedEvent.BarcodeData;
            
            foreach (BarcodeDataReceivedEvent_.BarcodeData_ barcodeData in listBarcodeData)
            {
                // 担当者コードのみバーコードスキャン有効
                if (mTantoCodeEditText.IsFocused)
                {
                    this.Activity.RunOnUiThread(() => mTantoCodeEditText.Text = barcodeData.Data);
                }
            }
        }
    }
}
 