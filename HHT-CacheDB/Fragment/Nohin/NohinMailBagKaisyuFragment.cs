﻿using Android.OS;
using Android.Util;
using Android.Views;
using Android.Widget;
using Com.Beardedhen.Androidbootstrap;
using Com.Densowave.Bhtsdk.Barcode;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace HHT
{
    public class NohinMailBagKaisyuFragment : BaseFragment
    {
        private readonly string TAG = "NohinMailBagKaisyuFragment";
        private View view;
        private List<string> arrMailBag = new List<string>();
        private int mail_bag;
        private EditText etKaisyuMail;


        public override View OnCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
        {
            view = inflater.Inflate(Resource.Layout.fragment_nohin_mailBag_kaisyu, container, false);

            InitComponent();
            
            return view;
        }

        private void InitComponent()
        {
            SetActionBarTitle("メールバック回収");

            TextView tvTokuisaki = view.FindViewById<TextView>(Resource.Id.txt_nohinKaisyuMail_tokuisakiNm);
            tvTokuisaki.Text = prefs.GetString("tokuisaki_nm", "");

            TextView tvTodokesaki = view.FindViewById<TextView>(Resource.Id.txt_nohinKaisyuMail_todokisakiNm);
            tvTodokesaki.Text = prefs.GetString("todokesaki_nm", "");

            etKaisyuMail = view.FindViewById<EditText>(Resource.Id.et_nohinKaisyuMail_kaisyuMailbag);

            BootstrapButton btnConfirm = view.FindViewById<BootstrapButton>(Resource.Id.et_nohinKaisyuMail_Confirm);
            btnConfirm.Click += delegate { ConfirmMailBagKaisyu(); };

            //tokuisaki_cd = prefs.GetString("tokuisaki_cd", "");
            //todokesaki_cd = prefs.GetString("todokesaki_cd", "");
            
            // メール回収情報取得
            //List<SndNohinMailKaisyu> temp = sndNohinMailKaisyuHelper.SelectByPK(tokuisaki_cd, todokesaki_cd);

            /*
            etKaisyuMail.Text = temp.Count.ToString();
            mail_bag = temp.Count;
            arrMailBag = new List<string>();

            foreach (SndNohinMailKaisyu mailkaisyu in temp)
            {
                arrMailBag.Add(mailkaisyu.wKanriNo);
            }
            */

        }

        public override bool OnKeyDown(Keycode keycode, KeyEvent paramKeyEvent)
        {
            return true;
        }

        public override bool OnBackPressed()
        {
            return true;
        }

        private void ConfirmMailBagKaisyu()
        {
            this.Activity.RunOnUiThread(() => {
                ShowDialog("確認", "よろしいですか？", (okFlag) => {
                    if (okFlag)
                    {
                        // 送信ファイル作成
                        foreach (string mailbag in arrMailBag)
                        {
                            appendFile(mailbag);
                        }

                        Log.Debug(TAG, "COMMIT");
                        FragmentManager.PopBackStack();
                    }
                });
            });
        }

        public override void OnBarcodeDataReceived(BarcodeDataReceivedEvent_ dataReceivedEvent)
        {
            foreach (BarcodeDataReceivedEvent_.BarcodeData_ barcodeData in dataReceivedEvent.BarcodeData)
            {
                Task.Run(() => {
                    try
                    {
                        string data = barcodeData.Data;

                        if (data[0].ToString() != "M" || data.Length < 9)
                        {
                            this.Activity.RunOnUiThread(() => { ShowDialog("エラー", "メールバッグではありません。", () => { }); });
                            Log.Debug(TAG, "MAIN BAG KAISYU INPUT_ERR1:" + data);
                            return;
                        }

                        string btvTokuisaki = data.Substring(1, 4);
                        string btvTodokesaki = data.Substring(5, 4);

                        // スキャン済みチェック
                        foreach (string mailkaisyu in arrMailBag)
                        {
                            if (mailkaisyu == data)
                            {
                                this.Activity.RunOnUiThread(() => { ShowDialog("エラー", "既にスキャン済みです。", () => { }); });
                                return;
                            }
                        }

                        // 納品メールバッグ重複チェック
                        //bool hasData = MbFileHelper.HasExistMailBagData(tokuisaki_cd, todokesaki_cd, data);
                        bool hasData = true;
                        if (hasData)
                        {
                            this.Activity.RunOnUiThread(() => {
                                ShowDialog("警告", "回収メールバッグと納入メールバッグが同じです\nよろしいですか？", (flag) => {
                                    if (flag)
                                    {
                                        CheckMailBack(data);
                                    }
                                });
                            });
                        }
                        else
                        {
                            CheckMailBack(data);
                        }
                    }
                    catch(Exception e)
                    {
                        Log.Debug("", e.StackTrace);
                        this.Activity.RunOnUiThread(() => {
                            ShowDialog("エラー", "エラーが発生しました。", () => {
                                //CommonUtils.SendSMTPMailAsync("junghan.kim@invsoft.com", "junghan.kim@invsoft.com", prefs.GetString("hht_no", "") + " エラー", e.StackTrace);
                            });
                        });
                    }
                });
            }
        }

        private void CheckMailBack(string barcode)
        {
            string btvTokuisaki = barcode.Substring(1, 4);
            string btvTodokesaki = barcode.Substring(5, 4);

            string btvKey1 = btvTokuisaki + btvTodokesaki;
            string btvKey2 = prefs.GetString("tokuisaki_cd", "") + prefs.GetString("todokesaki_cd", "");
            
            if (btvKey1 != btvKey2)
            {
                this.Activity.RunOnUiThread(() => {
                    ShowDialog("エラー", "納入先店舗が違います。", () => { });
                });
                
                Log.Debug(TAG, "納入先店舗が違います  btvKey1 :" + btvKey1 + "  btvKey2 :" + btvKey2);
                return;
            }

            bool result = PlusMailBack(barcode);
            if (!result)
            {
                this.Activity.RunOnUiThread(() => {
                    ShowDialog("エラー", "既にスキャン済みです。", () => { });
                });
                
                Log.Debug(TAG, "既にスキャン済みです。 barcode :" + barcode);
                return;
            }
        }

        private bool PlusMailBack(string data)
        {
            int idx = arrMailBag.FindIndex(x => x == data);
            if (idx == -1)
            {
                arrMailBag.Add(data);
                mail_bag++;

                Activity.RunOnUiThread(() =>
                {
                    etKaisyuMail.Text = mail_bag.ToString();
                });

                return true;
            }
            else
            {
                Activity.RunOnUiThread(() => {
                    ShowDialog("エラー", "既にスキャン済みです。", () => { });
                });
                
                Log.Debug(TAG, "既にスキャン済みです。 data :" + data);
                return false;
            }
        }

        private void appendFile(string mailbag)
        {

        }
    }
}