﻿using Android.OS;
using Android.Views;
using Android.Widget;

namespace HHT
{
    public class NohinMenuFragment : BaseFragment
    {
        public override View OnCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
        {
            View view = inflater.Inflate(Resource.Layout.fragment_menu_nohin, container, false);

            SetActionBarTitle("納品検品");

            Button button1 = view.FindViewById<Button>(Resource.Id.btn_nohinMenu_mailNohin);
            button1.Click += delegate {
                StartFragment(FragmentManager, typeof(NohinMailBagNohinFragment));
            };

            Button button2 = view.FindViewById<Button>(Resource.Id.btn_nohinMenu_nohin);
            button2.Click += delegate {
                StartFragment(FragmentManager, typeof(NohinWorkFragment));
            };

            Button button3 = view.FindViewById<Button>(Resource.Id.btn_nohinMenu_kaisyu); // 回収業務
            button3.Click += delegate {
                StartFragment(FragmentManager, typeof(NohinKaisyuMenuFragment));
            };

            Button button4 = view.FindViewById<Button>(Resource.Id.btn_nohinMenu_mailKaisyu);
            button4.Click += delegate {
                    StartFragment(FragmentManager, typeof(NohinMailBagKaisyuFragment));
            }; 

            return view;
        }

        public override bool OnKeyDown(Keycode keycode, KeyEvent paramKeyEvent)
        {
            if (keycode == Keycode.Num1)
            {
                StartFragment(FragmentManager, typeof(NohinMailBagNohinFragment));
            }
            else if (keycode == Keycode.Num2)
            {
                StartFragment(FragmentManager, typeof(NohinWorkFragment));
            }

            return true;
        }

    }
}
 