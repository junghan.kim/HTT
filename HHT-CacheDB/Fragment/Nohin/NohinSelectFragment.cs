﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Android.App;
using Android.OS;
using Android.Views;
using Android.Widget;
using Com.Beardedhen.Androidbootstrap;
using Com.Densowave.Bhtsdk.Barcode;

namespace HHT
{
    public class NohinSelectFragment : BaseFragment
    {
        BootstrapEditText mTokEditText;
        BootstrapEditText mTdkEditText;
        BootstrapEditText mReceiptEditText;
        TextView mTokNmTextView;
        TextView mTdkNmTextView;

        public override View OnCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
        {
            View view = inflater.Inflate(Resource.Layout.fragment_nohin_select, container, false);

            SetActionBarTitle("納品検品");

            mTokNmTextView = view.FindViewById<TextView>(Resource.Id.tokNm);
            mTdkNmTextView = view.FindViewById<TextView>(Resource.Id.tdkNm);

            mTokEditText = view.FindViewById<BootstrapEditText>(Resource.Id.et_nohinSelect_tokuisaki);
            mTokEditText.Text = prefs.GetString("def_tokuisaki_cd", "");
            mTokEditText.FocusChange += delegate {
                if (!mTokEditText.IsFocused)
                {
                    if (mTokEditText.Text == "") {
                        mTokNmTextView.Text = "";
                        return;
                    }

                    var param = new Dictionary<string, string>
                    {
                        {"tokcd", mTokEditText.Text}
                    };

                    Task.Run(async() => {
                        var result = await WebService.PostRestAPI("nohin/nohin040", param);
                        var tokNm = result["tokNm"];

                        Activity.RunOnUiThread(() => mTokNmTextView.Text = tokNm);
                    });
                }; 
            };
            
            
            mTdkEditText = view.FindViewById<BootstrapEditText>(Resource.Id.et_nohinSelect_todokesaki);
            mTdkEditText.FocusChange += delegate {
                if (!mTdkEditText.IsFocused)
                {
                    if (mTdkEditText.Text == "")
                    {
                        mTdkNmTextView.Text = "";
                        return;
                    }

                    var param = new Dictionary<string, string>
                    {
                        {"tokcd", mTokEditText.Text},
                        {"tdkcd", mTdkEditText.Text}
                    };

                    Task.Run(async () => {
                        var result = await WebService.PostRestAPI("nohin/nohin050", param);
                        var tdkNm = result["tdkNm"];

                        Activity.RunOnUiThread(() => mTdkNmTextView.Text = tdkNm);
                    });

                };
            };
            mReceiptEditText = view.FindViewById<BootstrapEditText>(Resource.Id.et_nohinSelect_receipt);

            // TEST
            mTokEditText.Text = "237825";
            mTdkEditText.Text = "0";
            mReceiptEditText.Text = "J2378250";
            
            BootstrapButton confirm = view.FindViewById<BootstrapButton>(Resource.Id.btn_nohinSelect_confirm);
            confirm.FocusChange += delegate { if (confirm.IsFocused) CommonUtils.HideKeyboard(this.Activity); };
            confirm.Click += delegate {

                // 得意先チェック
                string tokui = mTokEditText.Text;
                if (tokui == "")
                {
                    ShowDialog("エラー", "得意先コードを入力してください。", () => { mTokEditText.RequestFocus(); });
                    return;
                }

                // 届先チェック
                string todoke = mTdkEditText.Text;
                if (todoke == "")
                {
                    ShowDialog("エラー", "届先コードを入力してください。", () => { mTdkEditText.RequestFocus(); });
                    return;
                }

                // 受領書チェック
                string jyuryo = mReceiptEditText.Text;
                if (jyuryo == "")
                {
                    ShowDialog("エラー", "受領書をスキャンしてください。", () => { mReceiptEditText.RequestFocus(); });
                    return;
                }

                if (jyuryo[0] != 'J')
                {
                    ShowDialog("エラー", "受領書ではありません。", () => { mReceiptEditText.RequestFocus(); });
                    return;
                }

                Confirm();
            };
            

            mReceiptEditText.KeyPress += (sender, e) => {
                if (e.Event.Action == KeyEventActions.Down && e.KeyCode == Keycode.Enter)
                {
                    e.Handled = true;
                    CommonUtils.HideKeyboard(Activity);
                    // 得意先チェック
                    string tokui = mTokEditText.Text;
                    if (tokui == "")
                    {
                        ShowDialog("エラー", "得意先コードを入力してください。", () => { mTokEditText.RequestFocus(); });
                        return;
                    }

                    // 届先チェック
                    string todoke = mTdkEditText.Text;
                    if (todoke == "")
                    {
                        ShowDialog("エラー", "届先コードを入力してください。", () => { mTdkEditText.RequestFocus(); });
                        return;
                    }

                    // 受領書チェック
                    string jyuryo = mReceiptEditText.Text;
                    if (jyuryo == "")
                    {
                        ShowDialog("エラー", "受領書をスキャンしてください。", () => { mReceiptEditText.RequestFocus(); });
                        return;
                    }

                    if (jyuryo[0] != 'J')
                    {
                        ShowDialog("エラー", "受領書ではありません。", () => { mReceiptEditText.RequestFocus(); });
                        return;
                    }
                    
                    Confirm();
                }
                else
                {
                    e.Handled = false;
                }
            };

            mTokEditText.RequestFocus();

            return view;
        }

        public override void OnBarcodeDataReceived(BarcodeDataReceivedEvent_ dataReceivedEvent)
        {
            IList<BarcodeDataReceivedEvent_.BarcodeData_> listBarcodeData = dataReceivedEvent.BarcodeData;

            foreach (BarcodeDataReceivedEvent_.BarcodeData_ barcodeData in listBarcodeData)
            {
                this.Activity.RunOnUiThread(() =>
                {
                    string data = barcodeData.Data;

                    if (mTdkEditText.HasFocus)
                    {
                        /*
                        if(data.Length >= 8)
                        {
                            string tokuisaki = data.Substring(0, 4);
                            string todokesaki = data.Substring(4, 4);
                            etTokuisaki.Text = tokuisaki;
                            etTodokesaki.Text = todokesaki;
                            
                            TokuiFileHelper helper = new TokuiFileHelper();
                            TokuiFile tokui = helper.SelectByPk(tokuisaki, todokesaki);
                            if(tokui == null)
                            {
                                ShowDialog("エラー", "得意先コードが見つかりません。", () => { });
                                return;   
                            }
                            else
                            {
                                etReceipt.RequestFocus();
                            }
                        }
                        */
                        
                    }
                    else if (mReceiptEditText.HasFocus)
                    {
                        /*
                        try
                        {
                            string barcode_toku_todo = data.Substring(1, 8);
                            if (data[0] != 'J')
                            {
                                ShowDialog("エラー", "受領書ではありません。", () => { });
                                return;
                            }

                            if (barcode_toku_todo != etTokuisaki.Text + etTodokesaki.Text)
                            {
                                ShowDialog("エラー", "納入先店舗が違います。", () => { });
                                return;
                            }

                            etReceipt.Text = data;
                            //Confirm();
                        }
                        catch
                        {
                            ShowDialog("エラー", "受領書ではありません。", () => { });
                            return;
                        }
                        */
                    }
                });
            }
        }
        
        private void Confirm()
        {
            // 得意先チェック
            string tokui = mTokEditText.Text;
            if (tokui == "")
            {
                ShowDialog("エラー", "得意先コードを入力してください。", () => { mTokEditText.RequestFocus(); });
                return;
            }

            // 届先チェック
            string todoke = mTdkEditText.Text;
            if (todoke == "")
            {
                ShowDialog("エラー", "届先コードを入力してください。", () => { mTdkEditText.RequestFocus(); });
                return;
            }

            // 受領書チェック
            string jyuryo = mReceiptEditText.Text;
            if (jyuryo == "")
            {
                ShowDialog("エラー", "受領書をスキャンしてください。", () => { mReceiptEditText.RequestFocus(); });
                return;
            }

            /*
            if (jyuryo[0] != 'J' || jyuryo.Length != 9)
            {
                ShowDialog("エラー", "受領書ではありません。", () => { etReceipt.RequestFocus(); });
                return;
            }

            if (jyuryo.Substring(1, 4) != etTokuisaki.Text || jyuryo.Substring(5, 4) != etTodokesaki.Text)
            {
                ShowDialog("エラー", "納入先店舗が違います。", () => { etReceipt.RequestFocus(); });
                return;
            }
            */

            ((MainActivity)this.Activity).ShowProgress("納品情報を確認しています。");

            Task.Run(async() =>
            {
                // 店着時間を更新
                var param = new Dictionary<string, string>
                {
                    {"tokcd", mTokEditText.Text },
                    {"tdkcd", mTdkEditText.Text }
                };

                var result = await WebService.PostRestAPI("nohin/nohin010", param);
                if(result.Count == 0)
                {
                    // "得意先コードが見つかりません。"
                    ShowDialog("エラー", "納品先情報が見つかりません。", () => { mTdkEditText.RequestFocus(); });
                    return;
                }

                string tokNm = result["tokuisaki_rk"];
                string syukaDate = result["syuka_date"];
                string course = result["course"];
                string binNo = result["binNo"];

                editor.PutString("tokuisaki_cd", mTokEditText.Text);
                editor.PutString("todokesaki_cd", mTdkEditText.Text);
                editor.PutString("tokuisaki_nm", mTokNmTextView.Text);
                editor.PutString("todokesaki_nm", mTdkNmTextView.Text);
                editor.PutString("haiso_date", syukaDate);
                editor.PutString("bin_no", binNo);
                editor.PutString("course", course);
                editor.PutString("driver_cd", "");
                editor.PutString("nohin_date", DateTime.Now.ToString("yyyyMMdd"));
                editor.PutString("nohin_time", DateTime.Now.ToString("HHmm"));
                

                //editor.PutString("vendor_cd", tsumikomiList[0].vendor_cd);
                //editor.PutString("vendor_nm", tsumikomiList[0].vendor_nm);
                //editor.PutString("mate_vendor_cd", tsumikomiList[0].default_vendor);
                //editor.PutString("mate_vendor_nm", tsumikomiList[0].default_vendor_nm);
                //editor.PutString("mailbag_flg", "1");

                //editor.PutString("bunrui", tsumikomiList[0].bunrui);
                //editor.PutString("matehan_cd", tsumikomiList[0].matehan);

                //editor.PutString("jyuryo", etReceipt.Text);
                //editor.PutBoolean("mailBagFlag", false);

                editor.Apply();

                StartFragment(FragmentManager, typeof(NohinMenuFragment));

            }).ContinueWith(t =>
            {
                Activity.RunOnUiThread(() => ((MainActivity)this.Activity).DismissDialog());
            });
        }
    }
}