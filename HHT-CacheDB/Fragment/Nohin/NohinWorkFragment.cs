﻿using Android.App;
using Android.OS;
using Android.Util;
using Android.Views;
using Android.Widget;
using Com.Beardedhen.Androidbootstrap;
using Com.Densowave.Bhtsdk.Barcode;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace HHT
{
    public class NohinWorkFragment : BaseFragment
    {
        private readonly string TAG = "NohinWorkFragment";
        private readonly string COMPLETE_MSG = "納品検品が\n完了しました。\n\nお疲れ様でした！";
        
        TextView mCaseTextView;                     //　ケース
        TextView mOriconTextView;                   //　オリコン
        TextView mSonotaTextView;                   //　その他
        TextView mIdoTextView;                      //　移動
        TextView mMailTextView;                     //　メール
        TextView mFuteikeiTextView;                 //　不定形
        TextView mHansokuTextView;                  //　販促
        TextView mTcTextView;                       //　TC
        TextView mTsumidaiTextView;                 //　積込台数
        TextView mKosuAllTextView;                  //　総個数
        TextView mMatehanNameTextView;              //　マテハン名（削除？）
        TextView mTokTextView;                      //  得意先
        TextView mTdkTextView;                      //  届先

        BootstrapButton mCompleteButton;            //　納品完了ボタン
        BootstrapButton mKaizoButton;               //　解除ボタン

        // 臨時変数
        private int ko_su;                          //　納品済個数
        private int maxko_su;                       //　納品総個数
        private int matehanCnt;                     //　マテハン数

        public override View OnCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
        {
            View view = inflater.Inflate(Resource.Layout.fragment_nohin_work, container, false);

            mTokTextView = view.FindViewById<TextView>(Resource.Id.txt_nohinwork_tokuisakiNm);
            mTokTextView.Text = prefs.GetString("tokuisaki_nm", "");
            mTdkTextView = view.FindViewById<TextView>(Resource.Id.txt_nohinwork_todokesakiNm);
            mTdkTextView.Text = prefs.GetString("todokesaki_nm", "");
            
            mCaseTextView = view.FindViewById<TextView>(Resource.Id.txt_nohinWork_case);
            mOriconTextView = view.FindViewById<TextView>(Resource.Id.txt_nohinWork_oricon);
            mSonotaTextView = view.FindViewById<TextView>(Resource.Id.txt_nohinWork_sonota);
            mIdoTextView = view.FindViewById<TextView>(Resource.Id.txt_nohinWork_ido);
            mMailTextView = view.FindViewById<TextView>(Resource.Id.txt_nohinWork_mail);
            mFuteikeiTextView = view.FindViewById<TextView>(Resource.Id.txt_nohinWork_futeikei);
            mHansokuTextView = view.FindViewById<TextView>(Resource.Id.txt_nohinWork_hansoku);
            mTcTextView = view.FindViewById<TextView>(Resource.Id.txt_nohinWork_tc);
            mTsumidaiTextView = view.FindViewById<TextView>(Resource.Id.txt_nohinWork_tsumidaisu);
            mKosuAllTextView = view.FindViewById<TextView>(Resource.Id.txt_nohinWork_all);
            mMatehanNameTextView = view.FindViewById<TextView>(Resource.Id.matehanNm);

            mCompleteButton = view.FindViewById<BootstrapButton>(Resource.Id.nohinButton);
            mCompleteButton.Click += delegate { CompleteNohinButtonClicked(); };
            mCompleteButton.Visibility = ViewStates.Gone;

            mKaizoButton = view.FindViewById<BootstrapButton>(Resource.Id.kaizoButton);
            mKaizoButton.Click += delegate { GotoPasswordFragment(); };
            
            SetActionBarTitle("納品検品");

            GetTumisuCount(); // 積込台数取得

            return view;
        }

        public override bool OnKeyDown(Keycode keycode, KeyEvent paramKeyEvent)
        {
            // TODO 削除予定
            if (CommonUtils.IsEmulator())
            {
                if (keycode == Keycode.Apostrophe)
                {
                    ProceedNohin("12290529006420115520");
                }
            }

            return true;
        }

        public override bool OnBackPressed()
        {
            return false;
        }

        public override void OnBarcodeDataReceived(BarcodeDataReceivedEvent_ dataReceivedEvent)
        {
            IList<BarcodeDataReceivedEvent_.BarcodeData_> listBarcodeData = dataReceivedEvent.BarcodeData;

            foreach (BarcodeDataReceivedEvent_.BarcodeData_ barcodeData in listBarcodeData)
            {
                string kamotu_no = barcodeData.Data;

                ProceedNohin(kamotu_no);
            }
        }

        private void GetTumisuCount()
        {
            string tokuisaki_cd = prefs.GetString("tokuisaki_cd", "");
            string todokesaki_cd = prefs.GetString("todokesaki_cd", "");

            // 積込台数
            ((MainActivity)this.Activity).ShowProgress("");
            Task.Run(async () =>
            {
                var param = new Dictionary<string, string>()
                {
                    { "syuka_date", "20190528" },
                    { "tokuisaki_cd", tokuisaki_cd },
                    { "todokesaki_cd", todokesaki_cd },
                };

                var result = await WebService.PostRestAPI("nohin/nohin020", param);
                matehanCnt = int.Parse(result["mateCount"]);
                maxko_su = int.Parse(result["kamotsuCount"]);
                ko_su = int.Parse(result["sumiCount"]);
                string sumiMateBunrui = result["sumiMateBunrui"];
                
                if (ko_su == maxko_su)
                {
                    ShowDialog("報告", "納品処理は\n終了しています。", 
                        () => FragmentManager.PopBackStack(FragmentManager.GetBackStackEntryAt(1).Id, 0));
                    return;
                }
                else if(matehanCnt == 0)
                {
                    ShowDialog("エラー", "納品可能なマテハンが存在しません。", 
                        () => FragmentManager.PopBackStack(FragmentManager.GetBackStackEntryAt(1).Id, 0));
                    return;
                }

                Activity.RunOnUiThread(() => {
                    // 積込台数
                    mTsumidaiTextView.Text = "0/" + matehanCnt;

                    // 総個数
                    mKosuAllTextView.Text = ko_su + "/" + maxko_su;

                    if (sumiMateBunrui != "")
                    {
                        string[] temp = sumiMateBunrui.Split(",");
                        foreach (string bunrui in temp)
                        {
                            UpdateListBunrui(bunrui);
                        }

                        matehanCnt += temp.Length;
                        mTsumidaiTextView.Text = temp.Length + "/" + matehanCnt;
                    }
                });
                
                
            }).ContinueWith(t => Activity.RunOnUiThread(() => ((MainActivity)this.Activity).DismissDialog()));
        }

        private void CompleteNohinButtonClicked()
        {
            if (maxko_su == ko_su)
            {
                Log.Debug(TAG, "MAIN NOHIN COMPLETE");
                editor.PutString("menu_flg", "2");
                editor.PutBoolean("nohinWorkEndFlag", true);
                editor.Apply();

                PlayBeepOk();

                // 2 -> 1
                ShowDialog("報告", COMPLETE_MSG, () => FragmentManager.PopBackStack(FragmentManager.GetBackStackEntryAt(1).Id, 0));
            }
            else
            {
                mCompleteButton.Visibility = ViewStates.Gone;
                mKaizoButton.Visibility = ViewStates.Visible;

                mCaseTextView.Text = "0";
                mOriconTextView.Text = "0";
                mFuteikeiTextView.Text = "0";
                mTcTextView.Text = "0";
                mIdoTextView.Text = "0";
                mMailTextView.Text = "0";
                mHansokuTextView.Text = "0";
                mSonotaTextView.Text = "0";
            }
        }

        private void GotoPasswordFragment()
        {
            Bundle bundle = new Bundle();
            bundle.PutBoolean("fromMailback", false);
            StartFragment(FragmentManager, typeof(NohinMailBagPasswordFragment), bundle);
        }

        private void UpdateListBunrui(string bunrui)
        {
            switch (bunrui)
            {
                case "01": mCaseTextView.Text = (int.Parse(mCaseTextView.Text) + 1).ToString(); break;
                case "02": mOriconTextView.Text = (int.Parse(mOriconTextView.Text) + 1).ToString(); break;
                case "03": mSonotaTextView.Text = (int.Parse(mSonotaTextView.Text) + 1).ToString(); break;
                case "04": mIdoTextView.Text = (int.Parse(mIdoTextView.Text) + 1).ToString(); break;
                case "05": mMailTextView.Text = (int.Parse(mMailTextView.Text) + 1).ToString(); break;
                case "06": mSonotaTextView.Text = (int.Parse(mSonotaTextView.Text) + 1).ToString(); break;
                case "07": mFuteikeiTextView.Text = (int.Parse(mFuteikeiTextView.Text) + 1).ToString(); break;
                case "08": mSonotaTextView.Text = (int.Parse(mSonotaTextView.Text) + 1).ToString(); break;
                case "09": mHansokuTextView.Text = (int.Parse(mHansokuTextView.Text) + 1).ToString(); break;
                case "T": mTcTextView.Text = (int.Parse(mTcTextView.Text) + 1).ToString(); break;
                default: mSonotaTextView.Text = (int.Parse(mSonotaTextView.Text) + 1).ToString(); break;
            }
        }

        private void ProceedNohin(string kamotsu)
        {
            Activity.RunOnUiThread(() => ShowProgress(""));
            
            Task.Run(async () =>
            {
                string syukaDate = prefs.GetString("haiso_date", "");
                string tokcd = prefs.GetString("tokuisaki_cd", "");
                string tdkcd = prefs.GetString("todokesaki_cd", "");
                string sagyosyaCd = prefs.GetString("sagyousya_cd", "");

                var param = new Dictionary<string, string>()
                {
                    { "syuka_date", syukaDate },
                    { "tokuisaki_cd", tokcd },
                    { "todokesaki_cd", tdkcd },
                    { "kamotsu", kamotsu },
                    { "sagyosyaCD", sagyosyaCd },
                };

                var result = await WebService.PostRestAPI("nohin/nohin030", param);
                if (result.Count == 0)
                {
                    ShowDialog("エラー", "該当データがありません。", () => { });
                    return;
                }
                else if(result.ContainsKey("msg"))
                {
                    ShowDialog("エラー", result["msg"], () => { });
                    return;
                }

                int count = int.Parse(result["count"]);
                string bunrui = result["bunrui"];

                //tvmatehanNm.Text = tsumikomi.category_nm;
                
                int idx = mTsumidaiTextView.Text.IndexOf('/');
                int tsumidai = (int.Parse(mTsumidaiTextView.Text.Substring(0, idx)) + 1);

                // 総個数
                idx = mKosuAllTextView.Text.IndexOf('/');
                ko_su = int.Parse(mKosuAllTextView.Text.Substring(0, idx)) + count;

                Activity.RunOnUiThread(() => {

                    // 画面の数字をカウントアップする
                    mTsumidaiTextView.Text = tsumidai + "/" + matehanCnt;
                    mKosuAllTextView.Text = ko_su + "/" + maxko_su;

                    UpdateListBunrui(bunrui);

                    if (ko_su == maxko_su)
                    {
                        editor.PutBoolean("nohinWorkEndFlag", true);
                        editor.Apply();

                        mKaizoButton.Visibility = ViewStates.Gone;
                        mCompleteButton.Visibility = ViewStates.Visible;
                        
                    }

                });


            }).ContinueWith(t => {
                Activity.RunOnUiThread(() => DismissProgress());
            });
        }

    }
}