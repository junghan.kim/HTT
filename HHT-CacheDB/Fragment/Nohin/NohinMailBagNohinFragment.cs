﻿using Android.OS;
using Android.Util;
using Android.Views;
using Android.Widget;
using Com.Beardedhen.Androidbootstrap;
using Com.Densowave.Bhtsdk.Barcode;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace HHT
{
    public class NohinMailBagNohinFragment : BaseFragment
    {
        private readonly string TAG = "NohinMailBagNohinFragment";
        private View view;

        private BootstrapEditText etMailBagNumber;
        private BootstrapButton muButton;

        //private string tokuisakiCd;
        //private string todokesakiCd;
        //private int mailbackCnt;
        
        public override View OnCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
        {
            view = inflater.Inflate(Resource.Layout.fragment_nohin_mailBag_nohin, container, false);

            // 得意先名設定
            TextView tokNameTextView = view.FindViewById<TextView>(Resource.Id.text_tokuisaki_name);
            tokNameTextView.Text = prefs.GetString("tokuisaki_nm", "");

            // 納品したメールバック数取得
            etMailBagNumber = view.FindViewById<BootstrapEditText>(Resource.Id.et_nohinMailBagNohin_mailbagNumber);
            //etMailBagNumber.Text = SndNohinMailHelper.SelectByPk(tokuisakiCd, todokesakiCd).Count.ToString();
            etMailBagNumber.Text = "0";

            SetActionBarTitle("メールバック納品");

            //tokuisakiCd = prefs.GetString("tokuisaki_cd", "");
            //todokesakiCd = prefs.GetString("todokesaki_cd", "");
            Task.Run(async() => {

                await Task.Delay(3000);
            
            });

            // 解除ボタン
            BootstrapButton kaizoButton = view.FindViewById<BootstrapButton>(Resource.Id.btn_nohinMailBagNohin_kaizo);
            kaizoButton.Click += delegate {
                Bundle bundle = new Bundle();
                bundle.PutBoolean("fromMailback", true);
                StartFragment(FragmentManager, typeof(NohinMailBagPasswordFragment), bundle);
            };

            // 解除ボタン
            muButton = view.FindViewById<BootstrapButton>(Resource.Id.btn_nohinMailBagNohin_mu);
            muButton.Click += delegate { CompleteMailNohin(); };
            
            // 納品可能なメールバックがあるか確認
            CountMailBackExsit();
            
            return view;
        }
        
        private void CountMailBackExsit()
        {
            Activity.RunOnUiThread(() => ((MainActivity)Activity).ShowProgress("少々お待ちください。"));
            int count = 0;

            Task.Run(async () =>
            {
                await Task.Delay(2000);
                
                count = 1;

                if (count == 0)
                {
                    Activity.RunOnUiThread(() => ((MainActivity)Activity).DismissDialog());
                    ShowDialog("エラー", "納品可能なメールバックがありません。", () =>
                    {
                        
                    });
                }
                else
                {
                    Activity.RunOnUiThread(() => {
                        ((MainActivity)Activity).DismissDialog();
                        if (count != 0) muButton.Enabled = false;
                    });
                }
            });
        }

        private void CompleteMailNohin()
        {
            ShowDialog("警告", "メールバッグ納品業務を終了しますか？", (okFlag) => {
                if (okFlag)
                {
                    Log.Debug(TAG, "F3 return pushed : " + prefs.GetString("tokuisaki_cd", "") + ", " + prefs.GetString("tokuisaki_nm", ""));
                    editor.PutBoolean("mailBagFlag", true);
                    editor.Apply();
                    FragmentManager.PopBackStack();
                }
            });
        }
        
        public override bool OnBackPressed()
        {
            return false;
        }

        public override void OnBarcodeDataReceived(BarcodeDataReceivedEvent_ dataReceivedEvent)
        {
            IList<BarcodeDataReceivedEvent_.BarcodeData_> listBarcodeData = dataReceivedEvent.BarcodeData;

            foreach (BarcodeDataReceivedEvent_.BarcodeData_ barcodeData in listBarcodeData)
            {
                ((MainActivity)this.Activity).DisableScanning();

                string mailbackCd = barcodeData.Data;

                ProceedNohin(mailbackCd);
                // 1. is MailBack Barcode?

                // 2. 

                /*
                wNohinDate = prefs.GetString("nohin_date", ""),
                wNohinTime = prefs.GetString("nohin_time", ""),
                wNohinEndDate = DateTime.Now.ToString("yyyyMMdd"),
                wNohinEndTime = DateTime.Now.ToString("HHmm")
                */

                //editor.PutBoolean("mailBagFlag", true);
                //editor.PutString("menu_flg", "1");
                //editor.Apply();
                PlayBeepOk();
                ShowDialog("報告", "メールバッグの\n納品が完了しました。", () => {
                    ((MainActivity)this.Activity).EnableScanning();
                    FragmentManager.PopBackStack(FragmentManager.GetBackStackEntryAt(1).Id, 0);
                });
            }
        }

        public override bool OnKeyDown(Keycode keycode, KeyEvent paramKeyEvent)
        {
            // TODO 削除予定
            if (CommonUtils.IsEmulator())
            {
                if (keycode == Keycode.Apostrophe)
                {
                    ProceedNohin("12290529006420115520");
                }
            }

            return base.OnKeyDown(keycode, paramKeyEvent);
        }

        private void ProceedNohin(string barcode)
        {
            Activity.RunOnUiThread(() => ((MainActivity)Activity).ShowProgress(""));
            int count = 0;

            Task.Run(async () =>
            {
                await Task.Delay(2000);

                count = 0;

                if (count == 0)
                {
                    Activity.RunOnUiThread(() => ((MainActivity)Activity).DismissDialog());
                    ShowDialog("エラー", "納品可能なメールバックがありません。", () =>
                    {
                        FragmentManager.PopBackStack();
                    });
                }
                else
                {
                    PlayBeepOk();
                    ShowDialog("報告", "メールバッグの\n納品が完了しました。", () => {
                        ((MainActivity)this.Activity).EnableScanning();
                        FragmentManager.PopBackStack(FragmentManager.GetBackStackEntryAt(1).Id, 0);
                    });
                }
            });
        }
    }
}