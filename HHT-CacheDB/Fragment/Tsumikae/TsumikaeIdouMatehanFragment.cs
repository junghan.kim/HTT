﻿using Android.App;
using Android.OS;
using Android.Views;
using Android.Widget;
using Newtonsoft.Json;
using System.Collections.Generic;
using HHT.Resources.Model;
using static Android.Widget.AdapterView;
using Com.Beardedhen.Androidbootstrap;
using System.Threading.Tasks;
using System.Linq;

namespace HHT
{
    public class TsumikaeIdouMatehanFragment : BaseFragment
    {
        private ListView mateListView;
        private List<Ido> motoInfoList;

        private string handyId;
        private string sagyousyaCd;
        private string syukaDate;

        private readonly int TANPIN_IDO = 1;    //単品移動

        public override View OnCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
        {
            View view = inflater.Inflate(Resource.Layout.fragment_tsumikae_idou_matehan, container, false);
           
            mateListView = view.FindViewById<ListView>(Resource.Id.lv_matehanList);

            motoInfoList = JsonConvert.DeserializeObject<List<Ido>>(Arguments.GetString("motoInfo"));
            handyId = prefs.GetString("terminal_id", "");
            sagyousyaCd = prefs.GetString("sagyousya_cd", "");
            syukaDate = prefs.GetString("syuka_date", "");

            mateListView.ItemClick += (object sender, ItemClickEventArgs e) => SelectListViewItem(e.Position);

            SetMatehanList();
            
            return view;
        }

        private void SetMatehanList()
        {
            try
            {
                List<string> temp = new List<string>();

                Activity.RunOnUiThread(() => {
                    temp.Add("1.  かご車");
                    temp.Add("2.  キャリー");

                    ArrayAdapter adapter = new ArrayAdapter<string>(Activity, Resource.Layout.adapter_list_matehan, temp);
                    mateListView.Adapter = adapter;
                });
            }
            catch
            {
                ShowDialog("エラー", "ベンダーコードがみつかりません。", () => { });
                return;
            }
        }
        
        public override bool OnKeyDown(Keycode keycode, KeyEvent paramKeyEvent)
        {
            if (keycode >= Keycode.Num1 && keycode <= Keycode.Num4)
            {
                // Keycode.Num1 = 8
                int keyNum = int.Parse(keycode.ToString()) - 7;
                SelectListViewItem(keyNum);
            }

            return true;
        }

        public void SelectListViewItem(int index)
        {
            string msg = "";
            string bunrui = "";
            if (index == 0) 
            { 
                msg = "かご車でよろしいですか？";
                bunrui = "2";
            }
            else
            {
                msg = "キャリーでよろしいですか？";
                bunrui = "1";
            }

            ShowDialog("警告", msg, async (okFlag) =>
            {
                if (okFlag)
                {
                    await GroupingMatehanAsync(bunrui);
                }
            });
        }
        
        private async Task GroupingMatehanAsync(string bunrui)
        {
            var param = new Dictionary<string, string>
            {
                {"pTerminalID", handyId },
                {"pSagyosyaCD", sagyousyaCd },
                {"pSyukaDate", syukaDate },
                {"bunrui", bunrui.ToString() }
            };

            // 1：キャリー,  2: かご車
            string kamotsuList = string.Join(",", motoInfoList.Select(x => x.kamotsuNo));
            param.Add("pMotoKamotsuNo", kamotsuList);

            var idou090 = await WebService.PostRestAPI("ido/ido090", param);
            string errorMsg = "";

            if (idou090 == null || idou090["poRet"] != "0")
            {
                errorMsg = "エラーが発生しました。";
            }

            Activity.RunOnUiThread(() => {
                if (string.IsNullOrEmpty(errorMsg))
                {
                    ShowDialog("報告", "移動処理が\n完了しました。",
                        () => FragmentManager.PopBackStack(FragmentManager.GetBackStackEntryAt(0).Id, 0));
                }
                else
                {
                    ShowDialog("エラー", errorMsg, () => { });
                }
            });
            
        }
    }
}
 