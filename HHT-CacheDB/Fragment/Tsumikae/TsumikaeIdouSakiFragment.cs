﻿using Android.App;
using Android.OS;
using Android.Util;
using Android.Views;
using Android.Widget;
using Com.Beardedhen.Androidbootstrap;
using Com.Densowave.Bhtsdk.Barcode;
using HHT.Resources.Model;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HHT
{
    public class TsumikaeIdouSakiFragment : BaseFragment
    {
        private readonly string TAG = "TsumikaeIdouSakiFragment";

        // UI Component
        private TextView caseTextView;
        private TextView oriconTextView;
        private TextView idoTextView;
        private TextView mailTextView;
        private TextView sonotaTextView;
        private TextView futeikeiTextView;
        private TextView hansokuTextView;
        private TextView tcTextView;
        private TextView kosuTextView;
        private BootstrapButton confirmBtn;
        
        private int menuFlag, btvQty, btvScnFlg;
        
        private List<string> motokamotuList;
        private IDOU033 kamotuInfo;

        private string sakiKamotsuNo;
        private string sakiMatehanCd;
        private string targetMatehanVendor;
        List<Ido> motoInfoList;

        private readonly int TANPIN_IDO = 1;    //単品移動
        private readonly int ZENPIN_IDO = 2;
        private readonly int MATHAN_IDO = 3;    //マテハン移動
        
        public override View OnCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
        {
            View view = inflater.Inflate(Resource.Layout.fragment_tsumikae_Idou_saki, container, false);

            SetActionBarTitle("移動先マテハン");

            caseTextView = view.FindViewById<TextView>(Resource.Id.txt_tsumikae_case);
            oriconTextView = view.FindViewById<TextView>(Resource.Id.txt_tsumikae_oricon);
            idoTextView = view.FindViewById<TextView>(Resource.Id.txt_tsumikae_idosu);
            mailTextView = view.FindViewById<TextView>(Resource.Id.txt_tsumikae_mail);
            sonotaTextView = view.FindViewById<TextView>(Resource.Id.txt_tsumikae_sonota);
            futeikeiTextView = view.FindViewById<TextView>(Resource.Id.txt_tsumikae_futeikei);
            hansokuTextView = view.FindViewById<TextView>(Resource.Id.txt_tsumikae_hansoku);
            tcTextView = view.FindViewById<TextView>(Resource.Id.txt_tsumikae_tc);
            kosuTextView = view.FindViewById<TextView>(Resource.Id.txt_tsumikae_kosu);
            confirmBtn = view.FindViewById<BootstrapButton>(Resource.Id.confirmButton);

            menuFlag = prefs.GetInt("menuFlag", TANPIN_IDO);
            btvQty = 0;
            btvScnFlg = 0;
            
            confirmBtn.Text = menuFlag != MATHAN_IDO ? "完了" : "確定";
            confirmBtn.Enabled = false;
            confirmBtn.Click += delegate{ if(btvScnFlg > 0) { CompleteIdou(); }};
            motoInfoList = JsonConvert.DeserializeObject<List<Ido>>(Arguments.GetString("motoInfo"));

            // 当画面だけの仕様でCODE128は読めないようにする。
            // ((MainActivity)Activity).ResizeBarcodeLength(20);

            return view;
        }

        public override bool OnKeyDown(Keycode keycode, KeyEvent paramKeyEvent)
        {
            if (keycode == Keycode.F1)
            {
                if (btvScnFlg > 0)
                {
                    CompleteIdou();
                }
            }
            else if (keycode == Keycode.Enter)
            {
                if (btvQty > 0)
                {
                    CompleteIdou();
                }
            }

            // TODO 削除予定
            if (CommonUtils.IsEmulator())
            {
                if (keycode == Keycode.Apostrophe)
                {
                    sakiKamotsuNo = "12320529006420115639";
                    CompleteIdou();
                }
            }

            return true;
        }

        public override void OnBarcodeDataReceived(BarcodeDataReceivedEvent_ dataReceivedEvent)
        {
            IList<BarcodeDataReceivedEvent_.BarcodeData_> listBarcodeData = dataReceivedEvent.BarcodeData;

            foreach (BarcodeDataReceivedEvent_.BarcodeData_ barcodeData in listBarcodeData)
            {
                string kamotsu_no = barcodeData.Data;

                if (btvScnFlg > 0)
                {
                    ShowDialog("エラー", "既にスキャン済みです", () => { });
                    Log.Debug(TAG, "既にスキャン済みです barcode :" + kamotsu_no);
                    return;
                }

                // 移動先バーコードチェック
                Task.Run(async () =>
                {
                    if (!await CheckScanNo(kamotsu_no)) return;

                    var param = new Dictionary<string, string>()
                    {
                        {"kamotsuNo" ,kamotsu_no }
                    };

                    if (menuFlag == TANPIN_IDO)
                    {
                        //var idou010List = await WebService.PostRestAPI<List<IDOU010>>("ido/ido020", param);
                        var result = await WebService.PostRestAPI("ido/ido033", param);
                        if (result == null)
                        {
                            ShowDialog("報告", "表示データがありません", () => { });
                            return;
                        }
                        // {\"tokuisaki_cd\":\"2773\",\"todokesaki_cd\":\"0\",\"matehan\":\"46\",\"SyukaDate\":\"20190528\"}"

                        string motomateCdString = prefs.GetString("motomate_cd", "");

                        foreach (string motomatecd in motomateCdString.Split(','))
                        {
                            if (motomatecd == result["matehan"])
                            {
                                ShowDialog("エラー", "同一のマテハンです", () => { });
                                return;
                            }
                        }

                        //SetMatehan(idou010.bunrui, int.Parse(idou010.cnt));
                        SetMatehan("01", 1);

                        sakiMatehanCd = result["matehan"];
                        sakiKamotsuNo = kamotsu_no;
                        btvScnFlg = 1;
                        Activity.RunOnUiThread(() => confirmBtn.Enabled = true);

                    }
                    else if (menuFlag == ZENPIN_IDO)
                    {
                        /*
                        var idou020List = await WebService.PostRestAPI<List<IDOU020>>("ido/ido020", param);
                        if (idou020List.Count == 0)
                        {
                            ShowDialog("報告", "表示データがありません", () => { });
                            return;
                        }

                        // 遷移先マテハン設定
                        foreach (IDOU020 idou020 in idou020List)
                        {
                            if (idou020.matehan.Substring(0, 2) == idou020.bara_matehan)
                            {
                                ShowDialog("エラー", "バラへの移動は出来ません", () => { });
                                return;
                            }

                            if (prefs.GetString("motomate_cd", "") == idou020.matehan)
                            {
                                ShowDialog("エラー", "同一のマテハンです", () => { });
                                return;
                            }

                            SetMatehan(idou020.bunrui, int.Parse(idou020.cnt));
                        }

                        targetMatehanVendor = idou020List[0].matehan_vendor;
                        sakiKamotsuNo = kamotsu_no;
                        Activity.RunOnUiThread(() => confirmBtn.Enabled = true);

                        btvScnFlg = 1;

                        */
                    }

                }).ContinueWith(t => {
                    Activity.RunOnUiThread(() => confirmBtn.Visibility = ViewStates.Visible);
                });
            }
        }

        private async Task<bool> CheckScanNo(string kamotsu_no)
        {
            // 元と先マテハンが一致するか確認
            motokamotuList = prefs.GetStringSet("kamotuList", new List<string>()).ToList();
            if (motokamotuList.FindIndex(x => x == kamotsu_no) != -1)
            {
                ShowDialog("エラー", "同一のマテハンです。", () => { });
                return false;
            }


            var param = new Dictionary<string, string>
            {
                { "kamotsuNo", kamotsu_no }
            };
            kamotuInfo = await WebService.PostRestAPI<IDOU033>("ido/ido033", param);

            // 得意先、届先が一致するかを確認する
            if ((kamotuInfo.tokuisaki_cd == prefs.GetString("tmptokui_cd", "") && kamotuInfo.todokesaki_cd == prefs.GetString("tmptodoke_cd", ""))
                || prefs.GetString("tmptokui_cd", "") == "")
            {
                // Do nothing
            }
            else
            {
                ShowDialog("エラー", "届先が異なります。", () => { });
                return false;
            }

            // 便情報が一致するかを確認する
            if (kosuTextView.Text != "0")
            {
                // 便チェック
                if (prefs.GetString("bin_no", "0") != kamotuInfo.torikomi_bin)
                {
                    ShowDialog("エラー", "便が異なります。", () => { });
                    return false;
                }
            }

            targetMatehanVendor = kamotuInfo.vendor_cd;

            return true;
        }
        
        private void SetMatehan(string bunrui, int addValue)
        {
            string addedValue = ""; //加算した値を保存

            Activity.RunOnUiThread(() => {
                switch (bunrui)
                {
                    case "01":
                        addedValue = (int.Parse(caseTextView.Text) + addValue).ToString();
                        editor.PutString("sk_case_su", addedValue);
                        caseTextView.Text = addedValue.ToString();
                        break;
                    case "02":
                        addedValue = (int.Parse(oriconTextView.Text) + addValue).ToString();
                        editor.PutString("sk_oricon_su", addedValue);
                        oriconTextView.Text = addedValue.ToString();
                        break; // case 03は存在しない
                    case "04":
                        addedValue = (int.Parse(idoTextView.Text) + addValue).ToString();
                        editor.PutString("sk_ido_su", addedValue);
                        idoTextView.Text = addedValue.ToString();
                        break;
                    case "05":
                        addedValue = (int.Parse(mailTextView.Text) + addValue).ToString();
                        editor.PutString("sk_mail_su", addedValue);
                        mailTextView.Text = addedValue.ToString();
                        break;
                    case "06":
                        addedValue = (int.Parse(sonotaTextView.Text) + addValue).ToString();
                        editor.PutString("sk_sonota_su", addedValue);
                        sonotaTextView.Text = addedValue.ToString();
                        break;
                    case "07":
                        addedValue = (int.Parse(futeikeiTextView.Text) + addValue).ToString();
                        editor.PutString("sk_futeikei_su", addedValue);
                        futeikeiTextView.Text = addedValue.ToString();
                        break;
                    // case 08は存在しない
                    case "09":
                        addedValue = (int.Parse(hansokuTextView.Text) + addValue).ToString();
                        editor.PutString("sk_hansoku_su", addedValue);
                        hansokuTextView.Text = addedValue.ToString();
                        break;
                    default:
                        addedValue = (int.Parse(sonotaTextView.Text) + addValue).ToString();
                        editor.PutString("sk_sonota_su", addedValue);
                        sonotaTextView.Text = addedValue.ToString();
                        break;
                }

                kosuTextView.Text = (int.Parse(kosuTextView.Text) + addValue).ToString();
                editor.PutString("ko_su", kosuTextView.Text);
                editor.Apply();
            });
        }
        
        private void CompleteIdou()
        {
            List<string> motomateCdList = prefs.GetStringSet("motoMateCdList", new List<string>()).ToList();
            
            if (menuFlag == 1)
            {
                foreach (Ido motoInfo in motoInfoList)
                {
                    Dictionary<string, string> param = new Dictionary<string, string>
                    {
                        {"pTerminalID", prefs.GetString("terminal_id","") },
                        {"pProgramID", "IDO" },
                        {"pSagyosyaCD", prefs.GetString("sagyousya_cd", "") },
                        {"pMotoKamotsuNo", motoInfo.kamotsuNo },
                        {"pSakiKamotsuNo", sakiKamotsuNo },
                        {"mSyukaDate", "20190528" },
                    };

                    ((MainActivity)Activity).ShowProgress("aaaaaa");
                    Task.Run(async () =>
                    {
                        IDOU050 idou050 = await WebService.PostRestAPI<IDOU050>("ido/ido050", param);

                        if (idou050 == null)
                        {
                            ShowDialog("エラー", "データがありません。", () => { });
                            return;
                        }

                        if (idou050.poRet != "0")
                        {
                            ShowDialog("エラー", idou050.poMsg, () => { });
                            return;
                        }

                        PlayBeepOk();
                        ShowDialog("報告", "移動処理が\n完了しました。", () => { BackToMainMenu(); });

                    }).ContinueWith(t => {
                        Activity.RunOnUiThread(()=>((MainActivity)this.Activity).DismissDialog());
                    });
                }
            }
            else if (menuFlag == 2)
            {
                var param = new Dictionary<string, string>
                {
                    {"pSagyosyaCD", prefs.GetString("sagyousya_cd", "") },
                    {"pMotoMatehan", motoInfoList[0].motoMateCode },
                    {"pSakiKamotsuNo", sakiKamotsuNo },
                    {"SyukaDate", prefs.GetString("syuka_date", "")}
                };

                Task.Run(async () => await WebService.PostRestAPI<IDOU060>("ido/ido060", param))
                    .ContinueWith(t => {
                        IDOU060 idou060 = t.Result;
                        if (idou060.poMsg != null && idou060.poMsg != "")
                        {
                            ShowDialog("エラー", idou060.poMsg, () => { });
                            return;
                        }

                        PlayBeepOk();
                        ShowDialog("報告", "移動処理が\n完了しました。", () => { BackToMainMenu(); });

                    });
            }
        }

        private void BackToMainMenu()
        {
            /*
            string driver_nm = prefs.GetString("driver_nm", "");
            string souko_cd = prefs.GetString("souko_cd", "");
            string driver_cd = prefs.GetString("driver_cd", "");
            string kitaku_cd = prefs.GetString("kitaku_cd", "");
            string def_tokuisaki_cd = prefs.GetString("def_tokuisaki_cd", "");
            string souko_kbn = prefs.GetString("souko_kbn", "");

            //editor.Clear();
            //editor.Commit();
            
            editor.PutString("terminal_id", Build.Serial);
            editor.PutString("hht_no", Build.Serial);
            // ***********************

            editor.PutString("driver_nm", driver_nm);
            editor.PutString("souko_cd", souko_cd);
            editor.PutString("driver_cd", driver_cd);
            editor.PutString("sagyousya_cd", driver_cd);
            editor.PutString("kitaku_cd", kitaku_cd);
            editor.PutString("def_tokuisaki_cd", def_tokuisaki_cd);
            editor.PutString("souko_kbn", souko_kbn);
            editor.Apply();
            */

            Activity.RunOnUiThread(() => {
                FragmentManager.PopBackStack(FragmentManager.GetBackStackEntryAt(0).Id, 0);
            });
            
        }


    }
}