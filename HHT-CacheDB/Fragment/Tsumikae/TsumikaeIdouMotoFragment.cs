﻿using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Android.OS;
using Android.Views;
using Android.Widget;
using Com.Beardedhen.Androidbootstrap;
using Com.Densowave.Bhtsdk.Barcode;
using HHT.Resources.Model;
using Newtonsoft.Json;

namespace HHT
{
    public class TsumikaeIdouMotoFragment : BaseFragment
    {
        private TextView caseTextView;
        private TextView oriconTextView;
        private TextView idoTextView;
        private TextView mailTextView;
        private TextView sonotaTextView;
        private TextView futeikeiTextView;
        private TextView hansokuTextView;
        private TextView tcTextView;
        private TextView kosuTextView;
        private BootstrapButton confirmButton;
        private BootstrapButton matehanButton;

        private List<string> kamotuList = new List<string>();
        private List<string> motomateCdList = new List<string>();
        private List<Ido> motoMateInfo = new List<Ido>();

        private string tmptokui_cd = "";
        private string tmptodoke_cd = "";

        private static readonly int TANPIN_IDO = 1;
        private static readonly int ZENPIN_IDO = 2;
        private static readonly int MATHAN_IDO = 3;
        
        private int menuFlag;

        public override View OnCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
        {
            View view = inflater.Inflate(Resource.Layout.fragment_tsumikae_Idou_moto, container, false);
            
            // タイトル設定
            SetActionBarTitle("移動元マテハン");
            
            caseTextView = view.FindViewById<TextView>(Resource.Id.txt_tsumikae_case);
            oriconTextView = view.FindViewById<TextView>(Resource.Id.txt_tsumikae_oricon);
            idoTextView = view.FindViewById<TextView>(Resource.Id.txt_tsumikae_idosu);
            mailTextView = view.FindViewById<TextView>(Resource.Id.txt_tsumikae_mail);
            sonotaTextView = view.FindViewById<TextView>(Resource.Id.txt_tsumikae_sonota);
            futeikeiTextView = view.FindViewById<TextView>(Resource.Id.txt_tsumikae_futeikei);
            hansokuTextView = view.FindViewById<TextView>(Resource.Id.txt_tsumikae_hansoku);
            tcTextView = view.FindViewById<TextView>(Resource.Id.txt_tsumikae_tc);
            kosuTextView = view.FindViewById<TextView>(Resource.Id.txt_tsumikae_kosu);
            confirmButton = view.FindViewById<BootstrapButton>(Resource.Id.confirmButton);
            matehanButton = view.FindViewById<BootstrapButton>(Resource.Id.mateButton);
            
            // メニュー区分
            menuFlag = prefs.GetInt("menuFlag", TANPIN_IDO);

            if (menuFlag == TANPIN_IDO) { 
                matehanButton.Visibility = ViewStates.Visible;
            }else{
                matehanButton.Visibility = ViewStates.Gone;
            }

            // 連続バーコード読み取り時に使う変数初期化
            editor.PutString("tmptokui_cd", "");
            editor.PutString("tmptodoke_cd", "");
            editor.PutString("motomate_cd", "");
            editor.Apply();

            // motomateCdList = prefs.GetStringSet("motoMateCdList", new List<string>()).ToList();
            
            // 確定ボタン: スキャン後活性して他の画面へ遷移
            confirmButton.Click += delegate {
                Bundle bundle = new Bundle();
                bundle.PutString("motoInfo", JsonConvert.SerializeObject(motoMateInfo));

                if (menuFlag == TANPIN_IDO)  // 単品積替
                {
                    StartFragment(FragmentManager, typeof(TsumikaeIdouSakiFragment), bundle);
                }
                else if (menuFlag == MATHAN_IDO)　// マテハン変更
                {
                    StartFragment(FragmentManager, typeof(TsumikaeIdouMatehanFragment), bundle);
                }
            };

            // マテハンボタン
            matehanButton.Click += delegate
            {
                Bundle bundle = new Bundle();
                bundle.PutString("motoInfo", JsonConvert.SerializeObject(motoMateInfo));

                StartFragment(FragmentManager, typeof(TsumikaeIdouMatehanFragment), bundle);
            };

            // 当画面だけの仕様でCODE128は読めないようにする。
            ((MainActivity)Activity).ResizeBarcodeLength(20);

            return view;
        }

        public override bool OnKeyDown(Keycode keycode, KeyEvent paramKeyEvent)
        {
            if (keycode == Keycode.Back)
            {
                editor.PutString("case_su", "0");
                editor.PutString("sk_case_su", "0");
                editor.PutString("oricon_su", "0");
                editor.PutString("sk_oricon_su", "0");
                editor.PutString("ido_su", "0");
                editor.PutString("sk_ido_su", "0");
                editor.PutString("mail_su", "0");
                editor.PutString("sk_mail_su", "0");
                editor.PutString("sonota_su", "0");
                editor.PutString("sk_sonota_su", "0");
                editor.PutString("futeikei_su", "0");
                editor.PutString("sk_futeikei_su", "0");
                editor.PutString("hansoku_su", "0");
                editor.PutString("sk_hansoku_su", "0");
                editor.PutString("sonota_su", "0");
                editor.PutString("sk_sonota_su", "0");

                editor.PutString("ko_su", "0");
                editor.PutString("sk_ko_su", "0");

                editor.PutString("motok_su", "0");

                editor.PutStringSet("kamotuList", new List<string>());
                editor.Apply();

            }
            
            return true;
        }

        public override void OnBarcodeDataReceived(BarcodeDataReceivedEvent_ dataReceivedEvent)
        {
            IList<BarcodeDataReceivedEvent_.BarcodeData_> listBarcodeData = dataReceivedEvent.BarcodeData;

            foreach (BarcodeDataReceivedEvent_.BarcodeData_ barcodeData in listBarcodeData)
            {
                // スキャン無効化
                ((MainActivity)Activity).DisableScanning();
                Activity.RunOnUiThread(() => ((MainActivity)Activity).ShowProgress(""));
                
                string kamotsuNo = barcodeData.Data;
                
                Task.Run(async() => {

                    // スキャン重複チェック
                    if (kamotuList.FindIndex(x => x == kamotsuNo) != -1)
                    {
                        Activity.RunOnUiThread(() => ShowDialog("エラー", "同一の商品です。", () => { }));
                        return;
                    }

                    // スキャン処理
                    if (menuFlag == TANPIN_IDO) // 単品移動
                    {
                        await SettingTanpinMotoInfo(kamotsuNo);
                    }
                    else // 全品移動, マテハン移動
                    {
                        await SettingZenpinMotoInfo(kamotsuNo);
                    }

                }).ContinueWith((x) => {
                    Activity.RunOnUiThread(() => ((MainActivity)this.Activity).DismissDialog());
                    ((MainActivity)this.Activity).EnableScanning();
                });
                
            }
        }

        // 単品移動
        private async Task SettingTanpinMotoInfo(string kamotsu_no)
        {
            try
            {
                // 貨物番号に紐づく情報を取得する
                IDOU033 idou033 = await GetKamotsuInfo(kamotsu_no);

                // 得意先、届先が一致するかを確認する
                if ((idou033.tokuisaki_cd == prefs.GetString("tmptokui_cd", "")
                    && idou033.todokesaki_cd == prefs.GetString("tmptodoke_cd", ""))
                    || prefs.GetString("tmptokui_cd", "") == "")
                {
                    // Do nothing
                }
                else
                {
                    ShowDialog("エラー", "届先が異なります。", () => { });
                    Activity.RunOnUiThread(() => ((MainActivity)Activity).DismissDialog());
                    return;
                }

                idou033.bunrui = "01";

                SetMatehanValue(idou033.bunrui, 1);
                motomateCdList.Add(idou033.matehan);
                kamotuList.Add(kamotsu_no);
                motoMateInfo.Add(new Ido { kamotsuNo = kamotsu_no, motoMateCode = idou033.matehan });
                
                string motomate = prefs.GetString("motomate_cd", "");
                if (motomate == "")
                {
                    motomate += idou033.matehan;
                }
                else
                {
                    motomate += "," + idou033.matehan;
                }

                editor.PutString("motomate_cd", motomate);
                editor.PutString("motok_su", kosuTextView.Text);
                editor.PutString("ko_su", kosuTextView.Text);
                editor.PutString("sk_ko_su", kosuTextView.Text);
                editor.PutStringSet("motomateCdList", motomateCdList);
                editor.PutStringSet("kamotuList", kamotuList);
                editor.Apply();

                tmptokui_cd = idou033.tokuisaki_cd;
                tmptodoke_cd = idou033.todokesaki_cd;

                Activity.RunOnUiThread(() => {
                    confirmButton.Enabled = true;
                    matehanButton.Enabled = true;
                });
            }
            catch
            {
                ShowDialog("エラー", "移動元の貨物Noが見つかりません。", () => { });
            }
        }

        // 全品移動、マテハン移動
        private async Task SettingZenpinMotoInfo(string kamotsu_no)
        {
            try
            {
                // マテハン情報取得
                var param = new Dictionary<string, string>()
                {
                    {"kamotsuNo", kamotsu_no }
                };

                var result = await WebService.PostRestAPI<IDOU020>("ido/ido020", param);
                if (result == null)
                {
                    ShowDialog("エラー", "移動元の貨物Noが見つかりません。", () => { });
                    ((MainActivity)Activity).DismissDialog();
                    return;
                }

                string caseSu = result.caseSu == "" ? "0" : result.caseSu;
                string oriconSu = result.oriconSu == "" ? "0" : result.oriconSu;
                string hutekeiSu = result.hutekeiSu == "" ? "0" : result.hutekeiSu;
                string matehan = result.matehan;
                List<string> kamotsuList = result.kamotsuList;

                Activity.RunOnUiThread(() => {
                    caseTextView.Text = caseSu;
                    oriconTextView.Text = oriconSu;
                    futeikeiTextView.Text = hutekeiSu;

                    kosuTextView.Text = (int.Parse(caseTextView.Text) + int.Parse(oriconTextView.Text) + int.Parse(futeikeiTextView.Text)).ToString();
                });
                
               
                foreach (string kamotsu in kamotsuList)
                {
                    motoMateInfo.Add(new Ido { kamotsuNo = kamotsu, motoMateCode = matehan });   
                }

                editor.PutString("motomate_cd", matehan);
                editor.Apply();
                
               if (menuFlag == ZENPIN_IDO) { 
                   editor.PutStringSet("kamotuList", kamotsuList);
                   editor.Apply();

                   Bundle bundle = new Bundle();
                   bundle.PutString("motoInfo", JsonConvert.SerializeObject(motoMateInfo));
                   StartFragment(FragmentManager, typeof(TsumikaeIdouSakiFragment), bundle);
               }
               else
               {
                   Activity.RunOnUiThread(() => confirmButton.Enabled = true);
               }
            }
            catch {
                ShowDialog("エラー", "貨物Noが見つかりません。", () => { });
            }
        }
        
        private void SetMatehanValue(string bunrui, int addValue)
        {
            string addedValue = ""; //加算した値を保存

            editor.PutString("case_su", "0");
            editor.PutString("sk_case_su", "0");
            editor.PutString("oricon_su", "0");
            editor.PutString("sk_oricon_su", "0");
            editor.PutString("ido_su", "0");
            editor.PutString("sk_ido_su", "0");
            editor.PutString("mail_su", "0");
            editor.PutString("sk_mail_su", "0");
            editor.PutString("sonota_su", "0");
            editor.PutString("sk_sonota_su", "0");
            editor.PutString("futeikei_su", "0");
            editor.PutString("sk_futeikei_su", "0");
            editor.PutString("hansoku_su", "0");
            editor.PutString("sk_hansoku_su", "0");
            editor.PutString("ko_su", "0");
            editor.PutString("sk_ko_su", "0");
            editor.Apply();

            // テキスト修正はUiThreadで動かないとエラーになる
            new Thread(new ThreadStart(delegate {
                Activity.RunOnUiThread(() =>
                {
                    switch (bunrui)
                    {
                        case "01":
                            addedValue = (int.Parse(caseTextView.Text) + addValue).ToString();
                            editor.PutString("case_su", addedValue);
                            editor.PutString("sk_case_su", addedValue);
                            caseTextView.Text = addedValue.ToString();
                            break;
                        case "02":
                            addedValue = (int.Parse(oriconTextView.Text) + addValue).ToString();
                            editor.PutString("oricon_su", addedValue);
                            editor.PutString("sk_oricon_su", addedValue);
                            oriconTextView.Text = addedValue.ToString();
                            break; // case 03は存在しない
                        case "04":
                            addedValue = (int.Parse(idoTextView.Text) + addValue).ToString();
                            editor.PutString("ido_su", addedValue);
                            editor.PutString("sk_ido_su", addedValue);
                            idoTextView.Text = addedValue.ToString();
                            break;
                        case "05":
                            addedValue = (int.Parse(mailTextView.Text) + addValue).ToString();
                            editor.PutString("mail_su", addedValue);
                            editor.PutString("sk_mail_su", addedValue);
                            mailTextView.Text = addedValue.ToString();
                            break;
                        case "06":
                            addedValue = (int.Parse(sonotaTextView.Text) + addValue).ToString();
                            editor.PutString("sonota_su", addedValue);
                            editor.PutString("sk_sonota_su", addedValue);
                            sonotaTextView.Text = addedValue.ToString();
                            break;
                        case "07":
                            addedValue = (int.Parse(futeikeiTextView.Text) + addValue).ToString();
                            editor.PutString("futeikei_su", addedValue);
                            editor.PutString("sk_futeikei_su", addedValue);
                            futeikeiTextView.Text = addedValue.ToString();
                            break;
                        // case 08は存在しない
                        case "09":
                            addedValue = (int.Parse(hansokuTextView.Text) + addValue).ToString();
                            editor.PutString("hansoku_su", addedValue);
                            editor.PutString("sk_hansoku_su", addedValue);
                            hansokuTextView.Text = addedValue.ToString();
                            break;
                        default:
                            addedValue = (int.Parse(sonotaTextView.Text) + addValue).ToString();
                            editor.PutString("sonota_su", addedValue);
                            editor.PutString("sk_sonota_su", addedValue);
                            sonotaTextView.Text = addedValue.ToString();
                            break;
                    }

                    kosuTextView.Text = (int.Parse(kosuTextView.Text) + addValue).ToString();
                    editor.PutString("ko_su", kosuTextView.Text);
                    editor.PutString("sk_ko_su", kosuTextView.Text);
                    editor.Apply();

                }
                );

            }
            )).Start();
        }

        private async Task<IDOU033> GetKamotsuInfo(string kamotsu_no)
        {
            var param = new Dictionary<string, string>()
            {
                {"kamotsuNo", kamotsu_no }
            };

            var idou033 = await WebService.PostRestAPI<IDOU033>("ido/ido033", param);
            if (idou033.todokesaki_cd == "" || idou033.todokesaki_cd == null)
            {
                idou033.todokesaki_cd = "0";
            }

            if (idou033.SyukaDate == null)
            {
                ShowDialog("エラー", "貨物Noが見つかりません。", () => { });
                return null;
            }

            
            editor.PutString("syuka_date", idou033.SyukaDate);
            editor.PutString("tmptokui_cd", idou033.tokuisaki_cd);
            editor.PutString("tmptodoke_cd", idou033.todokesaki_cd);
            editor.PutString("tsumi_vendor_cd", idou033.vendor_cd);
            editor.PutString("tsumi_vendor_nm", idou033.vendor_nm);
            editor.PutString("vendor_cd", idou033.default_vendor);
            editor.PutString("vendor_nm", idou033.vendor_nm);
            editor.PutString("btvBunrui", idou033.bunrui);
            editor.PutString("bin_no", idou033.torikomi_bin);

            editor.Apply();

            return idou033;
        }
    }
}