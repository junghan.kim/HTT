﻿using Android.OS;
using Android.Views;
using Android.Widget;

namespace HHT
{
    public class MainMenuFragment : BaseFragment
    {
        private int menu_kbn;

        public override View OnCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
        {
            View view = inflater.Inflate(Resource.Layout.fragment_menu_main, container, false);

            SetActionBarTitle("業務メニュー");
            //menu_kbn = Arguments.GetInt("menu_kbn");
            menu_kbn = 2;

            if (menu_kbn == 0)
            {
                // 構内の場合
                LinearLayout konaiLayout = view.FindViewById<LinearLayout>(Resource.Id.konaiLayout);
                konaiLayout.Visibility = ViewStates.Visible;

                Button btnNyuka = view.FindViewById<Button>(Resource.Id.konaiNyuka);
                btnNyuka.Click += delegate { StartFragment(FragmentManager, typeof(KosuMenuFragment)); };

                Button btnTsumikae = view.FindViewById<Button>(Resource.Id.konaiIdou);
                btnTsumikae.Click += delegate { StartFragment(FragmentManager, typeof(TsumikaeMenuFragment)); };

                Button btnMailBag = view.FindViewById<Button>(Resource.Id.konaiMail);
                btnMailBag.Click += delegate { StartFragment(FragmentManager, typeof(MailMenuFragment)); };
                
            }
            else if(menu_kbn == 1)
            {
                // ドライバの場合
                LinearLayout driverLayout = view.FindViewById<LinearLayout>(Resource.Id.driverLayout);
                driverLayout.Visibility = ViewStates.Visible;
                
                Button btnNyuka = view.FindViewById<Button>(Resource.Id.driverNyuka);
                btnNyuka.Click += delegate {
                    StartFragment(FragmentManager, typeof(KosuMenuFragment));
                };

                Button btnTsumikae = view.FindViewById<Button>(Resource.Id.driverIdou);
                btnTsumikae.Click += delegate { StartFragment(FragmentManager, typeof(TsumikaeMenuFragment)); };

                Button btnTsumikomi = view.FindViewById<Button>(Resource.Id.driverTsumikomi);
                btnTsumikomi.Click += delegate { StartFragment(FragmentManager, typeof(TsumikomiSelectFragment)); };

                Button btnNohin = view.FindViewById<Button>(Resource.Id.driverNohin);
                btnNohin.Click += delegate { StartFragment(FragmentManager, typeof(NohinSelectFragment)); };

            }
            else if (menu_kbn == 2)
            {
                // 管理者の場合
                LinearLayout managerLayout = view.FindViewById<LinearLayout>(Resource.Id.managerLayout);
                managerLayout.Visibility = ViewStates.Visible;

                Button btnNyuka = view.FindViewById<Button>(Resource.Id.btn_main_manager_nyuka);
                btnNyuka.Click += delegate { StartFragment(FragmentManager, typeof(KosuMenuFragment));};

                Button btnTsumikae = view.FindViewById<Button>(Resource.Id.btn_main_manager_tsumikae);
                btnTsumikae.Click += delegate { StartFragment(FragmentManager, typeof(TsumikaeMenuFragment)); };
                
                Button btnTsumikomi = view.FindViewById<Button>(Resource.Id.btn_main_manager_tsumikomi);
                btnTsumikomi.Click += delegate { StartFragment(FragmentManager, typeof(TsumikomiSelectFragment)); };

                Button btnNohin = view.FindViewById<Button>(Resource.Id.btn_main_manager_nohin);
                btnNohin.Click += delegate { StartFragment(FragmentManager, typeof(NohinSelectFragment)); };

                Button btnMailBag = view.FindViewById<Button>(Resource.Id.btn_main_manager_mailBag);
                btnMailBag.Click += delegate { StartFragment(FragmentManager, typeof(MailMenuFragment)); };
                btnMailBag.Visibility = ViewStates.Gone;

            }

            return view;
        }

        public override bool OnKeyDown(Keycode keycode, KeyEvent paramKeyEvent)
        {
            if (menu_kbn == 0)
            {
                //構内
                if (keycode == Keycode.Num1)
                {
                    StartFragment(FragmentManager, typeof(KosuMenuFragment));
                }
                else if (keycode == Keycode.Num2)
                {
                    StartFragment(FragmentManager, typeof(TsumikaeMenuFragment));
                }
                else if (keycode == Keycode.Num3)
                {
                    StartFragment(FragmentManager, typeof(MailMenuFragment));
                }
            }
            else if (menu_kbn == 1)
            {
                //ドライバ
                if (keycode == Keycode.Num1)
                {
                    StartFragment(FragmentManager, typeof(KosuMenuFragment));
                }
                else if (keycode == Keycode.Num2)
                {
                    StartFragment(FragmentManager, typeof(TsumikaeMenuFragment));
                }
                else if (keycode == Keycode.Num3)
                {
                    StartFragment(FragmentManager, typeof(TsumikomiSelectFragment));
                }
                else if (keycode == Keycode.Num4)
                {
                    StartFragment(FragmentManager, typeof(NohinSelectFragment));
                }
            }
            else if (menu_kbn == 2)
            {
                //管理者基準
                if (keycode == Keycode.Num1)
                {
                    StartFragment(FragmentManager, typeof(KosuMenuFragment));
                }
                else if (keycode == Keycode.Num2)
                {
                    StartFragment(FragmentManager, typeof(TsumikaeMenuFragment));
                }
                else if (keycode == Keycode.Num3)
                {
                    StartFragment(FragmentManager, typeof(TsumikomiSelectFragment));
                }
                else if (keycode == Keycode.Num4)
                {
                    StartFragment(FragmentManager, typeof(NohinSelectFragment));
                }
                else if (keycode == Keycode.Num5)
                {
                    // メールバッグ
                    StartFragment(FragmentManager, typeof(MailMenuFragment));
                }
            }
               
            return true;
        }

    }
}