﻿using Android.OS;
using Android.Views;
using Android.Widget;

namespace HHT
{
    public class MailMenuFragment : BaseFragment
    {
        public override View OnCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
        {
            View view = inflater.Inflate(Resource.Layout.fragment_menu_mailbag, container, false);

            SetActionBarTitle("メールバック");

            Bundle bundle = new Bundle();

            Button button1 = view.FindViewById<Button>(Resource.Id.btn_mailMenu_regist);
            button1.Click += delegate {
                bundle.PutBoolean("registFlg", true);
                StartFragment(FragmentManager, typeof(MailManageSelectFragment), bundle);  
            };

            Button button2 = view.FindViewById<Button>(Resource.Id.btn_mailMenu_delete);
            button2.Click += delegate {
                bundle.PutBoolean("registFlg", false);
                StartFragment(FragmentManager, typeof(MailManageSelectFragment), bundle);
            };

            return view;
        }

        public override bool OnKeyDown(Keycode keycode, KeyEvent paramKeyEvent)
        {
            if (keycode == Keycode.Num1)
            {
                Bundle bundle = new Bundle();
                bundle.PutBoolean("registFlg", true);
                
                StartFragment(FragmentManager, typeof(MailManageSelectFragment), bundle);
            }
            else if (keycode == Keycode.Num2)
            {
                Bundle bundle = new Bundle();
                bundle.PutBoolean("registFlg", false);
                StartFragment(FragmentManager, typeof(MailManageSelectFragment), bundle);
            }

            return true;
        }
    }
}