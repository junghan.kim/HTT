﻿using System;
using Android.OS;
using Android.Views;
using Com.Beardedhen.Androidbootstrap;

namespace HHT
{
    public class MailManageSelectFragment : BaseFragment
    {
        BootstrapEditText mHaisoEditText;
        BootstrapEditText mBinEditText;

        public override View OnCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
        {
            View view = inflater.Inflate(Resource.Layout.fragment_mail_manage_select, container, false);

            bool registFlg = Arguments.GetBoolean("registFlg");
            SetActionBarTitle(registFlg ? "メールバッグ登録" : "メールバッグ削除");

            mBinEditText = view.FindViewById<BootstrapEditText>(Resource.Id.et_mailRegistSelect_bin);
            mBinEditText.KeyPress += (sender, e) => {
                if (e.Event.Action == KeyEventActions.Down && e.KeyCode == Keycode.Enter)
                {
                    e.Handled = true;
                    CommonUtils.HideKeyboard(Activity);
                    Confirm();
                }
                else
                {
                    e.Handled = false;
                }
            };

            BootstrapButton btnConfirm = view.FindViewById<BootstrapButton>(Resource.Id.btn_mailRegistSelect_confirm);
            btnConfirm.Click += delegate { Confirm(); };

            mHaisoEditText = view.FindViewById<BootstrapEditText>(Resource.Id.et_mailRegistSelect_haiso);
            mHaisoEditText.FocusChange += (sender, e) => {
                if (e.HasFocus)
                {
                    mHaisoEditText.Text = mHaisoEditText.Text.Replace("/", "");
                    mHaisoEditText.SetSelection(mHaisoEditText.Text.Length);
                }
                else
                {
                    try
                    {
                        mHaisoEditText.Text = CommonUtils.GetDateYYYYMMDDwithSlash(mHaisoEditText.Text);
                    }
                    catch
                    {
                        ShowDialog("エラー", "日付を正しく入力してください。", () => {
                            mHaisoEditText.Text = "";
                            mHaisoEditText.RequestFocus();
                        });
                    }
                }
            };

            mHaisoEditText.Text = DateTime.Now.ToString("yyyy/MM/dd");
            mBinEditText.RequestFocus();

            return view;
        }
        
        private void Confirm()
        {   
            // Input Check
            if(mHaisoEditText.Text == "")
            {
                ShowDialog("エラー", "日付を入力してください。", () => {
                    mHaisoEditText.RequestFocus();
                });
                return;
            }

            if (mBinEditText.Text == "")
            {
                ShowDialog("エラー", "便を入力してください。", () => {
                    mBinEditText.RequestFocus();
                });
                return;
            }

            // 登録済みメールバッグ数を取得
            string haisoDate = mHaisoEditText.Text.Replace("/", "");

            Arguments.PutString("haisoDate", haisoDate);
            Arguments.PutString("bin_no", mBinEditText.Text);
            
            StartFragment(FragmentManager, typeof(MailManageInputFragment), Arguments);
        }
    }
}