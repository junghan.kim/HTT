﻿using Android.App;
using Android.OS;
using Android.Views;
using Android.Widget;
using Com.Beardedhen.Androidbootstrap;
using Com.Densowave.Bhtsdk.Barcode;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace HHT
{
    public class MailManageInputFragment : BaseFragment
    {
        BootstrapEditText mMailBagEditText;
        TextView mMailBagSuTextView;

        private bool registFlg;
        private int mail_back;

        private readonly string ERR1 = "メールバッグバーコードではありません。";
        private readonly string ERR2 = "該当のメールバッグは登録済です。";
        //private readonly string ERR3 = "該当コースは積込済のため登録できません。";
        //private readonly string ERR4 = "該当のメールバッグは出発点呼済です。";
        //private readonly string ERR5 = "マスタに存在しない店舗のメールバッグをスキャンしました。";

        public override View OnCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
        {
            View view = inflater.Inflate(Resource.Layout.fragment_mail_manage_input, container, false);

            // コンポーネント初期化
            registFlg = Arguments.GetBoolean("registFlg");

            SetActionBarTitle(registFlg ? "メールバッグ登録" : "メールバッグ削除");

            BootstrapButton mCompleteButton = view.FindViewById<BootstrapButton>(Resource.Id.btn_mailRegistInput_complete);
            mCompleteButton.Click += delegate { Complete(); };

            mMailBagEditText = view.FindViewById<BootstrapEditText>(Resource.Id.et_mailRegistInput_mail);
            mMailBagSuTextView = view.FindViewById<TextView>(Resource.Id.txt_mailRegistInput_mailbagSu);
            mail_back = 0;

            mMailBagEditText.RequestFocus();

            return view;
        }

        private void Complete()
        {
            if (registFlg)
            {
                ShowDialog("報告", "メールバッグの登録が\n完了しました。", () => { 
                    FragmentManager.PopBackStack(FragmentManager.GetBackStackEntryAt(0).Id, 0); 
                });
            }
            else
            {
                FragmentManager.PopBackStack();
            }
        }

        public override void OnBarcodeDataReceived(BarcodeDataReceivedEvent_ dataReceivedEvent)
        {
            IList<BarcodeDataReceivedEvent_.BarcodeData_> listBarcodeData = dataReceivedEvent.BarcodeData;

            foreach (BarcodeDataReceivedEvent_.BarcodeData_ barcodeData in listBarcodeData)
            {
                Activity.RunOnUiThread(() => ((MainActivity)Activity).DisableScanning());

                Activity.RunOnUiThread(() =>
                {
                    try
                    {
                        string data = barcodeData.Data;

                        // メールバッグ専用バーコード確認
                        if (data[0].ToString() != "M")
                        {
                            ShowDialog("エラー", ERR1, () => { });
                            ((MainActivity)Activity).EnableScanning();
                            return;
                        }
                        else
                        {
                            mMailBagEditText.Text = data;
                        }

                        // メールバック登録処理
                        Dictionary<string, string> param = new Dictionary<string, string>
                        {
                            {"pHaisoDate", Arguments.GetString("haisoDate") },
                            {"pSagyosyaCD", prefs.GetString("sagyousya_cd","") },
                            {"pBinNo", Arguments.GetString("bin_no") },
                            {"pKanriNo", data }
                        };

                        if (registFlg)
                        {
                            // メールバック登録処理
                            Task.Run(async () =>
                            {
                                var result = await WebService.PostRestAPI("mail/mail010", param);
                                if (result.ContainsKey("errMsg"))
                                {
                                    //	登録済
                                    ShowDialog("エラー", ERR2, () => mMailBagEditText.Text = "");
                                }
                                else
                                {
                                    mail_back++;
                                    mMailBagSuTextView.Text = "(" + mail_back + ")";
                                    Vibrate();
                                }
                            });
                        }
                        else
                        {
                            // メールバック削除処理
                            Task.Run(async () =>
                            {
                                var result = await WebService.PostRestAPI("mail/mail030", param);
                                if (result.ContainsKey("errMsg"))
                                {
                                    ShowDialog("報告", "該当のメールバッグは未登録です。", () => mMailBagEditText.Text = "");
                                }
                                else
                                {
                                    mail_back++;
                                    mMailBagSuTextView.Text = "(" + mail_back + ")";
                                    ShowDialog("報告", "登録されているメールバッグを取消しました。", () => mMailBagEditText.Text = "");
                                }
                            });
                        }
                    }
                    catch
                    {
                        ShowDialog("エラー", "例外エラーが発生しました。\n管理者に連絡してください。", () => { });
                    }

                    ((MainActivity)Activity).EnableScanning();

                });
            }
        }
    }
}