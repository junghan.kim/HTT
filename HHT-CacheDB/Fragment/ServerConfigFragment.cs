﻿using Android.OS;
using Android.Views;
using Android.Widget;
using Android.App;
using Android.Util;
using System.Threading.Tasks;

namespace HHT
{
    public class ServerConfigFragment : BaseFragment
    {
        static readonly string TAG = "X:" + typeof(ServerConfigFragment).Name;
        private View view;
        //private EditText _ipEditText, _portEditText;
        private EditText mRestURLEditText;
        private Button _button;
        
        public override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            Log.Debug(TAG, "ConfigFragment is loadad.");
        }

        public override View OnCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
        {
            view = inflater.Inflate(Resource.Layout.fragment_config, container, false);
            
            SetActionBarTitle("ホスト設定");

            string restURL = prefs.GetString("REST_URL", "");
            if (restURL == "")
            {
                restURL = "bms.jobs-logistics.jp";
            }

            mRestURLEditText = view.FindViewById<EditText>(Resource.Id.restURLEditText);
            mRestURLEditText.Text = restURL;

            _button = view.FindViewById<Button>(Resource.Id.confirm);
            _button.Click += delegate {

                if (mRestURLEditText.Text == "")
                {
                    ShowDialog("エラー", "接続先を入力してください。", () => { });
                    return;
                }

                ShowDialog("警告", "この設定でよろしいでしょうか？", (isOkPushed) => {
                if (isOkPushed)
                {
                    //WebService.SetRESTURLForArata(mRestURLEditText.Text, "tmsmast");
                    WebService.SetRESTURLForArata(mRestURLEditText.Text, "ara");
                    editor.PutString("REST_URL", mRestURLEditText.Text);
                    editor.Apply();

                    Activity.RunOnUiThread(() => ShowProgress(""));

                    Task.Run(async() => {
                        bool isOK = await WebService.IsConnected();

                        if (isOK)
                        {
                            ShowDialog("報告", "設定完了しました。", () => {
                                Log.Debug(TAG, "REST_URL Setting Completed : " + mRestURLEditText.Text);
                                Log.Debug(TAG, "Back to LoginFragment");
                                Activity.RunOnUiThread(() => DismissProgress());

                                FragmentManager.PopBackStack();
                            });
                        }
                        else {
                                Activity.RunOnUiThread(() => DismissProgress());

                                ShowDialog("エラー", "接続できませんでした。\n再度確認してください。", () => {});
                        }
                    });
                }
                });
            };

            return view;
        }
        
        public override bool OnKeyDown(Keycode keycode, KeyEvent paramKeyEvent)
        {
            return true;
        }
    }
}
 