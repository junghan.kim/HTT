﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Android.App;
using Android.OS;
using Android.Views;
using Android.Widget;
using Com.Beardedhen.Androidbootstrap;
using Com.Densowave.Bhtsdk.Barcode;
using HHT.Resources.Model;

namespace HHT
{
    public class TsumikomiMailFragment : BaseFragment
    {
        View view;

        BootstrapEditText etMailBag;
        TextView mailBagSu;
        private int mail_back;

        private readonly string ERR1 = "メールバッグバーコードではありません。";
        private readonly string ERR2 = "該当のメールバッグは登録済です。";
        private readonly string ERR3 = "該当コースは積込済のため登録できません。";
        private readonly string ERR4 = "該当のメールバッグは出発点呼済です。";
        private readonly string ERR5 = "マスタに存在しない店舗のメールバッグをスキャンしました。";
        
        public override View OnCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
        {
            view = inflater.Inflate(Resource.Layout.fragment_mail_manage_input, container, false);

            // コンポーネント初期化
            SetActionBarTitle("積込検品");

            BootstrapButton btnComplete = view.FindViewById<BootstrapButton>(Resource.Id.btn_mailRegistInput_complete);
            btnComplete.Click += delegate { FragmentManager.PopBackStack(); };

            etMailBag = view.FindViewById<BootstrapEditText>(Resource.Id.et_mailRegistInput_mail);
            mailBagSu = view.FindViewById<TextView>(Resource.Id.txt_mailRegistInput_mailbagSu);
            mail_back = 0;
            
            etMailBag.RequestFocus();
            
            return view;
        }
        
        public override void OnBarcodeDataReceived(BarcodeDataReceivedEvent_ dataReceivedEvent)
        {
            IList<BarcodeDataReceivedEvent_.BarcodeData_> listBarcodeData = dataReceivedEvent.BarcodeData;

            foreach (BarcodeDataReceivedEvent_.BarcodeData_ barcodeData in listBarcodeData)
            {
                this.Activity.RunOnUiThread(() =>
                {
                    string data = barcodeData.Data;
                    string headChar = data.Substring(0, 1);

                    // メールバッグ専用バーコード確認
                    if (headChar != "M")
                    {
                        ShowDialog("エラー", ERR1, () => { });
                        return;
                    }
                    else
                    {
                        etMailBag.Text = data;
                    }

                    // メールバック登録処理
                    Dictionary<string, string> param = new Dictionary<string, string>
                    {
                        {"pTerminalID", prefs.GetString("terminal_id","") },
                        {"pProgramID", "MBA" },
                        {"pSagyosyaCD", prefs.GetString("sagyousya_cd","") },
                        {"pSoukoCD", prefs.GetString("souko_cd","") },
                        {"pHaisoDate", prefs.GetString("haiso_date","") },
                        {"pTokuisakiCD", data.Substring(1,4) },
                        {"pTodokesakiCD", data.Substring(5,4) },
                        {"pKanriNo", data },
                        {"pBinNo", prefs.GetString("bin_no","") },
                    };

                    Task.Run(async() => {
                        // メールバック登録処理

                        try
                        {
                            var result = await WebService.PostRestAPI<MTumikomiProc>("mail/mail040", param).ConfigureAwait(true);
                            if (result == null || result.poRet == null)
                            {
                                ShowDialog("エラー", ERR1, () => etMailBag.Text = "");
                                return;
                            }

                            switch (result.poRet.ToString())
                            {
                                case "0":
                                    //	登録解除
                                    mail_back++;
                                    mailBagSu.Text = "(" + mail_back + ")";
                                    Vibrate();
                                    break;
                                case "1":
                                    //	該当コースが既に積込中以上のステータスのためエラー
                                    ShowDialog("エラー", ERR3, () => etMailBag.Text = "");
                                    break;
                                case "2":
                                    //	該当コースが既に出発受付以上のステータスのためエラー
                                    ShowDialog("エラー", ERR4, () => etMailBag.Text = "");
                                    break;
                                case "3":
                                    //	コース割付マスタに存在しないためエラー
                                    ShowDialog("エラー", ERR5, () => etMailBag.Text = "");
                                    Vibrate();
                                    break;
                                case "9":
                                    //	登録済
                                    ShowDialog("エラー", ERR2, () => etMailBag.Text = "");
                                    break;
                                default:
                                    break;
                            }
                        }
                        catch (Exception)
                        {
                            ShowDialog("エラー", ERR1, () => etMailBag.Text = "");
                            return;
                        }
                    });
                });
            }
        }
    }
}