﻿using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Android.App;
using Android.OS;
using Android.Views;
using Android.Widget;
using HHT.Resources.Model;
using Newtonsoft.Json;

namespace HHT
{
    public class TsumikomiSearchFragment : BaseFragment
    {
        private TsumikomiTenpoListAdapter todokesakiAdapter;
        
        ListView tenpoListView;

        public override View OnCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
        {
            View view = inflater.Inflate(Resource.Layout.fragment_tsumikomi_search, container, false);
            
            SetActionBarTitle("積込検品");
            
            tenpoListView = view.FindViewById<ListView>(Resource.Id.listView1);

            return view;
        }

        public override void OnResume()
        {
            base.OnResume();

            GetTenpoList();
        }

        private void GetTenpoList()
        {
            ShowProgress("読み込み中...");
            
            Task.Run(async () =>
            {
                var contents = @"
[{'tokuisaki_cd':'471','todokesaki_cd':'0','tokuisaki_rk':'Ａ　峡西店','type':'2','tumi_kei':1,'tumi_sumi':0,'nohin_yti_time':'00:00','kansen_kbn':'0','kagosyaCount':1,'carryCount':0},{'tokuisaki_cd':'513','todokesaki_cd':'0','tokuisaki_rk':'Ａ　笛吹店','tumi_kei':0,'tumi_sumi':0,'nohin_yti_time':'00:00','kansen_kbn':'0','kagosyaCount':0,'carryCount':0},{'tokuisaki_cd':'740','todokesaki_cd':'0','tokuisaki_rk':'Ａ　山梨石森店','tumi_kei':0,'tumi_sumi':0,'nohin_yti_time':'00:00','kansen_kbn':'0','kagosyaCount':0,'carryCount':0},{'tokuisaki_cd':'745','todokesaki_cd':'0','tokuisaki_rk':'Ａ　上吉田店','tumi_kei':0,'tumi_sumi':0,'nohin_yti_time':'00:00','kansen_kbn':'0','kagosyaCount':0,'carryCount':0},{'tokuisaki_cd':'763','todokesaki_cd':'0','tokuisaki_rk':'Ａ　山梨中央店','tumi_kei':0,'tumi_sumi':0,'nohin_yti_time':'00:00','kansen_kbn':'0','kagosyaCount':0,'carryCount':0},{'tokuisaki_cd':'768','todokesaki_cd':'0','tokuisaki_rk':'Ａ　駒ヶ根店','tumi_kei':0,'tumi_sumi':0,'nohin_yti_time':'00:00','kansen_kbn':'0','kagosyaCount':0,'carryCount':0},{'tokuisaki_cd':'842','todokesaki_cd':'0','tokuisaki_rk':'Ａ　飯田上郷店','tumi_kei':0,'tumi_sumi':0,'nohin_yti_time':'00:00','kansen_kbn':'0','kagosyaCount':0,'carryCount':0},{'tokuisaki_cd':'891','todokesaki_cd':'0','tokuisaki_rk':'Ａ　竜王名取店','tumi_kei':0,'tumi_sumi':0,'nohin_yti_time':'00:00','kansen_kbn':'0','kagosyaCount':0,'carryCount':0},{'tokuisaki_cd':'907','todokesaki_cd':'0','tokuisaki_rk':'Ａ　甲府昭和店','tumi_kei':0,'tumi_sumi':0,'nohin_yti_time':'00:00','kansen_kbn':'0','kagosyaCount':0,'carryCount':0},{'tokuisaki_cd':'959','todokesaki_cd':'0','tokuisaki_rk':'Ａ　伊那福島店','tumi_kei':0,'tumi_sumi':0,'nohin_yti_time':'00:00','kansen_kbn':'0','kagosyaCount':0,'carryCount':0},{'tokuisaki_cd':'999','todokesaki_cd':'0','tokuisaki_rk':'Ａ　都留赤坂店','tumi_kei':0,'tumi_sumi':0,'nohin_yti_time':'00:00','kansen_kbn':'0','kagosyaCount':0,'carryCount':0},{'tokuisaki_cd':'1068','todokesaki_cd':'0','tokuisaki_rk':'Ａ　甲斐西八幡店','tumi_kei':0,'tumi_sumi':0,'nohin_yti_time':'00:00','kansen_kbn':'0','kagosyaCount':0,'carryCount':0},{'tokuisaki_cd':'1284','todokesaki_cd':'0','tokuisaki_rk':'Ａ　甲斐市役所前店','tumi_kei':0,'tumi_sumi':0,'nohin_yti_time':'00:00','kansen_kbn':'0','kagosyaCount':0,'carryCount':0},{'tokuisaki_cd':'1318','todokesaki_cd':'0','tokuisaki_rk':'Ａ　双葉響が丘店','tumi_kei':0,'tumi_sumi':0,'nohin_yti_time':'00:00','kansen_kbn':'0','kagosyaCount':0,'carryCount':0},{'tokuisaki_cd':'1373','todokesaki_cd':'0','tokuisaki_rk':'Ａ　甲州塩山店','tumi_kei':0,'tumi_sumi':0,'nohin_yti_time':'00:00','kansen_kbn':'0','kagosyaCount':0,'carryCount':0},{'tokuisaki_cd':'1420','todokesaki_cd':'0','tokuisaki_rk':'Ａ　市川大門店','tumi_kei':0,'tumi_sumi':0,'nohin_yti_time':'00:00','kansen_kbn':'0','kagosyaCount':0,'carryCount':0},{'tokuisaki_cd':'1468','todokesaki_cd':'0','tokuisaki_rk':'Ａ　飯田西鼎店','tumi_kei':0,'tumi_sumi':0,'nohin_yti_time':'00:00','kansen_kbn':'0','kagosyaCount':0,'carryCount':0},{'tokuisaki_cd':'1524','todokesaki_cd':'0','tokuisaki_rk':'Ａ　笛吹春日居店','tumi_kei':0,'tumi_sumi':0,'nohin_yti_time':'00:00','kansen_kbn':'0','kagosyaCount':0,'carryCount':0}]                
";
                var todokeList = JsonConvert.DeserializeObject<List<Haiso>>(contents, new JsonSerializerSettings { });

                //todokeList.RemoveAll(item => item.tumi_kei == 0);

                //List<Haiso> todokeList = await GetTumikomiTenpoList();

                Activity.RunOnUiThread(() =>
                {
                    if (todokeList.Count == 0)
                    {
                        ShowDialog("エラー", "表示データがありません", () => { FragmentManager.PopBackStack(); });
                    }
                    else
                    {
                        tenpoListView.ItemClick += ListView_ItemClick;

                        todokesakiAdapter = new TsumikomiTenpoListAdapter(todokeList);
                        tenpoListView.Adapter = todokesakiAdapter;
                        tenpoListView.RequestFocus();
                        tenpoListView.SetSelection(0);
                    }

                    DismissProgress();
                });
            });
        }

        private async Task<List<Haiso>> GetTumikomiTenpoList()
        {
            var param = new Dictionary<string, string>()
            {
                {"syuka_date", prefs.GetString("syuka_date", "")},
                {"bin_no", prefs.GetString("bin_no", "") },
                {"course", prefs.GetString("course", "") },
            };

            return await WebService.PostRestAPI<List<Haiso>>("tumi/tumi020", param);
        }

        void ListView_ItemClick(object sender, AdapterView.ItemClickEventArgs e)
        {
            try
            {
                var item = todokesakiAdapter[e.Position];
                var param = new Dictionary<string, string>()
                {
                    {"syuka_date", prefs.GetString("syuka_date", "")},
                    {"tokuisaki_cd", item.tokuisaki_cd },
                    {"todokesaki_cd", item.todokesaki_cd },
                };

                Task.Run(async() => {
                    var result = await WebService.PostRestAPI("tumi/tumi030", param);
                    int zoubinCount = int.Parse(result["count"]);

                    editor.PutInt("zoubin_flg", zoubinCount);
                    editor.PutString("todokesaki_cd", item.todokesaki_cd);
                    editor.PutString("tokuisaki_cd", item.tokuisaki_cd);
                    editor.PutString("tokuisaki_nm", item.tokuisaki_rk);
                    editor.PutString("kagosyaCount", item.kagosyaCount);
                    editor.PutString("carryCount", item.carryCount);

                    editor.PutString("tsumi_vendor_cd", "");
                    editor.PutString("tsumi_vendor_nm", "");
                    editor.PutString("vendor_cd", "");
                    editor.PutString("vendor_nm", "");
                    editor.PutString("start_vendor_cd", "");

                    editor.Apply();

                    StartFragment(FragmentManager, typeof(TsumikomiWorkFragment));

                });
                

            }
            catch
            {
                ShowDialog("エラー", "届先指定に失敗しました。", () => { });
                return;
            }
        }
    }
}
 