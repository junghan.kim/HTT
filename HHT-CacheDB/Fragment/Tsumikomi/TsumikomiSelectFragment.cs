﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Android.App;
using Android.OS;
using Android.Preferences;
using Android.Views;
using Com.Beardedhen.Androidbootstrap;
using Com.Densowave.Bhtsdk.Barcode;
using HHT.Resources.Model;

namespace HHT
{
    public class TsumikomiSelectFragment : BaseFragment
    {
        View view;
        BootstrapEditText etSyukaDate, etCourse;
        BootstrapButton btnConfirm;

        string binNo;
        string kansen_kbn;

        public override View OnCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
        {
            view = inflater.Inflate(Resource.Layout.fragment_tsumikomi_select, container, false);

            // TITLE SETTING
            SetActionBarTitle("積込検品");
            
            // ITEM EVENT SETTING 
            etSyukaDate = view.FindViewById<BootstrapEditText>(Resource.Id.et_tsumikomiSelect_syukaDate);
            etSyukaDate.FocusChange += (sender, e) => {
                if (e.HasFocus)
                {
                    etSyukaDate.Text = etSyukaDate.Text.Replace("/", "");
                    etSyukaDate.SetSelection(etSyukaDate.Text.Length);
                }
                else
                {
                    if (etSyukaDate.Text == "")
                        return;
                    
                    try
                    {
                        etSyukaDate.Text = CommonUtils.GetDateYYYYMMDDwithSlash(etSyukaDate.Text);
                    }
                    catch
                    {
                        ShowDialog("エラー", "正しい日付を入力してください。", () =>
                        {
                            etSyukaDate.Text = "";
                            etSyukaDate.RequestFocus();
                        });
                    }
                }
            };

            etCourse = view.FindViewById<BootstrapEditText>(Resource.Id.et_tsumikomiSelect_course);
            etCourse.KeyPress += (sender, e) => {
                if (e.Event.Action == KeyEventActions.Down && e.KeyCode == Keycode.Enter)
                {
                    e.Handled = true;
                    CommonUtils.HideKeyboard(Activity);
                    Confirm();
                }
                else
                {
                    e.Handled = false;
                }
            };

            btnConfirm = view.FindViewById<BootstrapButton>(Resource.Id.btn_tsumikomiSelect_confirm);
            btnConfirm.Click += delegate { Confirm(); };

            // FIRST FOCUS
            //etSyukaDate.Text = DateTime.Now.ToString("yyyy/MM/dd");
            etSyukaDate.Text = "2019/05/28";
            etCourse.RequestFocus();
            
            return view;
        }

        // CHECK INPUT AND MOVE TO NEXT FRAGMENT
        private void Confirm()
        {
            if(etSyukaDate.Text == "")
            {
                ShowDialog("エラー", "配送日を入力してください。", () => { etSyukaDate.RequestFocus(); });
                return;
            }

            if (etCourse.Text == "")
            {
                ShowDialog("エラー", "コースNoを入力してください。", () => { etCourse.RequestFocus(); });
                return;
            }

            ((MainActivity)this.Activity).ShowProgress("便情報を確認しています。");

            Task.Run(async () =>
            {
                string syuka_date = etSyukaDate.Text.Replace("/", "");
                var param = new Dictionary<string, string>()
                {
                    {"syuka_date",syuka_date},
                    {"course",etCourse.Text}
                };

                //TUMIKOMI010 result = await WebService.PostRestAPI<TUMIKOMI010>("tumi/tumi010", param);
                TUMIKOMI010 result = new TUMIKOMI010();
                //int state = int.Parse(result.state);
                int state = 1;
                result.state = "1";
                result.bin_no = "1";
                result.kansen_kbn = "1";

                if (state == 0)
                {
                    ShowDialog("エラー", "コースNoがみつかりません。", () => { });
                    return;
                }
                else if (int.Parse(result.state) >= 3)
                {
                    ShowDialog("エラー", "該当コースの積込みは完了しています。", () => { });
                    return;
                }

                binNo = result.bin_no;
                kansen_kbn = result.kansen_kbn;


                editor.PutString("syuka_date", syuka_date);
                editor.PutString("course", etCourse.Text);
                editor.PutString("bin_no", binNo);
                //editor.PutString("kansen_kbn", kansen_kbn);
                editor.PutString("kansen_kbn", "0");
                editor.Apply();

                ShowConfirmMessage();
                CommonUtils.HideKeyboard(this.Activity);
                
            }).ContinueWith(t => {
                Activity.RunOnUiThread(() => ((MainActivity)this.Activity).DismissDialog());
            });
        }

        private void ShowConfirmMessage()
        {
            string message = "配送日 : " + etSyukaDate.Text + "\n";
            message += "コース : " + etCourse.Text + "\n";
            message += "便No : " + binNo + "\n\n";
            message += "よろしいですか？";

            ShowDialog("確認", message, (okFlag) => 
            {
                // 積み込むメールバックあるかチェック
                // ある場合、StartFragment(FragmentManager, typeof(TsumikomiMailFragment));

                //StartFragment(FragmentManager, typeof(TsumikomiMailFragment));
                /*
                int count = WebService.RequestTumikomi230(souko_cd, kitaku_cd, syuka_date, nohin_date, bin_no, etCourse.Text);

                if (count > 0)
                {
                    StartFragment(FragmentManager, typeof(TsumikomiMailFragment));
                    return;
                }

                if (kansen_kbn == "1")
                {
                    editor.PutBoolean("scan_flg", false);
                    editor.Apply();

                    StartFragment(FragmentManager, typeof(TsumikomiWorkFragment));
                }
                else
                {
                    StartFragment(FragmentManager, typeof(TsumikomiSearchFragment));
                }
                
                */

                if (okFlag)
                    StartFragment(FragmentManager, typeof(TsumikomiSearchFragment));

            });
        }
        
        public override void OnBarcodeDataReceived(BarcodeDataReceivedEvent_ dataReceivedEvent)
        {
            IList<BarcodeDataReceivedEvent_.BarcodeData_> listBarcodeData = dataReceivedEvent.BarcodeData;

            foreach (BarcodeDataReceivedEvent_.BarcodeData_ barcodeData in listBarcodeData)
            {
                return;
            }
        }
    }
}