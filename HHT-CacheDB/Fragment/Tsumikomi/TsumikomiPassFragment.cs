﻿using Android.Content;
using Android.OS;
using Android.Preferences;
using Android.Util;
using Android.Views;
using Com.Beardedhen.Androidbootstrap;
using HHT.Resources.Model;
using System.Collections.Generic;

namespace HHT
{
    public class TsumikomiPassFragment : BaseFragment
    {
        private readonly string TAG = "TsumikomiPassFragment";
        
        private BootstrapEditText etPassword;
        private string passwrod;
        
        public override View OnCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
        {
            var view = inflater.Inflate(Resource.Layout.fragment_tsumikomi_pass, container, false);
            SetActionBarTitle("積込検品");
            
            BootstrapButton pwdConfirmBtn = view.FindViewById<BootstrapButton>(Resource.Id.btn_tsumikomiManger_pwdConfirm);
            pwdConfirmBtn.Click += delegate { CheckPassword(); };

            etPassword = view.FindViewById<BootstrapEditText>(Resource.Id.password);
            etPassword.RequestFocus();
            etPassword.KeyPress += (sender, e) => {
                if (e.Event.Action == KeyEventActions.Down && e.KeyCode == Keycode.Enter)
                {
                    e.Handled = true;
                    CommonUtils.HideKeyboard(Activity);
                    CheckPassword();
                }
                else
                {
                    e.Handled = false;
                }
            };

            try
            {
                //passwrod = WebService.RequestTumikomi130(prefs.GetString("souko_cd", ""), prefs.GetString("syuka_date", "")); // パスワード取得
                passwrod = "";
            }
            catch
            {
                passwrod = "";
            }
            
            return view;
        }

        private void CheckPassword()
        {
            if (etPassword.Text != passwrod)
            {
                ShowDialog("エラー", "認証できませんでした。", () => { });
                Log.Debug(TAG, "Password Error => input :" + etPassword.Text + ", pwd :" + passwrod);
                return;
            }

            // 強制的に積込検品を完了する
            //CreateTsumiFiles();

            string souko_cd = prefs.GetString("souko_cd", "");
            string kitaku_cd = prefs.GetString("kitaku_cd", "");
            string syuka_date = prefs.GetString("syuka_date", "");
            string tokuisaki_cd = prefs.GetString("tokuisaki_cd", "");
            string todokesaki_cd = prefs.GetString("todokesaki_cd", "");
            string bin_no = prefs.GetString("bin_no", "");
            string course = prefs.GetString("course", "");

            Dictionary<string, string> param = new Dictionary<string, string>
            {
                { "pTerminalID",  prefs.GetString("terminal_id","")},
                { "pProgramID", "TUM" },
                { "pSagyosyaCD", prefs.GetString("sagyousya_cd","") },
                { "pSoukoCD",  souko_cd},
                { "pSyukaDate", syuka_date},
                { "pBinNo", bin_no},
                { "pCourse", course },
                { "pTokuisakiCD", tokuisaki_cd },
                { "pTodokesakiCD", todokesaki_cd },
                { "pHHT_No", prefs.GetString("hht_no","") }
            };

            //配車テーブルの該当コースの各数量を実績数で更新する
            //var updateResult = WebService.CallTumiKomiProc("210", param);
            var updateResult = new MTumikomiProc();
            updateResult.poRet = "0";
            if (updateResult.poRet == "0" || updateResult.poRet == "99")
            {
                editor.PutBoolean("tenpo_zan_flg", false);
                editor.Apply();

                Activity.RunOnUiThread(() =>
                {
                    //	正常登録
                    PlayBeepOk();
                    ShowDialog("報告", "積込検品が\n完了しました。", () => {
                        FragmentManager.PopBackStack(FragmentManager.GetBackStackEntryAt(0).Id, 0);
                    });
                });
            }
            else
            {
                ShowDialog("エラー", "表示データがありません", () => { });
                return;
            }

        }
        
        // 積込完了時に生成されるファイル（納品で使います。）
        private void CreateTsumiFiles()
        {
            /*
            string souko_cd = prefs.GetString("souko_cd", "");
            string kitaku_cd = prefs.GetString("kitaku_cd", "");
            string syuka_date = prefs.GetString("syuka_date", "");
            string tokuisaki_cd = prefs.GetString("tokuisaki_cd", "");
            string todokesaki_cd = prefs.GetString("todokesaki_cd", "");
            string bin_no = prefs.GetString("bin_no", "");
            string course = prefs.GetString("course", "");
            
            // CRATE TUMIKOMI FILE
            // MAIN FILE
            List<MFile> mFiles = WebService.RequestTumikomi100(souko_cd, kitaku_cd, syuka_date, bin_no, course, tokuisaki_cd, todokesaki_cd);
            new MFileHelper().InsertALL(mFiles);

            PsFile psFile = WebService.RequestTumikomi180(souko_cd, syuka_date);
            new PsFileHelper().Insert(psFile);

            // MAILBACK FILE 
            List<MbFile> mbFiles = WebService.RequestTumikomi140(souko_cd, kitaku_cd, syuka_date, bin_no, course);
            new MbFileHelper().InsertAll(mbFiles);

            // SOUKO FILE
            SoFile soFile = WebService.RequestTumikomi160(souko_cd);
            new SoFileHelper().Insert(soFile);
            
            // VENDOR FILE
            List<MateFile> mateFile = WebService.RequestTumikomi260();
            new MateFileHelper().InsertAll(mateFile);

            // TOKUISAKI FILE
            List<TokuiFile> tokuiFile = WebService.RequestTumikomi270();
            new TokuiFileHelper().InsertAll(tokuiFile);

            Log.Debug(TAG, "CreateTsumiFiles end");
            */
        }
    }
}
 