﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Android.App;
using Android.OS;
using Android.Views;
using Android.Widget;
using Com.Beardedhen.Androidbootstrap;
using Com.Densowave.Bhtsdk.Barcode;
using HHT.Resources.Model;

namespace HHT
{
    public class TsumikomiWorkFragment : BaseFragment
    {
        private readonly string TAG = "TsumikomiWorkFragment";

        private EditText mKosuEditText;
        private EditText mCarEditText;
        private EditText mCarryEditText;
        private EditText mKargoEditText;
        private EditText mCardEditText;
        private EditText mBaraEditText;
        private EditText mSonotaEditText;
        private BootstrapButton mCompleteButton;

        private string kansen_kbn;
        private string tempCategory; // 貨物種類を臨時保存
        private string souko_cd, syuka_date, tokuisaki_cd, todokesaki_cd, bin_no, course;
        private int zoubin_flg;
        private string matehan;
        private volatile bool carLabelInputMode = false;
        private static readonly string ERR_UPDATE_001 = "更新出来ませんでした。\n再度商品をスキャンして下さい。";

        string tempKamotsuNo;
        
        public override View OnCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
        {
            View view = inflater.Inflate(Resource.Layout.fragment_tsumikomi_work, container, false);

            SetActionBarTitle("積込検品");

            TextView tokuiNm = view.FindViewById<TextView>(Resource.Id.txt_tsumikomiWork_tokuisakiNm);
            tokuiNm.Text = prefs.GetString("tokuisaki_nm", "");
            
            //　完了ボタン
            mCompleteButton = view.FindViewById<BootstrapButton>(Resource.Id.completeButton);
            mCompleteButton.Click += delegate { StartFragment(FragmentManager, typeof(TsumikomiPassFragment));};
            mCompleteButton.Enabled = (kansen_kbn == "0" && prefs.GetBoolean("scan_flg", false));
            
            tempCategory = "";
            souko_cd = prefs.GetString("souko_cd", "");
            syuka_date = prefs.GetString("syuka_date", "");
            tokuisaki_cd = prefs.GetString("tokuisaki_cd", "");
            todokesaki_cd = prefs.GetString("todokesaki_cd", "");
            bin_no = prefs.GetString("bin_no", "");
            course = prefs.GetString("course", "");
            zoubin_flg = prefs.GetInt("zoubin_flg", 1);
            kansen_kbn = prefs.GetString("kansen_kbn", "0");

            GridLayout teibanLayout = view.FindViewById<GridLayout>(Resource.Id.teibanLayout);
            teibanLayout.Visibility = ViewStates.Visible;
                
            // マテハン種類
            mKosuEditText = view.FindViewById<EditText>(Resource.Id.et_tsumikomiWork_kosu);
            mCarEditText = view.FindViewById<EditText>(Resource.Id.et_tsumikomiWork_carLabel);
            mCarryEditText = view.FindViewById<EditText>(Resource.Id.et_tsumikomiWork_carry);
            mKargoEditText = view.FindViewById<EditText>(Resource.Id.et_tsumikomiWork_kargoCar);
            mCardEditText = view.FindViewById<EditText>(Resource.Id.et_tsumikomiWork_card);
            mBaraEditText = view.FindViewById<EditText>(Resource.Id.et_tsumikomiWork_bara);
            mSonotaEditText = view.FindViewById<EditText>(Resource.Id.et_tsumikomiWork_sonota);

            mKosuEditText.SetBackgroundColor(Android.Graphics.Color.Yellow);

            //GetTenpoMatehanInfo();  // 作業5, 6 定番コースzoubin_flg = 1)

            string kagosyaCount = prefs.GetString("kagosyaCount", "0");
            string carryCount = prefs.GetString("carryCount", "0");

            mKargoEditText.Text = kagosyaCount;
            mCarryEditText.Text = carryCount;

            return view;
        }

        // TRG ボタン押した時
        public override void OnBarcodeDataReceived(BarcodeDataReceivedEvent_ dataReceivedEvent)
        {
            IList<BarcodeDataReceivedEvent_.BarcodeData_> listBarcodeData = dataReceivedEvent.BarcodeData;
            
            foreach (BarcodeDataReceivedEvent_.BarcodeData_ barcodeData in listBarcodeData)
            {
                // スキャン無効化
                ((MainActivity)this.Activity).DisableScanning();

                Dictionary<string, string> param = GetProcParam(barcodeData.Data);

                if (carLabelInputMode == false)
                {
                    Activity.RunOnUiThread(() =>ShowProgress("読み込み中"));
                    
                    Task.Run(async () =>
                    {
                        try
                        {
                            param = new Dictionary<string, string>()
                            {
                                { "pTerminalID",  prefs.GetString("terminal_id","")},
                                { "pSagyosyaCD", prefs.GetString("sagyousya_cd","") },
                                { "pSyukaDate", syuka_date},
                                { "pKamotsuNo", barcodeData.Data },
                                { "tokuisakiCd", tokuisaki_cd },
                                { "todokesakiCd", todokesaki_cd },
                                { "binNo", bin_no },
                            };

                            tempKamotsuNo = barcodeData.Data;

                            string url = "tumi/tumi060";
                            MTumikomiProc result = await WebService.PostRestAPI<MTumikomiProc>(url, param);

                            if (result.poMsg != null && result.poMsg != "")
                            {
                                ShowDialog("エラー", result.poMsg, () => {
                                    ((MainActivity)this.Activity).EnableScanning();
                                });
                                return;
                            }

                            tempCategory = result.poCategory;
                            matehan = result.poMatehan;
                            carLabelInputMode = true;

                            Activity.RunOnUiThread(() => {
                                //	正常登録
                                mKosuEditText.Text = result.poKosuCnt;
                                mCarEditText.SetBackgroundColor(Android.Graphics.Color.Yellow);
                                mKosuEditText.SetBackgroundColor(Android.Graphics.Color.White);
                            });
                        }
                        catch (Exception e)
                        {
                            ShowDialog("エラー", e.Message, () => {
                                ((MainActivity)this.Activity).EnableScanning();
                            });
                        }
                        
                    }).ContinueWith(t =>
                    {
                        // UIスレッドでの処理
                        ((MainActivity)this.Activity).EnableScanning();
                        Activity.RunOnUiThread(()=> ((MainActivity)this.Activity).DismissDialog());
                        
                    });
                }
                else
                {
                    // 作業ステータス更新・積込処理
                    Activity.RunOnUiThread(() => mCarEditText.Text = barcodeData.Data);

                    UpdateSagyoStatus(barcodeData.Data);
                }
            }
        }

        // 総個数取得 TUMIKOMI050
        private int GetCountSouko()
        {
            ((MainActivity)this.Activity).ShowProgress("マテハン情報取得しています。");
            int count = 0;

            Task.Run(() =>
            {


            }).ContinueWith(x => {
                Activity.RunOnUiThread(() => ((MainActivity)this.Activity).DismissDialog());

            });

            return count;
        }

        // 作業ステータス更新・積込処理 TUMIKOMI080,TUMIKOMI311
        private async void UpdateSagyoStatus(string saryouData)
        {
            Activity.RunOnUiThread(() => ShowProgress("読み込み中"));

            var param = GetProcParam(saryouData);
            param.Add("pKamotsuNo", tempKamotsuNo);

            int resultCode = 1;
            
            try
            {                
                MTumikomiProc result;
                // 増便未対応
                if (zoubin_flg > 1)
                {
                    result = await WebService.PostRestAPI<MTumikomiProc>("tumi/tumi080", param);

                    if (result.poMsg == null || result.poMsg == "")
                    {
                        resultCode = 8;
                    }
                    else
                    {
                        resultCode = int.Parse(result.poMsg);
                    }

                    if (resultCode == 0)
                    {
                        StartFragment(FragmentManager, typeof(TsumikomiConfirmFragment));
                        editor.PutBoolean("scan_flg", true);
                        editor.Apply();
                    }
                    else if (resultCode == 8)
                    {
                        Activity.RunOnUiThread(() =>
                        {
                            ((MainActivity)this.Activity).DismissDialog();

                            ShowDialog("エラー", "更新できませんでした。\n管理者に連絡してください。", () => {
                                ((MainActivity)this.Activity).EnableScanning();
                            });
                        });

                        return;
                    }
                    else
                    {
                        Activity.RunOnUiThread(() =>
                        {
                            mKosuEditText.SetBackgroundColor(Android.Graphics.Color.Yellow);
                            mCarEditText.SetBackgroundColor(Android.Graphics.Color.White);
                            
                            if (tempCategory == "00")
                            {
                                mCarryEditText.Text = (int.Parse(mCarryEditText.Text) - 1).ToString();
                            }
                            else if (tempCategory == "01")
                            {
                                mKargoEditText.Text = (int.Parse(mKargoEditText.Text) - 1).ToString();
                            }
                            else if (tempCategory == "02")
                            {
                                mCardEditText.Text = (int.Parse(mCardEditText.Text) - 1).ToString();
                            }
                            else if (tempCategory == "03")
                            {
                                mBaraEditText.Text = (int.Parse(mBaraEditText.Text) - 1).ToString();
                            }
                            else if (tempCategory == "04")
                            {
                                mSonotaEditText.Text = (int.Parse(mSonotaEditText.Text) - 1).ToString();
                            }

                            tempCategory = "";

                            mKosuEditText.Text = "";
                            mCarEditText.Text = "";

                            carLabelInputMode = false;

                            editor.PutBoolean("scan_flg", true);
                            editor.Apply();

                            mCompleteButton.Enabled = true;

                            ((MainActivity)this.Activity).EnableScanning();
                            ((MainActivity)this.Activity).DismissDialog();
                        });
                    }
                }
                else
                {
                    await Task.Run(async() =>
                     {
                         string url = "tumi/tumi080";
                         result = await WebService.PostRestAPI<MTumikomiProc>(url, param);

                         if (string.IsNullOrEmpty(result.poMsg))
                         {
                             resultCode = 8;
                         }
                         else
                         {
                             resultCode = int.Parse(result.poMsg);
                         }

                         return result;

                     }).ContinueWith(x =>
                     {
                         //全部完了
                         if (resultCode == 0)
                         {
                             int page = x.Result.poRet == "0" ? 0 : 2;

                             //	正常登録
                             PlayBeepOk();
                             ShowDialog("報告", "積込検品が\n完了しました。", () =>
                             {
                                 ((MainActivity)this.Activity).EnableScanning();
                                 DismissProgress();
                                 FragmentManager.PopBackStack(FragmentManager.GetBackStackEntryAt(page).Id, 0);
                             });
                         }
                         else if (resultCode == 1)
                         {
                             Activity.RunOnUiThread(() =>
                             {
                                 mKosuEditText.SetBackgroundColor(Android.Graphics.Color.Yellow);
                                 mCarEditText.SetBackgroundColor(Android.Graphics.Color.White);

                                 if (tempCategory == "00")
                                 {
                                     mCarryEditText.Text = (int.Parse(mCarryEditText.Text) - 1).ToString();
                                 }
                                 else if (tempCategory == "01")
                                 {
                                     mKargoEditText.Text = (int.Parse(mKargoEditText.Text) - 1).ToString();
                                 }
                                 else if (tempCategory == "02")
                                 {
                                     mCardEditText.Text = (int.Parse(mCardEditText.Text) - 1).ToString();
                                 }
                                 else if (tempCategory == "03")
                                 {
                                     mBaraEditText.Text = (int.Parse(mBaraEditText.Text) - 1).ToString();
                                 }
                                 else if (tempCategory == "04")
                                 {
                                     mSonotaEditText.Text = (int.Parse(mSonotaEditText.Text) - 1).ToString();
                                 }

                                 tempCategory = "";

                                 mKosuEditText.Text = "";
                                 mCarEditText.Text = "";

                                 carLabelInputMode = false;

                                 mCompleteButton.Enabled = true;

                                 ((MainActivity)this.Activity).EnableScanning();
                                 ((MainActivity)this.Activity).DismissDialog();

                             });
                         }
                         else if(resultCode == 2)
                         {
                             carLabelInputMode = false;

                             ((MainActivity)this.Activity).DismissDialog();

                             ShowDialog("警告", "積込可能な商品があります。\n積込みを完了\nしますか？", (flag) =>
                             {
                                 ((MainActivity)this.Activity).EnableScanning();

                                 if (flag)
                                 {
                                     // 強制積込完了
                                     carLabelInputMode = true;

                                     try
                                     {
                                         //配車テーブルの該当コースの各数量を実績数で更新する
                                         //MTumikomiProc updateResult = await WebService.CallTumiKomiProcAsync(kansen_kbn == "0" ? "210" : "314", param);
                                         MTumikomiProc updateResult = new MTumikomiProc();
                                         if (updateResult.poRet == "0")
                                         {
                                             PlayBeepOk();

                                             // 正常登録
                                             ShowDialog("報告", "積込検品が\n完了しました。", () =>
                                             {
                                                 FragmentManager.PopBackStack(FragmentManager.GetBackStackEntryAt(0).Id, 0);
                                             });

                                             editor.PutBoolean("scan_flg", true);
                                             editor.Apply();
                                         }
                                         else if (updateResult.poRet == "99")
                                         {
                                             // 正常登録
                                             PlayBeepOk();

                                             ShowDialog("報告", "積込検品が\n完了しました。", () =>
                                             {
                                                 FragmentManager.PopBackStack(FragmentManager.GetBackStackEntryAt(2).Id, 0);
                                             });

                                             editor.PutBoolean("scan_flg", true);
                                             editor.Apply();
                                         }
                                         else
                                         {
                                             ShowDialog("エラー", "表示データがありません", () =>
                                             {
                                                 FragmentManager.PopBackStack(FragmentManager.GetBackStackEntryAt(0).Id, 0);
                                             });
                                             return;
                                         }
                                     }
                                     catch (Exception e)
                                     {
                                         ShowDialog("エラー", e.Message, () =>
                                         {
                                             ((MainActivity)this.Activity).EnableScanning();
                                             FragmentManager.PopBackStack(FragmentManager.GetBackStackEntryAt(0).Id, 0);
                                         });
                                     }

                                 }
                                 else
                                 {
                                     // 残り積込検品を続ける
                                     GetTenpoMatehanInfo();

                                     mKosuEditText.SetBackgroundColor(Android.Graphics.Color.Yellow);
                                     mCarEditText.SetBackgroundColor(Android.Graphics.Color.White);

                                     mKosuEditText.Text = "";
                                     mCarEditText.Text = "";

                                     carLabelInputMode = false;
                                     mCompleteButton.Enabled = true;

                                     ((MainActivity)this.Activity).EnableScanning();

                                 }

                             });
                         }
                     });
                }
            }
            catch
            {
                ((MainActivity)this.Activity).DismissDialog();
                
                ShowDialog("エラー", "更新できませんでした。\n管理者に連絡してください。", () => {
                    ((MainActivity)this.Activity).EnableScanning();
                });
                return;
            }

            tempKamotsuNo = "";
        }

        // マテハン情報取得 TUMIKOMI040,TUMIKOMI300
        private async void GetTenpoMatehanInfo()
        {
            ((MainActivity)this.Activity).ShowProgress("マテハン情報取得しています。");
            
            List<TUMIKOMI040> resultList = new List<TUMIKOMI040>();

            try
            {
                var param = new Dictionary<string, string>()
                {
                    {"syuka_date", syuka_date},
                    {"tokuisaki_cd", tokuisaki_cd },
                    {"todokesaki_cd", todokesaki_cd },
                    {"bin_no", bin_no },
                    {"course", course }
                };

                // 該当店舗の各マテハン数を取得(定番コース)
                resultList = await WebService.PostRestAPI<List<TUMIKOMI040>>("tumi/tumi040", param);
            }
            catch(Exception e)
            {
                ShowDialog("エラー", e.Message, () => {
                    ((MainActivity)this.Activity).EnableScanning();
                });
            }
            
            foreach (TUMIKOMI040 result in resultList)
            {
                string btvCategory = result.name_cd;
                string btvKosu = result.cnt;

                Activity.RunOnUiThread(() =>
                {
                    if (btvCategory == "00")
                    {
                        mCarryEditText.Text = btvKosu;
                    }
                    else if (btvCategory == "01")
                    {
                        mKargoEditText.Text = btvKosu;
                    }
                    else if (btvCategory == "02")
                    {
                        mCardEditText.Text = btvKosu;
                    }
                    else if (btvCategory == "03")
                    {
                        mBaraEditText.Text = btvKosu;
                    }
                    else if (btvCategory == "04")
                    {
                        mSonotaEditText.Text = btvKosu;
                    }

                    ((MainActivity)this.Activity).DismissDialog();
                });
            }

             ((MainActivity)this.Activity).DismissDialog();
        }
        
        public override bool OnBackPressed()
        {
            if (carLabelInputMode)
            {
                CancelTsumiKomi();
                mKosuEditText.Text = "0";
                mKosuEditText.SetBackgroundColor(Android.Graphics.Color.Yellow);
                mCarEditText.SetBackgroundColor(Android.Graphics.Color.White);
            }
            else
            {
                return true;
            }

            return false;
        }

        private void CancelTsumiKomi()
        {
            new Thread(new ThreadStart(delegate {
                Activity.RunOnUiThread(async () =>
                {
                    Dictionary<string, string> param = GetProcParam("");

                    try
                    {
                        string url = kansen_kbn == "0" ? "tumi/tumi090" : "tumi/tumi312";
                        MTumikomiProc result = await WebService.PostRestAPI<MTumikomiProc>(url, param);

                        if (result.poMsg != null && result.poMsg != "")
                        {
                            ShowDialog("エラー", result.poMsg, () => { });
                            return;
                        }

                        if (result.poRet == "0")
                        {
                            carLabelInputMode = false;
                        }
                        else if (result.poRet == "8")
                        {
                            ShowDialog("エラー", ERR_UPDATE_001, () => { });
                            return;
                        }
                    }
                    catch (Exception e)
                    {
                        ShowDialog("エラー", e.Message, () => {
                            ((MainActivity)this.Activity).EnableScanning();
                        });
                    }

                    
                }
                );
            }
            )).Start();
        }

        // PROC専用のパラメータ設定
        private Dictionary<string, string> GetProcParam(string barcodeData)
        {
            return new Dictionary<string, string>
                        {
                            { "pTerminalID",  prefs.GetString("terminal_id","")},
                            { "pProgramID", "TUM" },
                            { "pSagyosyaCD", prefs.GetString("sagyousya_cd","") },
                            { "pSoukoCD",  souko_cd},
                            { "pSyukaDate", syuka_date},
                            { "pBinNo", bin_no},
                            { "pCourse", course },
                            { "pMatehan", matehan },
                            { "pTokuisakiCD", tokuisaki_cd },
                            { "pTodokesakiCD", todokesaki_cd },
                            { carLabelInputMode == false ? "pKamotsuNo" : "pSyaryoNo", barcodeData },
                            { "pHHT_No", prefs.GetString("hht_no","") }
                        };
        }

        public override bool OnKeyDown(Keycode keycode, KeyEvent paramKeyEvent)
        {
            // TODO 削除予定
            if (CommonUtils.IsEmulator())
            {
                if (keycode == Keycode.Apostrophe)
                {
                    if (carLabelInputMode == false)
                    {
                        var barcodeData = "02320529061030139410";

                        Activity.RunOnUiThread(() => ((MainActivity)Activity).ShowProgress("読み込み中"));

                        Task.Run(async () =>
                        {
                            try
                            {
                                var param = new Dictionary<string, string>()
                                {
                                    { "pTerminalID",  prefs.GetString("terminal_id","")},
                                    { "pSagyosyaCD", prefs.GetString("sagyousya_cd","") },
                                    { "pSyukaDate", syuka_date},
                                    { "pKamotsuNo", barcodeData }
                                };

                                tempKamotsuNo = barcodeData;

                                //string url = zoubin_flg > 1 || kansen_kbn == "0" ? "tumi/tumi060" : "tumi/tumi310";
                                string url = "tumi/tumi060";
                                MTumikomiProc result = await WebService.PostRestAPI<MTumikomiProc>(url, param);

                                if (result.poMsg != null && result.poMsg != "")
                                {
                                    ShowDialog("エラー", result.poMsg, () => {
                                        ((MainActivity)this.Activity).EnableScanning();
                                    });
                                    return;
                                }

                                tempCategory = result.poCategory;
                                matehan = result.poMatehan;
                                carLabelInputMode = true;

                                Activity.RunOnUiThread(() => {
                                    //	正常登録
                                    mKosuEditText.Text = result.poKosuCnt;
                                    mCarEditText.SetBackgroundColor(Android.Graphics.Color.Yellow);
                                    mKosuEditText.SetBackgroundColor(Android.Graphics.Color.White);
                                });
                            }
                            catch (Exception e)
                            {
                                ShowDialog("エラー", e.Message, () => {
                                    ((MainActivity)this.Activity).EnableScanning();
                                });
                            }

                        }).ContinueWith(t =>
                        {
                            // UIスレッドでの処理
                            ((MainActivity)this.Activity).EnableScanning();
                            Activity.RunOnUiThread(() => ((MainActivity)this.Activity).DismissDialog());

                        });
                    }
                    else
                    {
                        var barcodeData = "A1234";

                        // 作業ステータス更新・積込処理
                        Activity.RunOnUiThread(() => mCarEditText.Text = barcodeData);

                        UpdateSagyoStatus(barcodeData);
                    }
                }
            }

            return true;
        }

    }
}
 