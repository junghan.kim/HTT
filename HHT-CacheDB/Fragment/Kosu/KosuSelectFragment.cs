﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Android.App;
using Android.OS;
using Android.Views;
using Android.Widget;
using Com.Densowave.Bhtsdk.Barcode;
using Com.Beardedhen.Androidbootstrap;
using Android.Util;

namespace HHT
{
    public class KosuSelectFragment : BaseFragment
    {
        private readonly string TAG = "KosuSelectFragment";

        GridLayout mTodokeLayout;
        GridLayout mVendorLayout;

        BootstrapEditText mSyukaDateEditText;
        BootstrapEditText mTokuisakiEditText;
        BootstrapEditText mTodokesakiEditText;
        BootstrapEditText mVendorEditText;
        
        BootstrapButton mSearchButton;
        BootstrapButton mConfirmButton;

        private int kosuMenuflag;
        private string vendorNm;
        
        public override View OnCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
        {
            View view = inflater.Inflate(Resource.Layout.fragment_kosu_select, container, false);

            mTodokeLayout = view.FindViewById<GridLayout>(Resource.Id.gl_kosuSelect_Todoke);
            mVendorLayout = view.FindViewById<GridLayout>(Resource.Id.gl_kosuSelect_Vendor);

            mSyukaDateEditText = view.FindViewById<BootstrapEditText>(Resource.Id.todoke_et_deliveryDate);
            mTokuisakiEditText = view.FindViewById<BootstrapEditText>(Resource.Id.tokuiCode);
            mTodokesakiEditText = view.FindViewById<BootstrapEditText>(Resource.Id.todokeCode);
            mVendorEditText = view.FindViewById<BootstrapEditText>(Resource.Id.vendorCode);
            
            mSearchButton = view.FindViewById<BootstrapButton>(Resource.Id.todokeSearch);
            mConfirmButton = view.FindViewById<BootstrapButton>(Resource.Id.btn_todoke_confirm);

            // PARAM
            kosuMenuflag = Arguments.GetInt("menu");
            
            // INIT
            InitComponent();

            // TEST
            //mSyukaDateEditText.Text = "2019/05/28";

            return view;
        }

        private void InitComponent()
        {
            // 検品作業初期化
            editor.PutString("ko_su", "0");
            editor.PutString("dai_su", "0");
            editor.Apply();
            
            // 配送日設定
            mSyukaDateEditText.Text = DateTime.Now.ToString("yyyy/MM/dd");
            mSyukaDateEditText.FocusChange += (sender, e) => {
                if (e.HasFocus)
                {
                    mSyukaDateEditText.Text = mSyukaDateEditText.Text.Replace("/", "");
                    mSyukaDateEditText.SetSelection(mSyukaDateEditText.Text.Length);
                }
                else
                {
                    ChangeDateFormat();
                }
            };

            // 確定ボタン
            mConfirmButton.Click += delegate { Confirm(); };
            
            if (kosuMenuflag == (int)KOSUMENU.TODOKE)
            {
                SetActionBarTitle("配送先指定検品");
                
                mTodokeLayout.Visibility = ViewStates.Visible;

                mSearchButton.Click += delegate { SearchTodokesaki(); };
                mSearchButton.Enabled = false;

                mTokuisakiEditText.FocusChange += (sender, e) =>
                {
                    if (e.HasFocus)
                    {
                        mSearchButton.Enabled = true;
                    }
                    else
                    {
                        mSearchButton.Enabled = false;
                    }
                };
                mTodokesakiEditText.KeyPress += (sender, e) => {
                    if (e.Event.Action == KeyEventActions.Down && e.KeyCode == Keycode.Enter)
                    {
                        e.Handled = true;
                        Confirm();
                    }
                    else
                    {
                        e.Handled = false;
                    }
                };
                
                //初期フォーカス
                if (mTokuisakiEditText.Text == "")
                {
                    mTokuisakiEditText.Text = "237825";
                    mTokuisakiEditText.RequestFocus();
                }
            }
            else if (kosuMenuflag == (int)KOSUMENU.VENDOR)
            {
                SetActionBarTitle("ベンダー指定検品");
                mVendorLayout.Visibility = ViewStates.Visible;
                mSearchButton.Click += delegate { GoVendorSearchPage(); };
                mVendorEditText.FocusChange += (sender, e) => { mSearchButton.Enabled = e.HasFocus; };
                mVendorEditText.KeyPress += (sender, e) => {
                    if (e.Event.Action == KeyEventActions.Down && e.KeyCode == Keycode.Enter)
                    {
                        e.Handled = true;
                        Confirm();
                    }
                    else
                    {
                        e.Handled = false;
                    }
                };
                
                //初期フォーカス
                mVendorEditText.RequestFocus();
            }
            else if (kosuMenuflag == (int)KOSUMENU.BARA || kosuMenuflag == (int)KOSUMENU.ARATA_CASE || kosuMenuflag == (int)KOSUMENU.ARATA_ORICON)
            {
                if(kosuMenuflag == (int)KOSUMENU.BARA)
                {
                    SetActionBarTitle("バラ検品");
                }
                else if(kosuMenuflag == (int)KOSUMENU.ARATA_CASE)
                {
                    SetActionBarTitle("かご車紐づけ作業");
                }
                else if (kosuMenuflag == (int)KOSUMENU.ARATA_ORICON)
                {
                    SetActionBarTitle("キャリー紐づけ作業");
                }

                mSearchButton.Visibility = ViewStates.Gone;
                mSyukaDateEditText.KeyPress += (sender, e) => {
                    if (e.Event.Action == KeyEventActions.Down && e.KeyCode == Keycode.Enter)
                    {
                        e.Handled = true;
                        Confirm();
                    }
                    else
                    {
                        e.Handled = false;
                    }
                };
                mSyukaDateEditText.SetSelection(mSyukaDateEditText.Text.Length);
                mSyukaDateEditText.RequestFocus();
            }
        }
    
        private async Task<bool> IsNotExistTokuisaki()
        {
            var result = await PostRestAPI("", "kosu/kosu040", new Dictionary<string, string>()
            {
                { "syuka_date", mSyukaDateEditText.Text.Replace("/", "")},
                { "tokuisaki_cd", mTokuisakiEditText.Text },
                { "todokesaki_cd", mTodokesakiEditText.Text }
            });

            if (result.ContainsKey("cnt")){
                return await Task.FromResult("0".Equals(result["cnt"]));
            }
            else
            {
                return false;
            }
        }
        
        private async Task<string> GetVendorState()
        {
            string venderState = "";
            
            try
            {
                string syukaDate = mSyukaDateEditText.Text.Replace("/", "");
                //KOSU131 kosu131 = WebService.RequestKosu131(soukoCd, kitakuCd, syukaDate, etVendorCode.Text);

                //venderState = kosu131.state;
                //vendorNm = kosu131.vendor_nm;
            }
            catch
            {
                Log.Error(TAG, "KOSU131 ERROR");
                vendorNm = "";
            }

            return await Task.FromResult(venderState);
        }

        public override void OnBarcodeDataReceived(BarcodeDataReceivedEvent_ dataReceivedEvent)
        {
            IList<BarcodeDataReceivedEvent_.BarcodeData_> listBarcodeData = dataReceivedEvent.BarcodeData;

            foreach (BarcodeDataReceivedEvent_.BarcodeData_ barcodeData in listBarcodeData)
            {
                this.Activity.RunOnUiThread(() =>
                {
                    string data = barcodeData.Data;

                    if (mTokuisakiEditText.HasFocus)
                    {
                        mTokuisakiEditText.Text = data;
                    }
                });
            }
        }

        private void Confirm()
        {
            Task.Run(async () =>
            {
                return await CheckValidation();
            }).ContinueWith(t=>{

                if (t.Result == false)
                {
                    return;
                }

                if (kosuMenuflag == (int)KOSUMENU.TODOKE)
                {
                    GoTodokeKenpin();
                }
                else if (kosuMenuflag == (int)KOSUMENU.VENDOR)
                {
                    var message =
                            @"配送日：" + mSyukaDateEditText.Text
                        + "\nベンダー：" + mVendorEditText.Text
                        + "\n" + vendorNm
                        + "\n\n" + "よろしいですか？";

                    ShowDialog("確認", message, (okFlag) => {
                        if (okFlag)
                            GoVendorKenpin();
                    });
                }
                else if (kosuMenuflag == (int)KOSUMENU.BARA)
                {
                    GoBaraKenpin();
                }
                else if (kosuMenuflag == (int)KOSUMENU.ARATA_CASE)
                {
                    editor.PutString("syuka_date", mSyukaDateEditText.Text.Replace("/", ""));
                    editor.Apply();
                    StartFragment(FragmentManager, typeof(KosuKagosyaFragment));
                }
                else if (kosuMenuflag == (int)KOSUMENU.ARATA_ORICON)
                {
                    editor.PutString("syuka_date", mSyukaDateEditText.Text.Replace("/", ""));
                    editor.Remove("mCarryBarcodes");
                    editor.Remove("mCarryCaseKbns");
                    editor.Apply();

                    StartFragment(FragmentManager, typeof(KosuCarryFragment));
                }


            });
        }

        private async Task<bool> CheckValidation()
        {

            if (mSyukaDateEditText.Text == "")
            {
                ShowDialog("エラー", "配送日を確認してください。", () => mSyukaDateEditText.RequestFocus());
                return false;
            }

            // Type Check
            string syukaDate = mSyukaDateEditText.Text.Replace("/", "");
            try
            {
                CommonUtils.GetDateYYYYMMDDwithSlash(mSyukaDateEditText.Text);
            }
            catch
            {
                ShowDialog("エラー", "正しい日付を入力してください。", () =>
                {
                    mSyukaDateEditText.Text = "";
                    mSyukaDateEditText.RequestFocus();
                });

                return false;
            }

            if (kosuMenuflag == (int)KOSUMENU.TODOKE)
            {
                if (mTokuisakiEditText.Text == "")
                {
                    ShowDialog("エラー", "配送先コードを入力してください。", () =>mTokuisakiEditText.RequestFocus());
                    return false;
                }
                if (mTodokesakiEditText.Text == "")
                {
                    ShowDialog("エラー", "届先コードを入力してください。", () => mTodokesakiEditText.RequestFocus());
                    return false;
                }

                if (await IsNotExistTokuisaki())
                {
                    ShowDialog("エラー", "配送先コードがみつかりません。", () =>
                    {
                        mTokuisakiEditText.Text = "";
                        mTodokesakiEditText.Text = "";
                        mTokuisakiEditText.RequestFocus();
                    });
                    return false;
                }
            }
            else if (kosuMenuflag == (int)KOSUMENU.VENDOR)
            {
                if (mVendorEditText.Text == "")
                {
                    ShowDialog("エラー", "ベンダーコードを入力してください。", () => mVendorEditText.RequestFocus());
                    return false;
                }

                // ベンダー情報検索
                syukaDate = mSyukaDateEditText.Text;
                string errTitle = "";
                string errMsg = "";

                //string resultState = await GetVendorState();
                string resultState = "";

                if (vendorNm == "")
                {
                    errTitle = "エラー";
                    errMsg = "ベンダーコードがみつかりません。";
                }
                else if (!(resultState == "00" || resultState == "01" || resultState == "99"))
                {
                    errTitle = "報告";
                    errMsg = "全ての検品が完了しています。";
                }
                else if (resultState != "00")
                {
                    errTitle = "エラー";
                    errMsg = "検品可能なベンダーではありません。";
                }

                if (errMsg.Length > 0)
                {
                    ShowDialog(errTitle, errMsg, () =>
                    {
                        mVendorEditText.Text = "";
                        mVendorEditText.RequestFocus();
                    });
                    return false;
                }
            }

            return true;
        }

        private void SearchTodokesaki()
        {
            if (mSyukaDateEditText.Text == "")
            {
                ShowDialog("エラー", "配送日を入力してください。", () => mSyukaDateEditText.RequestFocus());
                return;
            }

            editor.PutString("deliveryDate", mSyukaDateEditText.Text);
            editor.PutString("tokuisaki_cd", "");
            editor.PutString("todokesaki_cd", "");
            editor.Apply();

            Bundle bundle = new Bundle();
            bundle.PutBoolean("searchFlag", true); // 届先検索フラグ設定

            StartFragment(FragmentManager, typeof(KosuBinInputFragment), bundle);
        }

        private bool ChangeDateFormat()
        {
            try
            {
                mSyukaDateEditText.Text = CommonUtils.GetDateYYYYMMDDwithSlash(mSyukaDateEditText.Text);
            }
            catch
            {
                ShowDialog("エラー", "正しい日付を入力してください。", () =>
                {
                    mSyukaDateEditText.Text = "";
                    mSyukaDateEditText.RequestFocus();
                });

                return false;
            }

            return true;
        }

        // ************ ページ遷移 ************

        private void GoVendorSearchPage()
        {
            string syukaDate = mSyukaDateEditText.Text;

            if (mSyukaDateEditText.Text == "")
            {
                ShowDialog("エラー", "配送日を確認してください。", () =>
                {
                    mSyukaDateEditText.RequestFocus();
                });
                return;
            }
            
            editor.PutString("syuka_date", syukaDate.Replace("/", ""));
            editor.Apply();

            StartFragment(FragmentManager, typeof(KosuVendorSearchFragment));
        }

        private void GoTodokeKenpin()
        {
            editor.PutString("syuka_date", mSyukaDateEditText.Text.Replace("/", ""));

            editor.PutString("deliveryDate", mSyukaDateEditText.Text);
            editor.PutString("tokuisaki_cd", mTokuisakiEditText.Text);
            editor.PutString("todokesaki_cd", mTodokesakiEditText.Text);
            editor.Apply();

            Bundle bundle = new Bundle();
            bundle.PutBoolean("searchFlag", false);// 届先検索フラグ設定

            StartFragment(FragmentManager, typeof(KosuBinInputFragment), bundle);
        }

        private void GoVendorKenpin()
        {
            editor.PutString("syuka_date", mSyukaDateEditText.Text.Replace("/", ""));

            editor.PutString("vendor_cd", mVendorEditText.Text);
            editor.PutString("vendor_nm", vendorNm);
            editor.PutString("tokuisaki_nm", "");
            editor.Apply();

            StartFragment(FragmentManager, typeof(KosuWorkFragment));
        }

        private void GoBaraKenpin()
        {
            editor.PutString("syuka_date", mSyukaDateEditText.Text.Replace("/", ""));
            
            editor.PutString("vendor_cd", "");
            editor.PutString("vendor_nm", "");
            editor.PutString("tokuisaki_nm", "");
            editor.Apply();

            StartFragment(FragmentManager, typeof(KosuWorkFragment));
        }

    }
}