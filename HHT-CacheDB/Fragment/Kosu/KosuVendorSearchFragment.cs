﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Android.App;
using Android.OS;
using Android.Views;
using Android.Widget;
using HHT.Resources.Model;

namespace HHT
{
    public class KosuVendorSearchFragment : BaseFragment
    {
        private VendorAdapter vendorAdapter;
        private List<KOSU095> vendorList;
        
        public override View OnCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
        {
            View view = inflater.Inflate(Resource.Layout.fragment_kosu_vendor_search, container, false);
            
            SetTodokesakiAsync(view);

            return view;
        }

        private void SetTodokesakiAsync(View view)
        {
            var param = new Dictionary<string, string>()
            {
                { "syuka_date", prefs.GetString("syuka_date", "")}
            };

            Task.Run(async () =>
            {
                try
                {
                    vendorList = await PostRestAPI<List<KOSU095>>("", "kosu/kosu095", param);

                    if (vendorList.Count > 0)
                    {
                        ListView listView = view.FindViewById<ListView>(Resource.Id.listView1);
                        listView.ItemClick += listView_ItemClick;

                        Activity.RunOnUiThread(() => {
                            vendorAdapter = new VendorAdapter(vendorList);
                            listView.Adapter = vendorAdapter;
                            listView.RequestFocus();
                            listView.SetSelection(0);
                        });
                    }
                    else
                    {
                        ShowDialog("エラー", "表示データがありません。", () => FragmentManager.PopBackStack());
                    }
                }
                catch
                {
                    ShowDialog("エラー", "表示データがありません。", () => FragmentManager.PopBackStack());
                }
            });
        }

        void listView_ItemClick(object sender, AdapterView.ItemClickEventArgs e)
        {
            var item = vendorList[e.Position];

            var message =
                @"配送日：" + prefs.GetString("syuka_date", "20180320")
            + "\nベンダー：" + item.vendor_cd
            + "\nベンダー名："
            + "\n" + item.vendor_nm
            + "\n\n" + "よろしいですか？";

            ShowDialog("確認", message, (flag) => {
                if (flag)
                {
                    editor.PutString("vendor_cd", item.vendor_cd);
                    editor.PutString("vendor_nm", item.vendor_nm);
                    editor.Apply();
                    StartFragment(FragmentManager, typeof(KosuWorkFragment));
                }
            });
        }
    }
}