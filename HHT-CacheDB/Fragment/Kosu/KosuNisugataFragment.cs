﻿using Android.OS;
using Android.Views;
using Android.Widget;
using Com.Beardedhen.Androidbootstrap;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HHT
{
    public class KosuNisugataFragment : BaseFragment
    { 
        public override View OnCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
        {
            View view = inflater.Inflate(Resource.Layout.fragment_kosu_nisugata_kubun, container, false);

            Spinner spinner = view.FindViewById<Spinner>(Resource.Id.spinner);
            //spinner.ItemSelected += new EventHandler<AdapterView.ItemSelectedEventArgs>(spinner_ItemSelected);
            var adapter = ArrayAdapter.CreateFromResource(this.Context, Resource.Array.car_array, Android.Resource.Layout.SimpleSpinnerItem);
            adapter.SetDropDownViewResource(Android.Resource.Layout.SimpleSpinnerDropDownItem);
            spinner.Adapter = adapter;

            string barcode = Arguments.GetString("barcode");
            string matehancd = Arguments.GetString("matehancd");
            string kbn = Arguments.GetString("matekbn");

            BootstrapButton completeButton = view.FindViewById<BootstrapButton>(Resource.Id.completeButton);
            if (kbn == "1") completeButton.Text = "確定";
            completeButton.Click += delegate
            {
                int index = spinner.SelectedItemPosition;
                if(index == 0)
                {
                    ShowDialog("エラー", "荷姿を選んでください。");
                }
                else
                {
                    // キャリー紐づけの場合
                    if (kbn == "1")
                    {
                        List<string> mCarryBarcodes = prefs.GetStringSet("mCarryBarcodes", new List<string>()).ToList();
                        List<string> mCarryCaseKbns = prefs.GetStringSet("mCarryCaseKbns", new List<string>()).ToList();

                        mCarryBarcodes.Add(barcode);
                        mCarryCaseKbns.Add((index - 1).ToString());

                        editor.PutStringSet("mCarryBarcodes", mCarryBarcodes);
                        editor.PutStringSet("mCarryCaseKbns", mCarryCaseKbns);
                        editor.Apply();

                        FragmentManager.PopBackStack();
                        return;
                    }


                    //　サーバ処理
                    Task.Run(async() => 
                    {
                        var param = new Dictionary<string, string>
                        {
                            { "pSyukaDate", prefs.GetString("syuka_date","")},
                            { "pKamotsuNo", barcode},
                            { "tantoCd", prefs.GetString("sagyousya_cd","")},
                            { "pMatehan", matehancd},
                            { "handyId", Build.Serial},
                            { "caseKbn", (index - 1).ToString() }
                        };
                        
                        var kosu410 = await PostRestAPI("", "kosu/kosu410", param);

                        // index 1 : オリコン, 2: ケース, 3:不定形

                        // バーコードと荷姿の形式をグローバルに書く
                        Activity.RunOnUiThread(() => {
                            
                            if (kosu410 == null || kosu410.ContainsKey("errorMsg"))
                            {
                                string errorMsg = "";
                                if (kosu410 == null)
                                {
                                    errorMsg = "エラーが発生しました。";
                                }
                                else
                                {
                                    errorMsg = kosu410["errorMsg"];
                                }
                                
                                ShowDialog("エラー", errorMsg, () => { });
                                return;
                            }

                            string status = kosu410["status"].ToString();
                            if (status == "1")
                            {
                                ShowDialog("エラー", "すでに紐づけ作業済みです。", () => { });
                                return;
                            }

                            string kosu = kosu410["kosu"].ToString();
                            ShowDialog("報告", kosu410["tokNm"].ToString() + "\n" + kosu + "個口", () =>
                            {
                                FragmentManager.PopBackStack();
                            });
                        });
                    });
                }
            };
            return view;
        }

        private void spinner_ItemSelected(object sender, AdapterView.ItemSelectedEventArgs e)
        {
            Spinner spinner = (Spinner)sender;
            string toast = string.Format("Selected car is {0}", spinner.GetItemAtPosition(e.Position));
            Toast.MakeText(Context, toast, ToastLength.Long).Show();
        }


        public override bool OnKeyDown(Keycode keycode, KeyEvent paramKeyEvent)
        {
            return true;
        }

    }
}