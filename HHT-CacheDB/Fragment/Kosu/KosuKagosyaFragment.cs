﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Android.App;
using Android.OS;
using Android.Text;
using Android.Util;
using Android.Views;
using Android.Widget;
using Com.Beardedhen.Androidbootstrap;
using Com.Densowave.Bhtsdk.Barcode;
using HHT.Resources.Model;

namespace HHT
{
    public class KosuKagosyaFragment : BaseFragment
    {
        TextView mTenpoNameTextView;
        BootstrapEditText mSakiEditText;
        BootstrapEditText mMotoEditText;

        bool isFirstSekizai;
        string matehancd;
        bool mantanFlag;
        string tenpoNm;

        public override View OnCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
        {
            View view = inflater.Inflate(Resource.Layout.fragment_kosu_arata_kagosya, container, false);
            
            SetActionBarTitle("かご車紐づけ作業");

            mTenpoNameTextView = view.FindViewById<TextView>(Resource.Id.txt_tenpoName);
            mSakiEditText = view.FindViewById<BootstrapEditText>(Resource.Id.himotukesaki);
            mMotoEditText = view.FindViewById<BootstrapEditText>(Resource.Id.himotukemoto);
            mantanFlag = false;
            BootstrapButton mMantanButton = view.FindViewById<BootstrapButton>(Resource.Id.btn_todoke_mantan);

            isFirstSekizai = false;

            mMantanButton.Click += delegate
            {
                if (mSakiEditText.Text != "")
                {
                    ShowDialog("警告", "満タン完了します。\nよろしいですか？", (flag) => {

                        if(flag == false)
                        {
                            return;
                        }

                        // 正しい配送先なのか？サーバーで確認する
                        Task.Run(async () =>
                        {
                            var param = new Dictionary<string, string>
                            {
                                { "pSyukaDate", prefs.GetString("syuka_date","")},
                                { "pMatehan", matehancd},
                                { "tantoCd", prefs.GetString("sagyousya_cd","")},
                            };

                            return await PostRestAPI("", "kosu/kosu420", param);
                        }).ContinueWith(t => {

                            Activity.RunOnUiThread(() => {
                                
                                if (t.Result.ContainsKey("errorMsg"))
                                {
                                    string errorMsg = t.Result["errorMsg"];
                                    ShowDialog("エラー", errorMsg, () => { });
                                    return;
                                }

                                Vibrate();
                                ShowDialog("報告", "完了しました。", () => {
                                    FragmentManager.PopBackStack();
                                });

                            });
                        });
                    });

                    return;
                }

                mSakiEditText.Visibility = ViewStates.Gone;
                view.FindViewById<TextView>(Resource.Id.himotukemotolbl).Visibility = ViewStates.Gone;
                view.FindViewById<TextView>(Resource.Id.sakiTextView).Visibility = ViewStates.Gone;
                
                view.FindViewById<TextView>(Resource.Id.messagelbl).Text = "満タンする貨物をスキャンしてください。";

                mMantanButton.Visibility = ViewStates.Gone;

                mantanFlag = true;

            };

            // 当画面だけの仕様でCODE128は読めないようにする。
            ((MainActivity)Activity).ResizeBarcodeLength(20);
            
            mMotoEditText.FocusChange += (s, e) =>
            {
                if (e.HasFocus)
                {
                    if (mSakiEditText.Text == "" && isFirstSekizai == false)
                    {
                        mSakiEditText.RequestFocus();
                    }
                }
            };

            mSakiEditText.KeyPress += (object sender, View.KeyEventArgs e) =>
            {
                e.Handled = false;
                if (e.Event.Action == KeyEventActions.Down && e.KeyCode == Keycode.Enter)
                {
                    //add your logic here  
                    e.Handled = true;

                    if(mSakiEditText.Text == "")
                    {
                        ShowDialog("警告", "\n一個目の組付けですか？\n", (okFlag) => {
                            if (okFlag)
                            {
                                isFirstSekizai = true;
                                matehancd = "";

                                mSakiEditText.Enabled = false;
                                mMotoEditText.RequestFocus();
                            }
                            else
                            {
                                mSakiEditText.Text = "";
                                mSakiEditText.RequestFocus();
                            }

                        });
                    }
                    else
                    {
                        mMotoEditText.RequestFocus();
                    }
                }
            };

            mMotoEditText.KeyPress += (object sender, View.KeyEventArgs e) =>
            {
                e.Handled = false;
                if (e.Event.Action == KeyEventActions.Down && e.KeyCode == Keycode.Enter)
                {
                    //add your logic here  
                    e.Handled = true;
                    return;
                }
                else
                {
                    CommonUtils.HideKeyboard(Activity);
                }
            };

            mMotoEditText.Enabled = false;
            mSakiEditText.RequestFocus();

            return view;
        }

        public override void OnBarcodeDataReceived(BarcodeDataReceivedEvent_ dataReceivedEvent)
        {
            IList<BarcodeDataReceivedEvent_.BarcodeData_> listBarcodeData = dataReceivedEvent.BarcodeData;
            
            foreach (BarcodeDataReceivedEvent_.BarcodeData_ barcodeData in listBarcodeData)
            {
                if(mantanFlag == true)
                {
                    ShowDialog("警告", "満タン完了します。\nよろしいですか？", (isOk) => {

                        if (isOk == false)
                        {
                            return;
                        }

                        // 正しい配送先なのか？サーバーで確認する
                        Task.Run(async () =>
                        {
                            var param = new Dictionary<string, string>
                            {
                                { "pSyukaDate", prefs.GetString("syuka_date","")},
                                { "pMatehan", matehancd},
                                { "tantoCd", prefs.GetString("sagyousya_cd","")},
                            };

                            return await PostRestAPI("", "kosu/kosu420", param);
                        }).ContinueWith(t => {

                            Activity.RunOnUiThread(() => {

                                if (t.Result.ContainsKey("errorMsg"))
                                {
                                    string errorMsg = t.Result["errorMsg"];
                                    ShowDialog("エラー", errorMsg, () => { });
                                    return;
                                }

                                Vibrate();
                                ShowDialog("報告", "完了しました。", () => {
                                    FragmentManager.PopBackStack();
                                });

                            });
                        });
                    });

                    return;
                }

                // スキャン無効化
                ((MainActivity)this.Activity).DisableScanning();

                if (mSakiEditText.Text == "" && isFirstSekizai == false)
                {
                    // 正しい配送先なのか？サーバーで確認する
                    Task.Run(async () =>
                    {
                        var param = new Dictionary<string, string>
                        {
                            { "pSyukaDate", prefs.GetString("syuka_date","")},
                            { "pKamotsuNo", barcodeData.Data}
                        };

                        return await PostRestAPI("", "kosu/kosu400", param);
                    }).ContinueWith(t => {

                        Activity.RunOnUiThread(() => {
                            ((MainActivity)this.Activity).EnableScanning();
                            
                            if (t.Result.ContainsKey("errorMsg"))
                            {
                                string errorMsg = t.Result["errorMsg"];
                                ShowDialog("エラー", errorMsg, () => { });
                                return;
                            }

                            mSakiEditText.SetFilters(new IInputFilter[] { new InputFilterLengthFilter(barcodeData.Data.Length) });
                            mSakiEditText.Text = barcodeData.Data;
                            mSakiEditText.Enabled = false;
                            
                            tenpoNm = t.Result["tokNm"].ToString();
                            matehancd = t.Result["matehanCd"].ToString();
                            mTenpoNameTextView.Text = tenpoNm;

                            mMotoEditText.Enabled = true;
                            mMotoEditText.RequestFocus();
                        });
                        
                    });
                    
                }
                else if (mMotoEditText.Text == "")
                {
                    // マテハンで紐づく
                    var param = new Dictionary<string, string>
                    {
                        { "pSyukaDate", prefs.GetString("syuka_date","")},
                        { "pKamotsuNo", barcodeData.Data},
                        { "tantoCd", prefs.GetString("sagyousya_cd","")},
                        { "pMatehan", matehancd},
                        { "handyId", Build.Serial}
                    };
                    
                    Activity.RunOnUiThread(() => ShowProgress(""));

                    Task.Run(async () =>
                    {
                        // CHK グローバル確認してから
                        var chkResult = await PostRestAPI("", "kosu/kosu440", param);

                        Activity.RunOnUiThread(() => DismissProgress());
                        ((MainActivity)this.Activity).EnableScanning();

                        if (chkResult.ContainsKey("errorMsg"))
                        {
                            ShowDialog("エラー", chkResult["errorMsg"].ToString());
                            return;
                        }

                        
                        if (chkResult["existFlag"].ToString() == "0")
                        {
                            // 個口データが存在しない場合
                            Bundle bundle = new Bundle();
                            bundle.PutString("barcode", barcodeData.Data);
                            bundle.PutString("matehancd", matehancd);
                            bundle.PutString("matekbn", "0");
                            
                            StartFragment(FragmentManager, typeof(KosuNisugataFragment), bundle);
                            
                            return;
                        }
                        else
                        {
                            // 個口データが存在する場合

                            param.Add("caseKbn", chkResult["caseKbn"].ToString());
                           
                            var kosu410 = await PostRestAPI("", "kosu/kosu410", param);

                            Activity.RunOnUiThread(() => {

                                //((MainActivity)this.Activity).EnableScanning();

                                if (kosu410 == null || kosu410.ContainsKey("errorMsg"))
                                {
                                    string errorMsg = kosu410["errorMsg"];
                                    ShowDialog("エラー", errorMsg, () => { });
                                    return;
                                }

                                string status = kosu410["status"].ToString();
                                if (status == "1")
                                {
                                    ShowDialog("エラー", "すでに紐づけ作業済みです。", () => { });
                                    return;
                                }

                                string kosu = kosu410["kosu"].ToString();

                                mMotoEditText.SetFilters(new IInputFilter[] { new InputFilterLengthFilter(barcodeData.Data.Length) });
                                mMotoEditText.Text = barcodeData.Data;

                                ShowDialog("報告", kosu410["tokNm"].ToString() + "\n" + kosu + "個口", () =>
                                {
                                    mSakiEditText.SetFilters(new IInputFilter[] { new InputFilterLengthFilter(0) });
                                    mMotoEditText.SetFilters(new IInputFilter[] { new InputFilterLengthFilter(0) });

                                    isFirstSekizai = false;

                                    mSakiEditText.Enabled = true;
                                    mMotoEditText.Enabled = false;

                                    mSakiEditText.Text = "";
                                    mMotoEditText.Text = "";
                                    mTenpoNameTextView.Text = "";
                                    matehancd = "";

                                    mSakiEditText.RequestFocus();
                                });
                            });


                        }

                        //var result = await WebService.PostRestAPI(url, param);

                        //await Task.Delay(1500);
                        //Activity.RunOnUiThread(() => DismissProgress());
                    }).ContinueWith(t =>
                    {
                        //t.Result

                    });

                    /*
                    Task.Run(async () =>
                    {
                        var param = new Dictionary<string, string>
                        {
                            { "pSyukaDate", prefs.GetString("syuka_date","")},
                            { "pKamotsuNo", barcodeData.Data},
                            { "tantoCd", prefs.GetString("sagyousya_cd","")},
                            { "pMatehan", matehancd},
                            { "handyId", Build.Serial}
                        };

                        return await PostRestAPI("", "kosu/kosu410", param);
                    }).ContinueWith(t => {

                        
                    });
                    */
                }
            }
        }


        public async Task<bool> CountItem(string kamotsu_no)
        {
            var param = GetProcedureParam(kamotsu_no);

            try
            {
                #region WebService呼び出し
                KOSU070 kosuKenpin = await PostRestAPI<KOSU070>("", "kosu/kosu070", param);
                #endregion
            }
            catch
            {
                ShowDialog("エラー", "更新出来ませんでした。\n管理者に連絡してください。", () => { });
                Log.Error(Tag, "");
                return false;
            }

            return true;
        }


        // プロシージャパラメータ設定
        private Dictionary<string, string> GetProcedureParam(string kamotsu_no)
        {
            string pSagyosyaCD = prefs.GetString("driver_cd", "");
            string pSoukoCD = prefs.GetString("souko_cd", "");
            string pSyukaDate = prefs.GetString("syuka_date", "");
            string pTokuisakiCD = prefs.GetString("tokuisaki_cd", "");
            string pTodokesakiCD = prefs.GetString("todokesaki_cd", "");
            string pVendorCD = prefs.GetString("vendor_cd", "");
            string pTsumiVendorCD = "";
            string pKamotsuNo = kamotsu_no;
            string pBinNo = prefs.GetString("bin_no", "0");

            string pMatehan = "";

            Dictionary<string, string> param = new Dictionary<string, string>
            {
                { "pTerminalID",  prefs.GetString("terminal_id","")},
                { "pProgramID",  "KOS"},
                { "pSagyosyaCD",  pSagyosyaCD},
                { "pSoukoCD",  pSoukoCD},
                { "pSyukaDate",  pSyukaDate},
                { "pTokuisakiCD" ,  pTokuisakiCD},
                { "pTodokesakiCD" ,  pTodokesakiCD},
                { "pVendorCD",  pVendorCD},
                { "pTsumiVendorCD",  pTsumiVendorCD},
                { "pKamotsuNo",  pKamotsuNo},
                { "pBinNo",  pBinNo},
                { "pHHT_No",  prefs.GetString("hht_no","")},
                { "pMatehan",  pMatehan}
            };

            return param;
        }

    }
}