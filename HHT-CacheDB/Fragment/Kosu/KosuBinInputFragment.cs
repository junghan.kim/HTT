﻿using Android.OS;
using Android.Views;
using Com.Beardedhen.Androidbootstrap;
using Android.Widget;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace HHT
{
    public class KosuBinInputFragment : BaseFragment
    {
        TextView mTokuiTextView;
        TextView mTodokeTextView;
        BootstrapEditText mDeliveryDateEditText;
        BootstrapEditText mBinNoEditText;
        BootstrapEditText mTokuiEditText;
        BootstrapEditText mTodokeEditText;
        BootstrapButton mConfirmButton;

        private bool searchFlag;
        private string deliveryDate;
        private string tokuisaki;
        private string todokesaki;
        
        public override View OnCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
        {
            View view = inflater.Inflate(Resource.Layout.fragment_kosu_bin_input, container, false);

            SetActionBarTitle("届先指定検品");

            mTokuiTextView = view.FindViewById<TextView>(Resource.Id.tokuiText);
            mTokuiEditText = view.FindViewById<BootstrapEditText>(Resource.Id.tokuiCode);
            mTodokeTextView = view.FindViewById<TextView>(Resource.Id.todokeText);
            mTodokeEditText = view.FindViewById<BootstrapEditText>(Resource.Id.todokeCode);
            mDeliveryDateEditText = view.FindViewById<BootstrapEditText>(Resource.Id.syukaDate);
            mBinNoEditText = view.FindViewById<BootstrapEditText>(Resource.Id.binNo);
            mBinNoEditText.KeyPress += (sender, e) => {
                if (e.Event.Action == KeyEventActions.Down && e.KeyCode == Keycode.Enter)
                {
                    e.Handled = true;
                    ConfirmBinInfo();
                }
                else
                {
                    e.Handled = false;
                }
            };
            
            mConfirmButton = view.FindViewById<BootstrapButton>(Resource.Id.btn_binInput_confirm);
            mConfirmButton.Click += delegate { ConfirmBinInfo(); };

            deliveryDate = prefs.GetString("deliveryDate", "");
            tokuisaki = prefs.GetString("tokuisaki_cd", "");
            todokesaki = prefs.GetString("todokesaki_cd", "");
            searchFlag = Arguments.GetBoolean("searchFlag");

            mDeliveryDateEditText.Text = deliveryDate;

            if (searchFlag == false)
            {
                mTokuiEditText.Text = tokuisaki;
                mTodokeEditText.Text = todokesaki;
            }
            else
            {
                mTokuiTextView.Visibility = ViewStates.Gone;
                mTokuiEditText.Visibility = ViewStates.Gone;
                mTodokeTextView.Visibility = ViewStates.Gone;
                mTodokeEditText.Visibility = ViewStates.Gone;
            }

            mBinNoEditText.RequestFocus();

            return view;
        }

        private void ConfirmBinInfo()
        {
            if (mBinNoEditText.Text == "")
            {
                ShowDialog("エラー", "便番号が入力されていません。", () => { });
                return;
            }

            if (searchFlag.Equals(true)) // 届先検索画面へ遷移する
            {
                editor.PutString("syuka_date", mDeliveryDateEditText.Text.Replace("/", ""));
                editor.PutString("bin_no", mBinNoEditText.Text);
                editor.Apply();

                StartFragment(FragmentManager, typeof(KosuSearchFragment));
            }
            else
            {   // 届先検索画面へ遷移しない場合
                GetTokuisakiMasterInfo();
            }
        }

        private void GetTokuisakiMasterInfo()
        {
            string souko_cd = prefs.GetString("souko_cd", "");
            string kitaku_cd = prefs.GetString("kitaku_cd", "");
            string syuka_date = mDeliveryDateEditText.Text.Replace("/", "");
            string bin_no = mBinNoEditText.Text;
            string tokuisaki_cd = prefs.GetString("tokuisaki_cd", "");
            string todokesaki_cd = prefs.GetString("todokesaki_cd", "");


            Task.Run(async() => {

                var param = new Dictionary<string, string>()
                {
                    {"syuka_date", syuka_date},
                    {"tokuisaki_cd", tokuisaki_cd},
                    {"todokesaki_cd", todokesaki_cd},
                    {"bin_no", bin_no},
                };

                var result = await PostRestAPI("検品情報を確認しています。", "kosu/kosu040", param);
                int status = int.Parse(result["cnt"]);
                if (status == 99)
                {
                    ShowDialog("エラー", "検品可能なデータがありません。", () => { });
                }
                else if (status >= 1)
                {
                    ShowDialog("報告", "全ての検品が完了しています。", () => { });
                }
                else
                {
                    editor.PutString("tokuisaki_nm", result["tokuisaki_rk"].ToString());
                    editor.PutString("syuka_date", syuka_date);
                    editor.PutString("bin_no", bin_no);
                    editor.Apply();

                    StartFragment(FragmentManager, typeof(KosuWorkFragment));
                }
            });
        }
    }
}
 