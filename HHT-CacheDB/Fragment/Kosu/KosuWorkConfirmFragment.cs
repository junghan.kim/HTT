﻿using Android.App;
using Android.OS;
using Android.Views;
using Android.Widget;
using HHT.Resources.Model;
using Com.Beardedhen.Androidbootstrap;
using System.Threading.Tasks;
using System.Collections.Generic;

namespace HHT
{
    public class KosuWorkConfirmFragment : BaseFragment
    {
        public override View OnCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
        {
            View view = inflater.Inflate(Resource.Layout.fragment_kosu_tyingConfirm, container, false);
           
            view.FindViewById<BootstrapButton>(Resource.Id.stopButton).Click += delegate {
                FragmentManager.PopBackStack(FragmentManager.GetBackStackEntryAt(2).Id, 0);
            };

            var param = new Dictionary<string, string>()
            {
                { "syuka_date", prefs.GetString("syuka_date", "") },
                { "tokuisaki_cd",prefs.GetString("tokuisaki_cd", "") },
                { "todokesaki_cd",prefs.GetString("todokesaki_cd", "") },
                { "bin_no", prefs.GetString("bin_no", "")},
                { "vendor_cd",prefs.GetString("vendor_cd", "") },

            };

            Task.Run(async () =>
            {
                try
                {
                    return await PostRestAPI<KOSU110>("", "kosu/kosu110", param);
                }
                catch
                {
                    Activity.RunOnUiThread(() =>
                    {
                        ShowDialog("報告", "表示データがありません。", () => {
                            FragmentManager.PopBackStack(FragmentManager.GetBackStackEntryAt(3).Id, 0);
                        });
                    });

                    return null;
                }

            }).ContinueWith((t) => {

                if (t == null) return;

                KOSU110 nyukaStatus = t.Result;
                Activity.RunOnUiThread(() =>
                {
                    view.FindViewById<TextView>(Resource.Id.txt_tyConfirm_case).Text = nyukaStatus.sum_case_sumi + "/" + nyukaStatus.sum_case;
                    view.FindViewById<TextView>(Resource.Id.txt_tyConfirm_oricon).Text = nyukaStatus.sum_oricon_sumi + "/" + nyukaStatus.sum_oricon;
                    view.FindViewById<TextView>(Resource.Id.txt_tyConfirm_huteikei).Text = nyukaStatus.sum_futeikei_sumi + "/" + nyukaStatus.sum_futeikei;
                    view.FindViewById<TextView>(Resource.Id.txt_tyConfirm_hazai).Text = nyukaStatus.sum_hazai_sumi + "/" + nyukaStatus.sum_hazai;
                    view.FindViewById<TextView>(Resource.Id.txt_tyConfirm_miseidou).Text = nyukaStatus.sum_ido_sumi + "/" + nyukaStatus.sum_ido;
                    view.FindViewById<TextView>(Resource.Id.txt_tyConfirm_henpin).Text = nyukaStatus.sum_henpin_sumi + "/" + nyukaStatus.sum_henpin;
                    view.FindViewById<TextView>(Resource.Id.txt_tyConfirm_hansoku).Text = nyukaStatus.sum_hansoku_sumi + "/" + nyukaStatus.sum_hansoku;
                    view.FindViewById<TextView>(Resource.Id.txt_tyConfirm_kaisyu).Text = "0" + "/" + "0";
                    view.FindViewById<TextView>(Resource.Id.txt_tyConfirm_total).Text = nyukaStatus.total_sumi + "/" + nyukaStatus.total;
                    view.FindViewById<TextView>(Resource.Id.txt_tyConfirm_daisu).Text = nyukaStatus.sum_mate_cnt.ToString();
                });

            });

            return view;
        }
    }
}
 