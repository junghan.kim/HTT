﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Android.App;
using Android.OS;
using Android.Views;
using Android.Widget;
using HHT.Resources.Model;

namespace HHT
{
    public class KosuVendorAllSearchFragment : BaseFragment
    {
        private View view;
        private VendorAllAdapter vendorAdapter;
        private int kosuMenuflag;

        private List<KOSU190> vendorList;

        public override View OnCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
        {
            view = inflater.Inflate(Resource.Layout.fragment_search_vendor_all, container, false);

            kosuMenuflag = prefs.GetInt("KOSU_MENU_FLAG", (int)KOSUMENU.TODOKE); // 画面区分

            SetTodokesakiAsync();

            return view;
        }

        private void SetTodokesakiAsync()
        {
            string soukoCd = prefs.GetString("souko_cd", "");
            string kitakuCd = prefs.GetString("kitaku_cd", "");
            string syuka_date = prefs.GetString("syuka_date", "");

            try
            {
                ((MainActivity)this.Activity).ShowProgress("");

                Task.Run(async () =>
                {
                    var param = new Dictionary<string, string>();
                    vendorList = await WebService.PostRestAPI<List<KOSU190>>("kosu/kosu190", param);

                    if (vendorList.Count > 0)
                    {
                        ListView listView = view.FindViewById<ListView>(Resource.Id.listView1);
                        listView.ItemClick += listView_ItemClick;

                        this.Activity.RunOnUiThread(() => {
                            vendorAdapter = new VendorAllAdapter(vendorList);
                            listView.Adapter = vendorAdapter;
                        });
                    }
                    else
                    {
                        ShowDialog("エラー", "表示データがありません。", () =>
                        {
                            FragmentManager.PopBackStack();
                        });
                    }

                }).ContinueWith((x) => {
                    Activity.RunOnUiThread(() =>((MainActivity)this.Activity).DismissDialog());
                });
            }
            catch
            {
                ShowDialog("エラー", "表示データがありません。", () =>
                {
                    FragmentManager.PopBackStack();
                });
            }
        }
        
        void listView_ItemClick(object sender, AdapterView.ItemClickEventArgs e)
        {
            var item = vendorList[e.Position];

            editor.PutBoolean("searchFlag", true);
            editor.PutString("vendor_cd", item.vendor_cd);
            editor.PutString("vendor_nm", item.vendor_nm);
            editor.Apply();
            FragmentManager.PopBackStack();

        }
    }
}