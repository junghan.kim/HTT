﻿using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Android.App;
using Android.Content;
using Android.OS;
using Android.Preferences;
using Android.Views;
using Android.Widget;
using HHT.Resources.Model;

namespace HHT
{
    public class KosuSearchFragment : BaseFragment
    {
        private View view;
        private KosuTenpoListAdapter todokesakiAdapter;
        
        ListView listView;
        private int kosuMenuflag;

        public override View OnCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
        {
            view = inflater.Inflate(Resource.Layout.fragment_kosu_search, container, false);

            kosuMenuflag = prefs.GetInt("KOSU_MENU_FLAG", (int)KOSUMENU.TODOKE); // 画面区分

            SetActionBarTitle("届先指定検品");

            SetTodokesakiAsync();

            return view;
        }

        private void SetTodokesakiAsync()
        {
            //((MainActivity)this.Activity).ShowProgress("届先検索中。。。");

            var syncContext = SynchronizationContext.Current;

            Task.Run(() =>
            {
                try
                {
                    List<Haiso> todokeList = GetTokuisakiMasterInfo();

                    if (todokeList.Count > 0)
                    {
                        Activity.RunOnUiThread(() =>
                        {
                            listView = view.FindViewById<ListView>(Resource.Id.listView1);
                            listView.ItemClick += listView_ItemClick;

                            todokesakiAdapter = new KosuTenpoListAdapter(todokeList);
                            listView.Adapter = todokesakiAdapter;
                            listView.RequestFocus();
                            listView.SetSelection(0);
                        });
                    }
                    else
                    {
                        ShowDialog("エラー", "表示データがありません。", () =>
                        {
                            FragmentManager.PopBackStack();
                        });
                    }
                }
                catch
                {
                    ShowDialog("エラー", "例外エラーが発生しました。", () =>
                    {
                        FragmentManager.PopBackStack();
                    });
                }

            }).ContinueWith(t => syncContext.Post(state =>
            {
                // UIスレッドでの処理
                //((MainActivity)Activity).DismissDialog();
            }, null));

        }

        private List<Haiso> GetTokuisakiMasterInfo()
        {
            List<Haiso> resultList = new List<Haiso>(); ;
            string soukoCd = prefs.GetString("souko_cd", "");
            string kitakuCd = prefs.GetString("kitaku_cd", "");
            string syuka_date = prefs.GetString("syuka_date", "");
            string bin_no = prefs.GetString("bin_no", "");

            Task.Run(async() => {
                try
                {
                    if (kosuMenuflag == (int)KOSUMENU.TODOKE)
                    {
                        var param = new Dictionary<string, string>()
                        {
                            { "hiduke", syuka_date},
                            { "bin", bin_no}
                        };

                        resultList = await PostRestAPI<List<Haiso>>("", "kosu/kosu060", param);
                    }
                    else if (kosuMenuflag == (int)KOSUMENU.VENDOR)
                    {
                        //resultList = WebService.RequestKosu065(soukoCd, kitakuCd, syuka_date, bin_no);
                    }
                }
                catch
                {
                    ShowDialog("エラー", "例外エラーが発生しました。", () => { });
                }
            }).Wait();
            
            
            return resultList;
        }

        void listView_ItemClick(object sender, AdapterView.ItemClickEventArgs e)
        {
            var item = todokesakiAdapter[e.Position];
            
            editor.PutString("todokesaki_cd", item.todokesaki_cd);
            editor.PutString("tokuisaki_cd", item.tokuisaki_cd);
            editor.PutString("tokuisaki_nm", item.tokuisaki_rk);
            editor.PutString("tsumi_vendor_nm", "");
            editor.PutString("vendor_nm", "");
            editor.PutString("start_vendor_cd", "");
            
            editor.Apply();

            StartFragment(FragmentManager, typeof(KosuWorkFragment));
            
        }


        public override bool OnKeyDown(Keycode keycode, KeyEvent paramKeyEvent)
        {
            if (keycode == Keycode.F1)
            {
                var item = todokesakiAdapter[listView.SelectedItemPosition];
                editor.PutString("tokuisaki_cd", item.tokuisaki_cd);
                editor.PutString("todokesaki_cd", item.todokesaki_cd);
                editor.Apply();

                StartFragment(FragmentManager, typeof(KosuMikenFragment));
            }

            return true;
        }

    }
}