﻿using Android.OS;
using Android.Views;
using Android.Widget;
using System;
using System.Collections.Generic;
using HHT.Resources.Model;
using static Android.Widget.AdapterView;
using Com.Beardedhen.Androidbootstrap;
using System.Threading.Tasks;

namespace HHT
{
    public class KosuMantanFragment : BaseFragment
    {
        BootstrapEditText etMantanVendor;
        BootstrapButton vendorSearchButton;

        private TextView txtVenderName;
        private ListView listView;
        private KOSU200 kosu200;
        private int kosuMenuflag;

        public override View OnCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
        {
            View view = inflater.Inflate(Resource.Layout.fragment_kosu_mantan, container, false);
            listView = view.FindViewById<ListView>(Resource.Id.lv_matehanList);

            listView.ItemClick += (object sender, ItemClickEventArgs e) =>
            {
                SelectListViewItem(e.Position);
            };

            return view;
        }

        public override void OnResume()
        {
            base.OnResume();
            
            kosuMenuflag = prefs.GetInt("KOSU_MENU_FLAG", (int)KOSUMENU.TODOKE); // 画面区分
            etMantanVendor.Text = prefs.GetString("vendor_cd", "");
            txtVenderName.Text = prefs.GetString("vendor_nm", "");
            etMantanVendor.Text = prefs.GetString("vendor_cd", "");

            SetMatehanList();
        }

        private void SetMatehanList()
        {
            try
            {
                List<string> temp = new List<string>();

                // ベンダー別マテハンコード取得
                Task.Run(async() => {
                    
                    var param = new Dictionary<string, string>()
                    {
                        { "vendorCd" , etMantanVendor.Text }
                    };

                    //kosu200 = await WebService.PostRestAPI<KOSU200>("kosu/kosu200", param);
                    /*
                    KOSU200 kosu200 = new KOSU200();
                    List<Matehan> matehanList = new List<Matehan>();
                    Matehan matehan = new Matehan();
                    matehan.MatehanCd = "";
                    matehan.MatehanNm = "";
                    matehanList.Add()
                    */
                    //kosu200.MatehanList
                    
                    temp.Add("1.  かご車");
                    temp.Add("2.  キャリー");
                    
                }).ContinueWith((result)=> {
                    Activity.RunOnUiThread(() => {
                        ArrayAdapter<String> adapter = new ArrayAdapter<string>(Activity, Resource.Layout.adapter_list_matehan, temp);
                        listView.Adapter = adapter;
                        //txtVenderName.Text = venderName;
                    });
                });
                
            }
            catch
            {
                ShowDialog("エラー", "ベンダーコードがみつかりません。", () => { });
                return;
            }
        }

        public override bool OnKeyDown(Keycode keycode, KeyEvent paramKeyEvent)
        {
            if (keycode == Keycode.Num1)
            {
                SelectListViewItem(0);
            }
            else if (keycode == Keycode.Num2)
            {
                SelectListViewItem(1);
            }
            else if (keycode == Keycode.Num3)
            {
                SelectListViewItem(2);
            }
            else if (keycode == Keycode.Num4)
            {
                SelectListViewItem(3);
            }

            return true;
        }

        public void SelectListViewItem(int index)
        {
            if (kosu200.MatehanList.Count <= index)
            {
                return;
            }

            string msg = kosu200.MatehanList[index].MatehanNm + "でよろしいですか？";

            ShowDialog("確認", msg, async (isOkPushed) => {
                if (isOkPushed)
                {
                    try
                    {

                        KOSU070 result = new KOSU070();
                        var param = GetProcedureParam(kosu200.MatehanList[index].MatehanCd);
                        
                        if (kosuMenuflag == (int)KOSUMENU.TODOKE)
                        {
                            result = await WebService.PostRestAPI<KOSU070>("kosu/kosu080", param);
                        }
                        else if (kosuMenuflag == (int)KOSUMENU.VENDOR)
                        {
                            result = await WebService.PostRestAPI<KOSU070>("kosu/kosu160", param);
                        }

                        string dai_su = prefs.GetString("dai_su", "0");
                        int dai_su_intValue = int.TryParse(dai_su, out dai_su_intValue) ? dai_su_intValue : 0;

                        if (result.poRet == "0")
                        {
                            dai_su_intValue++; //台数加算

                            editor.PutString("dai_su", dai_su_intValue.ToString());

                            editor.Apply();

                            if (int.Parse(result.poMsg) == 0)
                            {
                                StartFragment(FragmentManager, typeof(KosuCompleteFragment));
                            }
                            else if (int.Parse(result.poMsg) > 0)
                            {
                                // 正常に処理されたが、残り作業がある場合、
                                // 紐づけ画面に遷移する。

                                editor.PutString("case_su", "0");
                                editor.PutString("oricon_su", "0");
                                editor.PutString("futeikei_su", "0");
                                editor.PutString("ido_su", "0");
                                editor.PutString("hazai_su", "0");
                                editor.PutString("henpin_su", "0");
                                editor.PutString("hansoku_su", "0");
                                editor.PutString("kaisyu_su", "0");

                                editor.PutString("tmp_kosu", prefs.GetString("ko_su", "0"));
                                editor.Apply();

                                Activity.FragmentManager.PopBackStack();
                            }
                        }
                        else
                        {
                            ShowDialog("エラー", "更新出来ませんでした。\n管理者に連絡してください。", () => { });
                            return;
                        }
                    }
                    catch
                    {
                        ShowDialog("エラー", "更新出来ませんでした。\n管理者に連絡してください。", () => { });
                        return;
                    }
                }
            });
        }
        
        private Dictionary<string, string> GetProcedureParam(string pMatehan)
        {
            string pSagyosyaCD = prefs.GetString("sagyousya_cd", "");
            string pSoukoCD = prefs.GetString("souko_cd", "");
            string pSyukaDate = prefs.GetString("syuka_date", "");
            string pTokuisakiCD = prefs.GetString("tokuisaki_cd", "0000");
            string pTodokesakiCD = prefs.GetString("todokesaki_cd", "");
            string pVendorCD = prefs.GetString("vendor_cd", "");
            string pTsumiVendorCD = etMantanVendor.Text;
            string pKamotsuNo = "";
            string pBinNo = prefs.GetString("bin_no", "0");
            
            string pJskCaseSu = prefs.GetString("case_su", "0");
            string pJskOriconSu = prefs.GetString("oricon_su", "0");
            string pJskFuteikeiSu = prefs.GetString("futeikei_su", "0");
            string pJskHazaiSu = prefs.GetString("hazai_su", "0");
            string pJskIdoSu = prefs.GetString("ido_su", "0");
            string pJskHenpinSu = prefs.GetString("henpin_su", "0");
            string pJskHansokuSu = prefs.GetString("hansoku_su", "0");

            Dictionary<string, string> param = new Dictionary<string, string>
            {
                { "pTerminalID",  prefs.GetString("terminal_id","")},
                { "pProgramID",  "KOS"},
                { "pSagyosyaCD",  pSagyosyaCD},
                { "pSoukoCD",  pSoukoCD},
                { "pSyukaDate",  pSyukaDate},
                { "pTokuisakiCD" ,  pTokuisakiCD},
                { "pTodokesakiCD" ,  pTodokesakiCD},
                { "pVendorCD",  pVendorCD},
                { "pTsumiVendorCD",  pTsumiVendorCD},
                { "pKamotsuNo",  pKamotsuNo},
                { "pBinNo",  pBinNo},
                { "pHHT_No",  prefs.GetString("hht_no","")},
                { "pMatehan",  pMatehan},
                { "pJskCaseSu",  pJskCaseSu},
                { "pJskOriconSu",  pJskOriconSu},
                { "pJskFuteikeiSu",  pJskFuteikeiSu},
                { "pJskTcSu",  "0"},
                { "pJskMailbinSu",  "0"},
                { "pJskHazaiSu",  pJskHazaiSu},
                { "pJskIdoSu",  pJskIdoSu},
                { "pJskHenpinSu",  pJskHenpinSu},
                { "pJskHansokuSu",  pJskHansokuSu}
            };

            return param;
        }

    }
}
 