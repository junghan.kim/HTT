﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Android.App;
using Android.OS;
using Android.Views;
using Android.Widget;
using Com.Beardedhen.Androidbootstrap;
using Com.Densowave.Bhtsdk.Barcode;

namespace HHT
{
    public class KosuCarryFragment : BaseFragment
    {
        private View view;
        TextView mOriconListTextView;
        BootstrapButton mConfirmButton;

        List<string> mCarryBarcodes;
        List<string> mCarryCaseKbns;

        public override View OnCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
        {
            view = inflater.Inflate(Resource.Layout.fragment_kosu_arata_carry, container, false);

            SetActionBarTitle("キャリー紐づけ作業");

            mCarryBarcodes = prefs.GetStringSet("mCarryBarcodes", new List<string>()).ToList();
            mCarryCaseKbns = prefs.GetStringSet("mCarryCaseKbns", new List<string>()).ToList();

            // バーコード桁数を20桁で設定
            ((MainActivity)Activity).ResizeBarcodeLength(20);

            mOriconListTextView = view.FindViewById<TextView>(Resource.Id.txt_oriconList);
            mConfirmButton = view.FindViewById<BootstrapButton>(Resource.Id.btn_confirm);
            mConfirmButton.Click += delegate
            {
                string barcode = mOriconListTextView.Text;
                if (barcode.Length == 0)
                {
                    ShowDialog("エラー", "貨物番号をスキャンしてください。", () => { });
                    return;
                }

                mOriconListTextView.Text = "";

                Task.Run(async () => {

                    var param = new Dictionary<string, string>
                    {
                        { "pSyukaDate", prefs.GetString("syuka_date","")},
                        { "pKamotsuNoList", string.Join(",",mCarryBarcodes)},
                        { "pCaseKbnList", string.Join(",",mCarryCaseKbns)}
                    };

                    return await PostRestAPI("", "kosu/kosu430", param);
                }).ContinueWith((t) => {

                    ((MainActivity)this.Activity).EnableScanning();

                    Activity.RunOnUiThread(() => {

                        dynamic result = t.Result;

                        if (t.Result.ContainsKey("errorMsg"))
                        {
                            string errorMsg = t.Result["errorMsg"];
                            ShowDialog("エラー", errorMsg, () => { });
                            return;
                        }

                        PlayBeepOk();
                        Vibrate(300);

                        List<string> mCarryBarcodes = new List<string>();
                        List<string> mCarryCaseKbns = new List<string>();

                        editor.Remove("mCarryBarcodes");
                        editor.Remove("mCarryCaseKbns");
                        editor.Apply();

                        mOriconListTextView.Text = "";

                        ShowDialog("報告", "完了しました。", () => {});

                    });
                });


            };


            mOriconListTextView.Text = string.Join("\n", mCarryBarcodes);

            return view;
        }


        public override void OnBarcodeDataReceived(BarcodeDataReceivedEvent_ dataReceivedEvent)
        {
            IList<BarcodeDataReceivedEvent_.BarcodeData_> listBarcodeData = dataReceivedEvent.BarcodeData;

            foreach (BarcodeDataReceivedEvent_.BarcodeData_ barcodeData in listBarcodeData)
            {
                string barcode = mOriconListTextView.Text;
                string barcodeArray = barcode.Replace("\n", ",");

                if(barcodeArray != "")
                {
                    string haisoCd = barcodeArray.Substring(7, 4);

                    if (barcodeArray.IndexOf(barcodeData.Data) >= 0)
                    {
                        ShowDialog("エラー", "既にスキャン済みです。", () => { });
                        return;
                    }

                    if (haisoCd != barcodeData.Data.Substring(7, 4))
                    {
                        ShowDialog("エラー", "配送先が違います。", () => { });
                        return;
                    }
                }
                

                // マテハンで紐づく
                var param = new Dictionary<string, string>
                {
                    { "pSyukaDate", prefs.GetString("syuka_date","")},
                    { "pKamotsuNo", barcodeData.Data},
                    { "tantoCd", prefs.GetString("sagyousya_cd","")},
                    { "handyId", Build.Serial}
                };

                Task.Run(async () => {
                    // CHK グローバル確認してから
                    var chkResult = await PostRestAPI("", "kosu/kosu440", param);

                    if (chkResult.ContainsKey("errorMsg"))
                    {
                        ShowDialog("エラー", chkResult["errorMsg"].ToString());
                        return;
                    }

                    if (chkResult["existFlag"].ToString() == "0")
                    {
                        // 個口データが存在しない場合
                        Bundle bundle = new Bundle();
                        bundle.PutString("barcode", barcodeData.Data);
                        bundle.PutString("matehancd", "");
                        bundle.PutString("matekbn", "1");

                        StartFragment(FragmentManager, typeof(KosuNisugataFragment), bundle);
                    }
                    else
                    {
                        mCarryBarcodes.Add(barcodeData.Data);
                        mCarryCaseKbns.Add(chkResult["caseKbn"].ToString());

                        editor.PutStringSet("mCarryBarcodes", mCarryBarcodes);
                        editor.PutStringSet("mCarryCaseKbns", mCarryCaseKbns);
                        editor.Apply();

                        Activity.RunOnUiThread(() => {
                            mOriconListTextView.Text += "\n" + barcodeData.Data;
                        });
                    }
                });
            }
        }

        public override bool OnBackPressed()
        {
            if(mOriconListTextView.Text != "")
            {
                ShowDialog("警告", "スキャンした履歴がありますが、キャンセルしますが？",(flag)=>{
                    if (flag)
                    {
                        FragmentManager.PopBackStack();
                    }
                });
                return false;
            }

            return base.OnBackPressed();
        }
    }
}