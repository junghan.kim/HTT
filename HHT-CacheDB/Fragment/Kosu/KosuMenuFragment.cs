﻿using Android.OS;
using Android.Views;
using Android.Widget;

namespace HHT
{
    public class KosuMenuFragment : BaseFragment
    {
        public override View OnCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
        {
            View view = inflater.Inflate(Resource.Layout.fragment_menu_kosu, container, false);
            view.FindViewById<Button>(Resource.Id.todokeInspButton).Click += delegate { GoSelectPage(KOSUMENU.TODOKE); };
            view.FindViewById<Button>(Resource.Id.vendaInspButton).Click += delegate { GoSelectPage(KOSUMENU.VENDOR); };
            view.FindViewById<Button>(Resource.Id.baraInspButton).Click += delegate { GoSelectPage(KOSUMENU.BARA); };

            // TEST
            view.FindViewById<Button>(Resource.Id.arataCaseInspButton).Click += delegate { GoSelectPage(KOSUMENU.ARATA_CASE); };
            view.FindViewById<Button>(Resource.Id.arataOriconInspButton).Click += delegate { GoSelectPage(KOSUMENU.ARATA_ORICON); };

            SetActionBarTitle("入荷検品");

            return view;
        }

        public override bool OnKeyDown(Keycode keycode, KeyEvent paramKeyEvent)
        {
            if (keycode == Keycode.Num1)
            {
                //GoSelectPage(KOSUMENU.TODOKE);
                GoSelectPage(KOSUMENU.ARATA_CASE);
            }
            else if (keycode == Keycode.Num2)
            {
                //GoSelectPage(KOSUMENU.VENDOR);
                GoSelectPage(KOSUMENU.ARATA_ORICON);
            }
            else if (keycode == Keycode.Num3)
            {
                //GoSelectPage(KOSUMENU.BARA);
            }
            return true;
        }

        private void GoSelectPage(KOSUMENU kosumenu)
        {
            editor.PutString("case_su", "0");
            editor.PutString("oricon_su", "0");
            editor.PutString("futeikei_su", "0");
            editor.PutString("ido_su", "0");
            editor.PutString("hazai_su", "0");
            editor.PutString("henpin_su", "0");
            editor.PutString("hansoku_su", "0");
            editor.PutString("kaisyu_su", "0");
            editor.PutString("ko_su", "0");
            editor.PutString("dai_su", "0");

            editor.PutInt("KOSU_MENU_FLAG", (int)kosumenu);
            editor.Apply();

            Bundle bundle = new Bundle();
            bundle.PutInt("menu", (int)kosumenu);
            
            StartFragment(FragmentManager, typeof(KosuSelectFragment), bundle);
            
        }

    }
}