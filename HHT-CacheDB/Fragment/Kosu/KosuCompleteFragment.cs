﻿using Android.OS;
using Android.Views;
using Com.Beardedhen.Androidbootstrap;
using System.Threading;
using System.Threading.Tasks;

namespace HHT
{
    public class KosuCompleteFragment : BaseFragment
    {
        private View view;

        public override View OnCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
        {
            view = inflater.Inflate(Resource.Layout.fragment_kosu_complete, container, false);
            SetActionBarTitle("届先指定検品");

            PlayBeepOk();

            Activity.RunOnUiThread(async () => {
                for (int i = 0; i < 3; i++)
                {
                    PlayBeepOk();

                    await Task<bool>.Run(() =>
                    {
                        Thread.Sleep(500);
                        return true;
                    });
                }
            });

            Vibrate();

            BootstrapButton confirmButton = view.FindViewById<BootstrapButton>(Resource.Id.completeButton);
            confirmButton.Click += delegate {
                BackToMainMenu();
            };

            return view;
        }

        public override bool OnKeyDown(Keycode keycode, KeyEvent paramKeyEvent)
        {
            if (keycode == Keycode.F4)
            {
                BackToMainMenu();
            }

            return true;
        }

        private void BackToMainMenu()
        {
            ((MainActivity)this.Activity).ShowProgress("少々お待ちください。");

            Activity.RunOnUiThread(async () => {
                await Task<bool>.Run(() =>
                {
                    Thread.Sleep(1000);
                    return true;
                });

                ((MainActivity)this.Activity).DismissDialog();
                
                string driver_nm = prefs.GetString("driver_nm", "");
                string souko_cd = prefs.GetString("souko_cd", "");
                string driver_cd = prefs.GetString("driver_cd", "");
                string kitaku_cd = prefs.GetString("kitaku_cd", "");
                string def_tokuisaki_cd = prefs.GetString("def_tokuisaki_cd", "");
                string souko_kbn = prefs.GetString("souko_kbn", "");

                editor.Clear();
                editor.Commit();
                
                editor.PutString("terminal_id", Build.Serial);
                editor.PutString("hht_no", Build.Serial);
                // ***********************

                editor.PutString("driver_nm", driver_nm);
                editor.PutString("souko_cd", souko_cd);
                editor.PutString("driver_cd", driver_cd);
                editor.PutString("sagyousya_cd", driver_cd);
                editor.PutString("kitaku_cd", kitaku_cd);
                editor.PutString("def_tokuisaki_cd", def_tokuisaki_cd);
                editor.PutString("souko_kbn", souko_kbn);
                editor.Apply();

                FragmentManager.PopBackStack(FragmentManager.GetBackStackEntryAt(0).Id, 0);

            });
        }

    }
}