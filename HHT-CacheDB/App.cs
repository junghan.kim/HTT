﻿using System;
using System.Collections.Generic;
using Android.App;
using Android.Runtime;
using Java.Lang;
using Microsoft.AppCenter.Crashes;

namespace HHT
{
    [Application]
    class App : Application
    {
        public App(IntPtr javaReference, JniHandleOwnership transfer)
        : base(javaReference, transfer) { }

        public override void OnCreate()
        {
            base.OnCreate();

            var exceptionHandler = new UncaughtExceptionHandler();
            AndroidEnvironment.UnhandledExceptionRaiser += MyApp_UnhandledExceptionHandler;
            Thread.DefaultUncaughtExceptionHandler = exceptionHandler;
        }

        void MyApp_UnhandledExceptionHandler(object sender, RaiseThrowableEventArgs e)
        {
            // Do your error handling here.
            var properties = new Dictionary<string, string> {
                        { "Category", "Music11" }
                      };

            Crashes.TrackError(e.Exception, properties);

            // 종료
            //Android.OS.Process.KillProcess(Android.OS.Process.MyPid());
            //System.Environment.Exit(10);

        }

        protected override void Dispose(bool disposing)
        {
            AndroidEnvironment.UnhandledExceptionRaiser -= MyApp_UnhandledExceptionHandler;
            base.Dispose(disposing);
        }
    }


    class UncaughtExceptionHandler : Java.Lang.Object, Thread.IUncaughtExceptionHandler
    {
        public void UncaughtException(Thread t, Throwable ex)
        {
            var properties = new Dictionary<string, string> {
                        { "Category", "Music11" }
                      };
            Crashes.TrackError(ex, properties);
        }
    }
}