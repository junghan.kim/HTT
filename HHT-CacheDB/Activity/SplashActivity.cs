﻿using Android.App;
using Android.Content;
using Android.Preferences;
using Android.Support.V7.App;
using Android.Util;
using Android.Widget;
using Microsoft.AppCenter.Crashes;
using System;
using System.Threading.Tasks;

namespace HHT
{
    [Activity(Theme = "@style/MyTheme.Splash", MainLauncher = true, NoHistory = true)]
    public class SplashActivity : AppCompatActivity
    {
        static readonly string TAG = typeof(SplashActivity).Name;

        protected override void OnResume()
        {
            base.OnResume();

            Task.Run(async () =>
            {
                
                try
                {
                    /*
                    bool isConnected = await WebService.IsConnected();
                    
                    RunOnUiThread(() =>
                    {
                        if (isConnected)
                        {
                            Toast.MakeText(ApplicationContext, "サーバと接続成功しました。", ToastLength.Short).Show();
                        }
                        else
                        {
                            Toast.MakeText(ApplicationContext, "サーバと接続失敗。\n接続情報を確認してください。", ToastLength.Short).Show();
                        }
                    });
                    */
                }
                catch (Exception exception)
                {
                    Crashes.TrackError(exception);
                }

                Log.Debug(TAG, "Startup work is finished - starting MainActivity.");
                StartActivity(new Intent(Application.Context, typeof(MainActivity)));
                
            });
        }
        
        public override void OnBackPressed() { }
    }
}