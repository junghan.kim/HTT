﻿
namespace HHT.Resources.Model
{
    public class KOSU110
    {
        public int sum_case { get; set; }
        public int sum_case_sumi { get; set; }
        public int sum_oricon { get; set; }
        public int sum_oricon_sumi { get; set; }
        public int sum_futeikei { get; set; }
        public int sum_futeikei_sumi { get; set; }
        public int sum_ido { get; set; }
        public int sum_ido_sumi { get; set; }
        public int sum_tc { get; set; }
        public int sum_tc_sumi { get; set; }
        public int sum_hansoku { get; set; }
        public int sum_hansoku_sumi { get; set; }
        public int sum_hazai { get; set; }
        public int sum_hazai_sumi { get; set; }
        public int sum_henpin { get; set; }
        public int sum_henpin_sumi { get; set; }
        public int total { get; set; }
        public int total_sumi { get; set; }
        public int sum_mate_cnt { get; set; }
        
    }
}