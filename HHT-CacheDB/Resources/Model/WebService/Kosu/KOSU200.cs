﻿
using System.Collections.Generic;

namespace HHT.Resources.Model
{
    public class KOSU200
    {
        public string VendorNm { get; set; }
        public List<Matehan> MatehanList { get; set; }
    }

    public class Matehan
    {
        public string MatehanCd { get; set; }
        public string MatehanNm { get; set; }
    }
    
}