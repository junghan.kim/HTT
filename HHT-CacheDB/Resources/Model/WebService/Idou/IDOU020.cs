﻿
using System.Collections.Generic;

namespace HHT.Resources.Model
{
    public class IDOU020
    {
        public string matehan { get; set; }
        public string oriconSu { get; set; }
        public string caseSu { get; set; }
        public string hutekeiSu { get; set; }
        public List<string> kamotsuList { get; set; }
    }
}